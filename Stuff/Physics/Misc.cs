﻿using System;

namespace Stuff.Physics
{
    public static class Misc
    {
        public const double G = 6.67408e-11;

        public const double g = 9.81;

        public const double LIGHT_YEAR = 9.4607e15;
    }
}
