﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Old;
using Stuff.StuffMath.Structures;

namespace Stuff.Physics
{
    public class MassObject : IMassObject
    {
        public double Mass { get; }

        public double Radius { get; }

        public HilbertVector<FDouble> Location { get; }

        public MassObject(HilbertVector<FDouble> location, double mass, double radius = 0)
        {
            if (radius < 0)
                throw new ArgumentException("Radius must be non-negative.");
            Location = location;
            Mass = mass;
            Radius = radius;
        }
    }
}
