﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Old;
using Stuff.StuffMath.Structures;

namespace Stuff.Physics
{
    /// <summary>
    /// A spherical object with uniform density.
    /// </summary>
    public interface IMassObject
    {
        /// <summary>
        /// The mass object's mass.
        /// </summary>
        double Mass { get; }

        /// <summary>
        /// The radius of the mass object.
        /// Should always be positive.
        /// </summary>
        double Radius { get; }

        /// <summary>
        /// The location of the mass object's center of mass.
        /// </summary>
        HilbertVector<FDouble> Location { get; }
    }

    public static class MassObjectExtensions
    {
        public static bool IsPointMass(this IMassObject mo) => mo.Radius == 0;

        public static double Volume(this IMassObject mo) => 4 * Math.PI * Math.Pow(mo.Radius, 3) / 3;

        public static double DistanceSquared(this IMassObject mo1, IMassObject mo2) =>
            (mo1.Location - mo2.Location).LengthSquared;

        /// <returns>The distance between the two objects center of mass.</returns>
        public static double Distance(this IMassObject mo1, IMassObject mo2) => (mo1.Location - mo2.Location).Length;

        /// <returns>The shortest distance between any points in the two objects.</returns>
        public static double MinimumDistance(this IMassObject mo1, IMassObject mo2) => mo1.Distance(mo2) - mo1.Radius - mo2.Radius;
        
        /// <returns>A unit direction vector pointing from this object in the direction of mo2.</returns>
        public static HilbertVector<FDouble> Direction(this IMassObject mo1, IMassObject mo2) => (mo1.Location - mo2.Location).Normalize();

        /// <returns>Whether there is any overlap between the two objects.</returns>
        public static bool Overlap(this IMassObject mo1, IMassObject mo2) =>
            mo1.Distance(mo2) < mo1.Radius + mo2.Radius;
        
        /// <returns>Whether this mass object's center of mass is inside the other object.</returns>
        public static bool Inside(this IMassObject mo1, IMassObject mo2) => mo1.Distance(mo2) < mo2.Radius;
        
        /// <returns>The part of the first objects mass that pulls on the second object.</returns>
        private static double GravMass(this IMassObject mo1, IMassObject mo2)
        {
            if (mo2.Inside(mo1))
            {
                Debug.Assert(!mo1.IsPointMass());
                return mo1.Mass * Math.Pow(mo1.Distance(mo2) / mo1.Radius, 3);
            }

            return mo1.Mass;
        }
        /// <returns>The gravitational force of mo2 on this object.</returns>
        public static HilbertVector<FDouble> GravitationalForce(this IMassObject mo1, IMassObject mo2) =>
            mo1.Direction(mo2) * Misc.G * mo1.GravMass(mo2) * mo2.GravMass(mo1) / DistanceSquared(mo1, mo2);
        
        /// <returns>The total gravitational force of all objects in list on this object.</returns>
        public static HilbertVector<FDouble> GravitationalForce(this IMassObject mo1, IEnumerable<IMassObject> list) =>
            list.Aggregate(mo1.Location.ZERO, (vec, mo) => vec + mo1.GravitationalForce(mo));

        /// <returns>The total gravitational force of all objects in list on this object.</returns>
        public static HilbertVector<FDouble> GravitationalForce(this IMassObject mo1, params IMassObject[] list) =>
            mo1.GravitationalForce(list.AsEnumerable());

        /// <returns>The total gravitational potential energy between two objects.</returns>
        public static double Potential(this IMassObject mo1, IMassObject mo2) =>
            -Misc.G * mo1.Mass * mo2.Mass / Distance(mo1, mo2);

        /// <returns>The speed needed for this object to escape the gravitational pull of mo2 assuming mo2 is stationary.</returns>
        public static double EscapeSpeed(this IMassObject mo1, IMassObject mo2) =>
            Math.Sqrt(2 * Misc.G * mo2.Mass / Distance(mo1, mo2));

        public static HilbertVector<FDouble> GravitationalAcceleration(this IMassObject mo1, HilbertVector<FDouble> loc) =>
            mo1.GravitationalForce(new MassObject(loc, 1));
    }
}
