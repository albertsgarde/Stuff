﻿using System.Xml.Linq;

namespace Stuff
{
    public interface ISaveable
    {
        XElement ToXElement(string name);
    }
}