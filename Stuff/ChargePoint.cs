﻿using System;
using Stuff.StuffMath;
using Stuff.StuffMath.Old;

namespace Stuff
{
    public class ChargePoint
    {
        public const double COULOMB = 1 / (4 * Math.PI * 8.85 * 0.000000000001);

        public ChargePoint(Location2D loc, double charge)
        {
            Location = loc;
            Charge = charge;
        }

        public Location2D Location { get; }

        public double Charge { get; }

        public double ForceSize(ChargePoint cp)
        {
            return COULOMB * Charge * cp.Charge / Location.DistanceToSquared(cp.Location);
        }

        public Vector2D ForceAtPoint(Location2D loc, double charge)
        {
            return new Vector2D(Location, loc).Scale(COULOMB * Charge * charge / Location.DistanceToSquared(loc));
        }
    }
}