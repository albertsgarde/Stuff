﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Stuff
{
    public static class ContainerUtils
    {
        public static LinkedList<TSource> GetRange<TSource>(this LinkedList<TSource> list, int index, int count)
        {
            var result = new LinkedList<TSource>();
            for (var i = index; i < index + count; i++)
                result.AddLast(list.ElementAt(i));
            return result;
        }

        public static int FirstIndexOf<TSource>(this IEnumerable<TSource> list, TSource value)
        {
            for (var i = 0; i < list.Count(); i++)
                if (list.ElementAt(i).Equals(value))
                    return i;
            throw new Exception("value not in IEnumerable");
        }

        public static int FirstIndexOf<TSource>(this IEnumerable<TSource> list, Func<TSource, bool> selector)
        {
            for (var i = 0; i < list.Count(); i++)
                if (selector.Invoke(list.ElementAt(i)))
                    return i;
            throw new Exception("value not in IEnumerable");
        }

        /// <summary>
        ///     Returns the first element in the collection that matches the selector.
        ///     Throws an exception if no such element is found.
        /// </summary>
        /// <returns>The first element in the collection that matches the selector.</returns>
        public static TSource SelectFirst<TSource>(this IEnumerable<TSource> list, Func<TSource, bool> selector)
        {
            foreach (var element in list)
                if (selector.Invoke(element))
                    return element;
            throw new Exception("No element found matching selector");
        }

        public static IEnumerable<T> Reverse<T>(this LinkedList<T> list)
        {
            var el = list.Last;
            while (el != null)
            {
                yield return el.Value;
                el = el.Previous;
            }
        }

        public static void RemoveAll<T>(this List<T> list, List<T> remover)
        {
            list.RemoveAll(remover.Contains);
        }

        public static T[] Append<T>(this T[] firstArray, params T[] secondArray)
        {
            var result = new LinkedList<T>();
            foreach (var t in firstArray)
                result.AddLast(t);
            foreach (var t in secondArray)
                result.AddLast(t);
            return result.ToArray();
        }

        public static LinkedList<T> Append<T>(this LinkedList<T> list1, IEnumerable<T> list2)
        {
            foreach (var t in list2)
                list1.AddLast(t);
            return list1;
        }

        public static List<T> Append<T>(this List<T> list1, IEnumerable<T> list2)
        {
            foreach (var t in list2)
                list1.Add(t);
            return list1;
        }

        public static void Print<T>(this IEnumerable<T> list, string prefix = "")
        {
            foreach (var t in list)
                Console.WriteLine(prefix + t);
        }

        public static T Random<T>(this IEnumerable<T> list)
        {
            return list.ElementAt(Rand.Next(list.Count()));
        }

        public static string AsString<T>(this IEnumerable<char> charArray)
        {
            return charArray.Aggregate("", (current, c) => current + c);
        }

        /// <summary>Like Max(), but returns the element instead of the value.</summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="list"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static T MaxValue<T, TKey>(this IEnumerable<T> list, Func<T, TKey> keySelector)
        {
            return list.OrderBy(keySelector).Last();
        }

        public static T MinValue<T, TKey>(this IEnumerable<T> list, Func<T, TKey> keySelector)
        {
            return list.OrderBy(keySelector).First();
        }

        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                var j = Rand.Next(i + 1);
                list.Swap(j, i);
            }

            return list;
        }

        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB)
        {
            var tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }

        public static Dictionary<TKey, TValue> Copy<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dict)
        {
            var result = new Dictionary<TKey, TValue>();
            foreach (var element in dict)
                result[element.Key] = element.Value;
            return result;
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            return dict.ToDictionary(k => k.Key, v => v.Value);
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(
            this IEnumerable<KeyValuePair<TKey, TValue>> dict)
        {
            var result = new Dictionary<TKey, TValue>();
            foreach (var (key, value) in dict)
                result[key] = value;
            return result;
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<(TKey, TValue)> dict)
        {
            var result = new Dictionary<TKey, TValue>();
            foreach (var (key, value) in dict)
                result[key] = value;
            return result;
        }

        // ReSharper disable All
        [Obsolete("Copy is redundant, please use IEnumerable.ToList instead.")]
        public static List<T> Copy<T>(this IEnumerable<T> list)
        {

            var result = new List<T>(list.Count());
            result.AddRange(list);
            return result;
        }
        // ReSharper restore All

        public static T[] Copy<T>(this T[] list)
        {
            var result = new T[list.Length];
            list.CopyTo(result, 0);
            return result;
        }

        // ReSharper disable All
        [Obsolete("Contains is redundant, please use IEnumerable.Any instead.")]
        public static bool Contains<T>(this IEnumerable<T> list, Func<T, bool> selector)
        {
            foreach (var t in list)
                if (selector.Invoke(t))
                    return true;
            return false;
        }
        // ReSharper restore All

        public static bool ContainsAll<T>(this IEnumerable<T> list1, IEnumerable<T> list2)
        {
            return list2.All(list1.Contains);
        }

        public static string AsString<T>(this IEnumerable<T> list)
        {
            var result = list.Aggregate("[", (current, value) => current + (value + ","));
            return result.TrimEnd(',') + "]";
        }

        public static string AsString<T>(this T[] list)
        {
            var result = list.Aggregate("{", (current, value) => current + (value + ","));
            return result.TrimEnd(',') + "}";
        }

        public static string AsString<T>(LinkedListNode<T> start, LinkedListNode<T> end)
        {
            var result = "{";
            var node = start;
            while (node != end.Next)
            {
                result += node.Value + ",";
                node = node.Next;
            }

            return result.TrimEnd(',') + "}";
        }

        public static T[] UniformArray<T>(T t, int length)
        {
            var result = new T[length];
            for (var i = 0; i < length; ++i)
                result[i] = t;
            return result;
        }

        public static void Deconstruct<K, V>(this KeyValuePair<K, V> kvp, out K k, out V v)
        {
            k = kvp.Key;
            v = kvp.Value;
        }

        public static IEnumerable<(T1, T2)> Zip<T1, T2>(this IEnumerable<T1> list1, IEnumerable<T2> list2)
        {
            return list1.Zip(list2, (t1, t2) => (t1, t2));
        }

        public static IReadOnlyDictionary<K, int> Add<K>(this IReadOnlyDictionary<K, int> dict1,
            IReadOnlyDictionary<K, int> dict2)
        {
            var result = dict1.ToDictionary();
            foreach (var (k, v) in dict2)
            {
                if (result.ContainsKey(k))
                    result[k] += v;
                else
                    result[k] = v;
            }

            return result;
        }

        public static IReadOnlyDictionary<K, double> Add<K>(this IReadOnlyDictionary<K, double> dict1,
            IReadOnlyDictionary<K, double> dict2)
        {
            var result = dict1.ToDictionary();
            foreach (var (k, v) in dict2)
            {
                if (result.ContainsKey(k))
                    result[k] += v;
                else
                    result[k] = v;
            }

            return result;
        }

        public static bool EqualTo<K, V>(this IReadOnlyDictionary<K, V> dict1, IReadOnlyDictionary<K, V> dict2)
        {
            if (dict1.Keys.Count() != dict2.Keys.Count())
                return false;
            foreach (var (k, v) in dict1)
            {
                if (!dict2.ContainsKey(k) || !dict2[k].Equals(v))
                    return false;
            }

            return true;
        }

        public static IEnumerable<T> List<T>(T head, Func<T, T> successor)
        {
            yield return head;
            var prev = head;
            while (true)
                yield return prev = successor(prev);
        }

        /// <returns>An infinite list of integers counting stepwise from 0.</returns>
        public static IEnumerable<int> Count(int startValue = 0) => List(startValue, (prev) => prev + 1);
    }
}