﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Stuff
{
    public class Rand
    {
        [ThreadStatic] private static Random local;

        public static Random ThisThreadsRandom =>
            local ?? (local = new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId)));

        public static Random Random => ThisThreadsRandom;

        /// <returns>A randomly generated integer.</returns>
        public static int Next()
        {
            return ThisThreadsRandom.Next();
        }

        /// <param name="exclusiveMaxValue">Maximum value of the randomly generated result.</param>
        /// <returns>A randomly generated integer from 0 inclusively to exclusiveMaxValue exclusively</returns>
        public static int Next(int exclusiveMaxValue)
        {
            return ThisThreadsRandom.Next(0, exclusiveMaxValue);
        }

        /// <param name="inclusiveMinValue">Minimum value of the randomly generated result.</param>
        /// <param name="exclusiveMaxValue">Maximum value of the randomly generated result.</param>
        /// <returns>A randomly generated integer from inclusiveMinValue inclusively to exclusiveMaxValue exclusively</returns>
        public static int Next(int inclusiveMinValue, int exclusiveMaxValue)
        {
            return ThisThreadsRandom.Next(inclusiveMinValue, exclusiveMaxValue);
        }

        /// <returns>A random double between 0.0 and 1.0.</returns>
        public static double NextDouble()
        {
            return ThisThreadsRandom.NextDouble();
        }

        /// <param name="exclusiveMaxValue">Maximum value of the randomly generated result.</param>
        /// <returns>A randomly generated double from 0 inclusively to exclusiveMaxValue exclusively</returns>
        public static double NextDouble(double exclusiveMaxValue)
        {
            return ThisThreadsRandom.NextDouble() * exclusiveMaxValue;
        }

        /// <param name="inclusiveMinValue">Minimum value of the randomly generated result.</param>
        /// <param name="exclusiveMaxValue">Maximum value of the randomly generated result.</param>
        /// <returns>A randomly generated integer from inclusiveMinValue inclusively to exclusiveMaxValue exclusively</returns>
        public static double NextDouble(double inclusiveMinValue, double exclusiveMaxValue)
        {
            return ThisThreadsRandom.NextDouble() * (exclusiveMaxValue - inclusiveMinValue) + inclusiveMinValue;
        }

        /// <returns>A randomly generated boolean.</returns>
        public static bool NextBool()
        {
            return ThisThreadsRandom.Next(0, 2) == 1 ? true : false;
        }

        public static int Roll(string roll)
        {
            var commands = roll.Split('d');
            var maxRoll = int.Parse(commands[1]);
            var rolls = new LinkedList<int>();
            var numRolls = int.Parse(commands[0]);
            for (var i = 0; i < numRolls; i++)
                rolls.AddLast(Next(1, maxRoll + 1));
            if (commands.Length == 3)
            {
                var drops = int.Parse(commands[2]);
                for (var i = 0; i < drops; i++)
                    rolls.Remove(rolls.Min());
            }

            var result = 0;
            foreach (var i in rolls)
                result += i;
            return result;
        }
    }
}