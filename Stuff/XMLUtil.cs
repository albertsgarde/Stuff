﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Stuff
{
    public static class XMLUtil
    {
        /// <param name="element">The element in which to check.</param>
        /// <param name="name">The name of the child.</param>
        /// <returns>Whether the this XElement contains a child of the specified name.</returns>
        public static bool Contains(this XElement element, XName name)
        {
            return element.Elements(name).Count(p => true) > 0;
        }

        /// <param name="element">The element whose children to apply the predicate to.</param>
        /// <param name="predicate">The predicate to check against the child elements.</param>
        /// <returns>Whether this XElement contains a child matching the predicate.</returns>
        public static bool Any(this XElement element, Func<XElement, bool> predicate)
        {
            return element.Elements().Any(predicate);
        }

        /// <summary>
        ///     Finds all child elements matching a predicate.
        /// </summary>
        /// <param name="element">The element whose children to apply the predicate to.</param>
        /// <param name="predicate">The predicate to check against the child elements.</param>
        /// <returns>An IEnumerable containing all matching child elements.</returns>
        public static IEnumerable<XElement> Where(this XElement element, Func<XElement, bool> predicate)
        {
            return element.Elements().Where(predicate);
        }

        /// <param name="element">The element whose child to get.</param>
        /// <param name="name">The name of the child to get the value of.</param>
        /// <returns>The value of a specified child.</returns>
        /// <exception cref="ArgumentException">Thrown when to child of the given name exists in the element.</exception>  
        public static string ElementValue(this XElement element, XName name)
        {
            return element.Element(name)?.Value ?? throw new ArgumentException("No child of name " + name + " exists in element.");
        }

        /// <param name="element">The element whose child to get.</param>
        /// <param name="name">The name of the child to get the value of.</param>
        /// <param name="defaultValue">Returned if no child with the given name exists.</param>
        /// <returns>The value of the first child with the specified child, or the default value if no such exists.</returns>
        public static string ElementValueOrDefault(this XElement element, XName name, string defaultValue)
        {
            return element.Element(name)?.Value ?? defaultValue;
        }

        /// <param name="element">The element whose children's values to get.</param>
        /// <returns>An IEnumerable containing the values of all children in this element.</returns>
        public static IEnumerable<string> ElementValues(this XElement element)
        {
            return element.Elements().Select(subElement => subElement.Value);
        }

        /// <param name="element">The element whose children's values to get.</param>
        /// <param name="name">The name of the children.</param>
        /// <returns>An IEnumerable containing the values of all elements contained in this element with the specified name.</returns>
        public static IEnumerable<string> ElementValues(this XElement element, XName name)
        {
            return element.Elements(name).Select(subElement => subElement.Value);
        }

        
        /// <summary>
        /// Creates a child with the specified name and value and adds it to the element.
        /// </summary>
        /// <param name="element">The element to add the child to.</param>
        /// <param name="name">The name of the child.</param>
        /// <param name="value">The value of the child.</param>
        /// <returns>The created child.</returns>
        public static XElement AddValue(this XElement element, XName name, object value)
        {
            var newElement = new XElement(name, value);
            element.Add(newElement);
            return newElement;
        }

        /// <summary>
        /// Creates a child in an element.
        /// </summary>
        /// <param name="element">The element in which to create the child.</param>
        /// <param name="name">The name of the child.</param>
        /// <returns>The created child.</returns>
        public static XElement CreateElement(this XElement element, XName name)
        {
            var newElement = new XElement(name);
            element.Add(newElement);
            return newElement;
        }

        /// <summary>
        ///     Returns an element with the given name if such exists and creates a new, empty one if it doesn't.
        /// </summary>
        /// <param name="element">The element to get from.</param>
        /// <param name="name">The name of the element to get.</param>
        public static XElement GetOrNew(this XElement element, XName name)
        {
            if (element.Contains(name))
                return element.Element(name);
            element.Add(new XElement(name));
            return element.Element(name);
        }

        /// <param name="path">The path in which to look for elements.</param>
        /// <param name="recursive">If false, the method only searches files in the top directory.</param>
        /// <param name="predicate">The predicate to apply.</param>
        /// <returns>All top level xml elements in any files in the given directory that match the given predicate.</returns>
        public static IEnumerable<XElement> FindElements(string path, Func<XElement, bool> predicate, bool recursive = true)
        {
            if (!File.GetAttributes(path).HasFlag(FileAttributes.Directory))
                return XElement.Load(path).Where(predicate);

            var result = new List<XElement>();
            foreach (var f in from filePath in Directory.EnumerateFiles(path, "*.xml",
                    recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                where
                    !filePath.Substring(filePath.LastIndexOf('/')).StartsWith("!")
                select filePath)
                result.AddRange(FindElements(f, predicate));
            return result;

        }
    }
}