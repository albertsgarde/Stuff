﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Stuff
{
    public class SettingsLoader
    {
        public static SettingsManager LoadSettings(string path = "Assets/Settings", bool recursive = true)
        {
            var result = new SettingsManager();
            try
            {
                foreach (var f in from filePath in Directory.EnumerateFiles(path, "*.txt",
                        recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                    where
                        !filePath.Substring(filePath.LastIndexOf('/')).StartsWith("!")
                    select filePath)
                {
                    var file = new StreamReader(f);
                    var category = f.Substring(f.LastIndexOf("\\") + 1, f.Length - 5 - f.LastIndexOf("\\"));
                    result.AddCategory(category);
                    while (!file.EndOfStream)
                    {
                        var line = file.ReadLine();
                        if (line.Contains("//"))
                            line = line.Substring(0, line.IndexOf("//"));
                        if (!string.IsNullOrWhiteSpace(line))
                        {
                            var args = line.Split(' ');
                            var key = args[0];
                            var values = args[1].Split(',');
                            result.AddSetting(category, key, values);
                        }
                    }

                    file.Close();
                }
            }
            catch (UnauthorizedAccessException UAEx)
            {
                Console.WriteLine(UAEx.Message);
            }
            catch (PathTooLongException PathEx)
            {
                Console.WriteLine(PathEx.Message);
            }
            catch (DirectoryNotFoundException dnfe)
            {
                Console.WriteLine(dnfe.Message);
            }

            return result;
        }

        public static void SaveSettings(SettingsManager settings, string path = "Assets/Settings")
        {
            var files = new LinkedList<string>();
            if (Directory.Exists(path))
                foreach (var f in from filePath in Directory.EnumerateFiles(path, "*.txt", SearchOption.AllDirectories)
                    where
                        !filePath.Substring(filePath.LastIndexOf('/')).Contains('!')
                    select filePath)
                    files.AddLast(f);
            foreach (var category in settings.Categories())
            {
                string file; // Find the file for the category if it exists, or else create a new one.
                if (files.Count(s => s.EndsWith("/" + category + ".txt")) != 0)
                {
                    file = files.Single(s => s.EndsWith("/" + category + ".txt"));
                }
                else
                {
                    using (File.Create(path + "/" + category + ".txt"))
                    {
                    }

                    file = path + "/" + category + ".txt";
                }

                //Create an IEnumerable with the lines.
                var lines = new LinkedList<string>();
                foreach (var setting in settings.SettingsInCategory(category))
                {
                    var line = setting.Key + " ";
                    foreach (var value in setting.Value)
                        line += value + " ";
                    lines.AddLast(line.Trim());
                }

                //Actually save the category.
                File.WriteAllLines(file, lines);
            }
        }
    }
}