﻿using System.Collections;
using System.Collections.Generic;

namespace Stuff
{
    public class CircularArray<T> : IEnumerable<T>
    {
        private readonly T[] values;

        private int curPos;

        public CircularArray(int size)
        {
            curPos = 0;
            values = new T[size];
        }

        public int Length => values.Length;

        public T this[int index]
        {
            get => values[(index + curPos) % Length];
            set => values[(index + curPos) % Length] = value;
        }

        /// <summary>
        ///     Adds the specified element to the front of the array, thereby removing the last element.
        /// </summary>
        public T Add(T value)
        {
            if (--curPos == -1)
                curPos = Length - 1;
            values[curPos] = value;
            return value;
        }

        /// <summary>
        ///     Adds the specified element to the end of the array, thereby removing the first element.
        /// </summary>
        public T AddLast(T value)
        {
            if (++curPos == Length)
                curPos = 0;
            values[curPos] = value;
            return value;
        }

        public void Add(params T[] values)
        {
            foreach (var value in values)
                Add(value);
        }

        /// <summary>
        ///     Moves the last element to the front.
        /// </summary>
        /// <returns>The new front element.</returns>
        public T Advance()
        {
            if (--curPos == -1)
                curPos = Length - 1;
            return values[curPos];
        }

        public T First() => values[curPos];

        public T Last() => this[curPos - 1];

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = curPos; i < Length; ++i)
                yield return values[i];
            for (var i = 0; i < curPos; ++i)
                yield return values[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}