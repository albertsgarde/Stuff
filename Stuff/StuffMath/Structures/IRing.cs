﻿using System.Diagnostics.CodeAnalysis;

namespace Stuff.StuffMath.Structures
{
    public interface IRing<R> : IGroup<R> where R : IRing<R>, new()
    {
        /// <summary>
        ///     This ring's multiplicative identity. Should be constant across all instances.
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        R ONE { get; }

        R Multiply(R t);
    }

    public static class RingExtensions
    {
        public static bool IsZero<R>(this R r) where R : IRing<R>, new()
        {
            return r.EqualTo(new R());
        }

        public static bool IsOne<R>(this R r) where R : IRing<R>, new()
        {
            return r.EqualTo(new R().ONE);
        }

        public static R Square<R>(this R r) where R : IRing<R>, new()
        {
            return r.Multiply(r);
        }
    }
}