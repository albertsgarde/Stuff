﻿using System.Diagnostics.CodeAnalysis;

namespace Stuff.StuffMath.Structures
{
    public interface IGroup<G> where G : IGroup<G>
    {
        /// <summary>
        ///     This ring's additive identity. Should be constant across all instances.
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        G ZERO { get; }

        G Add(G t);

        G AdditiveInverse();

        bool EqualTo(G t);
    }

    public static class GroupExtensions
    {
        public static G Subtract<G>(this G t1, G t2) where G : IGroup<G>
        {
            return t1.Add(t2.AdditiveInverse());
        }
    }
}