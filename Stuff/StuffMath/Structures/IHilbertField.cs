﻿namespace Stuff.StuffMath.Structures
{
    public interface IHilbertField<T> : IField<T>, ILatexable where T : IHilbertField<T>, new()
    {
        T AbsSqrt();
    }
}