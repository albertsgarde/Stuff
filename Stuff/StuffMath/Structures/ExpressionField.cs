﻿using System.Collections.Generic;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.Expressions.Functions;

namespace Stuff.StuffMath.Structures
{
    /// <summary>
    ///     A wrapper class that allows Expressions to be used as IHilbertFields.
    /// </summary>
    public class ExpressionField : IHilbertField<ExpressionField>, IExpression<ExpressionField>
    {
        public ExpressionField() : this(new ValueExpression(0))
        {
        }

        public ExpressionField(Expression exp)
        {
            Exp = exp;
        }

        public Expression Exp { get; }

        public ExpressionField NewExpression(Expression exp) => new ExpressionField(exp);

        public ExpressionField ONE => new ExpressionField(1);

        public ExpressionField ZERO => new ExpressionField(0);

        public ExpressionField AbsSqrt()
        {
            return new Sqrt(Exp);
        }

        public ExpressionField Add(ExpressionField t)
        {
            return Exp + t;
        }

        public ExpressionField AdditiveInverse()
        {
            return -Exp;
        }

        public ExpressionField Conjugate()
        {
            return new Conjugate(Exp);
        }

        public bool EqualTo(ExpressionField t)
        {
            return Exp.EqualTo(t.Exp);
        }

        public ExpressionField MultiplicativeInverse()
        {
            return 1 / Exp;
        }

        public ExpressionField Multiply(ExpressionField t)
        {
            return Exp * t;
        }

        public string ToLatex()
        {
            return Exp.ToLatex();
        }

        public static implicit operator ExpressionField(Expression exp)
        {
            return new ExpressionField(exp);
        }

        public static implicit operator Expression(ExpressionField expf)
        {
            return expf.Exp;
        }

        public bool ContainsVariable(string variable)
        {
            return Exp.ContainsVariable(variable);
        }

        public bool ContainsVariables(params string[] variables)
        {
            return Exp.ContainsVariables(variables);
        }

        public HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Exp.ContainedVariables(vars);
        }

        public HashSet<string> ContainedVariables()
        {
            return Exp.ContainedVariables();
        }

        public override string ToString()
        {
            return Exp.ToString();
        }
    }
}