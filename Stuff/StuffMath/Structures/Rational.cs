﻿using System;
using System.Diagnostics;

namespace Stuff.StuffMath.Structures
{
    /// <summary>
    ///     A rational number.
    /// </summary>
    public class Rational : IField<Rational>, ILatexable, IComparable<Rational>
    {
        private static readonly Rational zero = new Rational();

        private static readonly Rational one = new Rational(1, 1);

        public Rational()
        {
            Numerator = 0;
            Denominator = 1;
        }

        public Rational(Integer numerator, Integer denominator)
        {
            var gcd = numerator == 0 ? denominator : new Integer(Basic.GCD((int) numerator, (int) denominator));
            Numerator = numerator / gcd;
            if (denominator == 0)
                throw new ArgumentException("Denominator cannot be 0.");
            Denominator = denominator / gcd;
        }

        public Rational(Integer numerator) : this(numerator, 1)
        {

        }

        public Integer Numerator { get; }

        public Integer Denominator { get; }

        public Rational ZERO => zero;

        public Rational ONE => one;

        public static explicit operator double(Rational r)
        {
            return (double)r.Numerator / (double)r.Denominator;
        }

        public static implicit operator Rational((Integer numerator, Integer denominator) r)
        {
            return new Rational(r.numerator, r.denominator);
        }

        public static implicit operator Rational(Integer i)
        {
            return new Rational(i, 1);
        }

        public static implicit operator Rational(int i)
        {
            return new Rational(i, 1);
        }

        public static Rational operator +(Rational r1, Rational r2) => r1.Add(r2);

        public static Rational operator -(Rational r1, Rational r2) => r1.Subtract(r2);

        public static Rational operator *(Rational r1, Rational r2) => r1.Multiply(r2);

        public static Rational operator /(Rational r1, Rational r2) => r1.Divide(r2);

        public static Rational operator -(Rational r) => r.AdditiveInverse();

        public static bool operator ==(Rational r1, Rational r2) => r2.EqualTo(r1);

        public static bool operator !=(Rational r1, Rational r2) => r2.EqualTo(r1);

        public static bool operator <(Rational r1, Rational r2) => r1.Numerator*r2.Denominator < r2.Numerator*r1.Denominator;

        public static bool operator >(Rational r1, Rational r2) => r1.Numerator * r2.Denominator > r2.Numerator * r1.Denominator;

        public static bool operator <=(Rational r1, Rational r2) => r1.Numerator * r2.Denominator <= r2.Numerator * r1.Denominator;

        public static bool operator >=(Rational r1, Rational r2) => r1.Numerator * r2.Denominator >= r2.Numerator * r1.Denominator;

        public Rational Add(Rational r)
        {
            return new Rational(Numerator * r.Denominator + r.Numerator * Denominator, Denominator * r.Denominator);
        }

        public Rational AdditiveInverse()
        {
            return new Rational(-Numerator, Denominator);
        }

        public Rational Multiply(Rational r)
        {
            return new Rational(Numerator * r.Numerator, Denominator * r.Denominator);
        }

        public Rational MultiplicativeInverse()
        {
            if (Numerator == 0)
                throw new InvalidOperationException("0 has no mulitplicative inverse");
            return new Rational(Denominator, Numerator);
        }

        public Rational Conjugate() => this;

        public Rational AbsoluteValue()
        {
            Debug.Assert(Denominator > 0);
            if (Numerator > 0)
                return this;
            else
                return -this;
        }

        public int CompareTo(Rational r)
        {
            if (this < r)
                return -1;
            if (this > r)
                return 1;
            return 0;
        }

        public bool EqualTo(Rational r)
        {
            return Numerator * r.Denominator == r.Numerator * Denominator;
        }

        public override bool Equals(object obj)
        {
            return obj is Rational r && EqualTo(r);
        }

        public override int GetHashCode()
        {
            return Misc.HashCode(17, 23, Numerator, Denominator);
        }

        public override string ToString()
        {
            return $"{Numerator}/{Denominator}";
        }

        public string ToLatex()
        {
            return Denominator == 1 ? "" + Numerator : $"\\frac{{{Numerator}}}{{{Denominator}}}";
        }
    }
}