﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath
{
    public class Angle : IGroup<Angle>
    {
        public const double PI_OVER_180 = Math.PI/180;

        /// <summary>
        /// The radians in the interval ]-pi;pi].
        /// </summary>
        public double Radians { get; }

        public Angle(double radians)
        {
            Radians = Basic.Mod(radians+Math.PI, Math.PI * 2) - Math.PI;
        }

        public double ToDegrees() => Radians / PI_OVER_180;

        public double Reflex => Radians < 0 ? Radians + 2 * Math.PI : Radians - 2 * Math.PI;

        public Angle ZERO => new Angle(0);

        public static implicit operator Angle(double d) => new Angle(d);

        public static implicit operator double(Angle a) => a.Radians;

        public static Angle operator +(Angle a1, Angle a2) => a1.Add(a2);
        
        /// <returns>The smallest angle from a1 to a2.</returns>
        public static Angle operator -(Angle a1, Angle a2) => a1.Subtract(a2);

        public static Angle operator -(Angle a) => a.AdditiveInverse();

        public static bool operator ==(Angle a1, Angle a2) => a1.EqualTo(a2);

        public static bool operator !=(Angle a1, Angle a2) => !a1.EqualTo(a2);

        public Angle Flip() => new Angle(Radians + Math.PI);

        public Angle Add(Angle t) => new Angle(Radians + t.Radians);

        public Angle AdditiveInverse() => new Angle(-Radians);

        public bool IsObtuse() => Math.Abs(Radians) > Math.PI * 0.5;

        public bool IsAcute() => Radians < Math.PI * 0.5 && Radians > -Math.PI * 0.5;

        public bool EqualTo(Angle t) => Radians == t.Radians;

        public override bool Equals(object obj) => obj is Angle a && EqualTo(a);

        public override int GetHashCode() => Radians.GetHashCode();

        public override string ToString()
        {
            return Radians.ToString();
        }
    }
}
