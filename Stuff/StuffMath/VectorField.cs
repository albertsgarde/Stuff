﻿using System;
using System.Collections.Generic;
using System.Linq;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath
{
    /// <summary>
    ///     A vector field.
    ///     If VarNames = ["x", "y", "z"], then this represents the vector field described by
    ///     V(x,y,z) = (Expressions[0](x,y,z),Expressions[1](x,y,z),Expressions[2](x,y,z))
    /// </summary>
    public class VectorField
    {
        public VectorField(IEnumerable<Expression> exps, IEnumerable<string> varNames)
        {
            if (exps.Count() != varNames.Count())
                throw new ArgumentException("exps and varNames must have the same size.");
            Exps = new Vector<ExpressionField>(exps.Select(exp => new ExpressionField(exp)));
            VarNames = varNames.ToList();
            foreach (var exp in exps)
            {
                var vars = exp.ContainedVariables();
                if (!VarNames.ContainsAll(vars))
                    throw new ArgumentException("The functions should only depend on the given variables. Expression " +
                                                exp + " also depends on variables " +
                                                vars.Where(v => !varNames.Contains(v)));
            }
        }

        /// <summary>
        ///     A series of functions that describe the vector field.
        ///     Each function is associated with the variable in the same place.
        /// </summary>
        public Vector<ExpressionField> Exps { get; }

        /// <summary>
        ///     The variables used in the VectorField.
        /// </summary>
        public IReadOnlyList<string> VarNames { get; }

        public Vector<FDouble> Evaluate(IEnumerable<double> values)
        {
            return Exps.Apply(exp => (FDouble) exp.Evaluate(values.Zip(VarNames, (v, var) => (var, v)).ToDictionary()));
        }
    }
}