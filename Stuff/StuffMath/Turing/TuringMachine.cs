﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Stuff.StuffMath.Turing
{
    public class TuringMachine
    {
        private readonly State[] states;

        /// <summary>
        /// </summary>
        /// <param name="states">Element 0 in the given list, is state 1. Element n is state n+1.</param>
        public TuringMachine(IReadOnlyList<State> states)
        {
            this.states = states.ToArray();
        }

        public State this[int i]
        {
            get
            {
                if (i <= 0)
                    throw new ArgumentException("State numbers start from 1");
                return states[i - 1];
            }
        }

        public string AsString()
        {
            var result = "";
            for (var i = 1; i < states.Length + 1; ++i)
            {
                if (this[i][false].Action != TuringMachineAction.Halt)
                    result += $"q{i}S0{this[i][false].AsString()},";
                if (this[i][true].Action != TuringMachineAction.Halt)
                    result += $"q{i}S1{this[i][true].AsString()},";
                result += Environment.NewLine;
            }

            return result;
        }
    }

    public enum TuringMachineAction
    {
        Left,
        Right,
        Print,
        Erase,
        Halt
    }

    public struct Instruction
    {
        public TuringMachineAction Action { get; }

        public int NextState { get; }

        public Instruction(TuringMachineAction action, int nextState)
        {
            Action = action;
            NextState = nextState;
        }

        public static implicit operator Instruction((TuringMachineAction action, int nextState) instruction)
        {
            return new Instruction(instruction.action, instruction.nextState);
        }

        public string AsString()
        {
            var result = "";
            switch (Action)
            {
                case TuringMachineAction.Erase:
                    result += "S0";
                    break;
                case TuringMachineAction.Print:
                    result += "S1";
                    break;
                case TuringMachineAction.Left:
                    result += "L";
                    break;
                case TuringMachineAction.Right:
                    result += "R";
                    break;
                case TuringMachineAction.Halt:
                    result += "HALT";
                    break;
            }

            result += "q" + NextState;
            return result;
        }
    }

    public class State
    {
        private readonly Instruction one;
        private readonly Instruction zero;

        public State(Instruction zero, Instruction one)
        {
            this.zero = zero;
            this.one = one;
        }

        public State(int index)
        {
            zero = (TuringMachineAction.Halt, index);
            one = (TuringMachineAction.Halt, index);
        }

        public Instruction this[bool symbol] => symbol ? one : zero;

        public Instruction Instruction(bool symbol)
        {
            return this[symbol];
        }
    }
}