﻿using System.Collections.Generic;

namespace Stuff.StuffMath.Turing
{
    public class Instance
    {
        private readonly Tape tape;

        private readonly TuringMachine tm;

        public Instance(TuringMachine tm, IReadOnlyList<bool> arguments) : this(tm, new Tape(arguments))
        {
        }

        public Instance(TuringMachine tm, Tape tape)
        {
            this.tape = tape.Copy();
            this.tm = tm;
            Position = 0;
            State = 1;
            Halted = false;
        }

        public int Position { get; private set; }

        public int State { get; private set; }

        public bool Halted { get; private set; }

        public bool NextStep()
        {
            if (!Halted)
            {
                var inst = tm[State][tape[Position]];
                State = inst.NextState;
                switch (inst.Action)
                {
                    case TuringMachineAction.Halt:
                        Halted = true;
                        return false;
                    case TuringMachineAction.Left:
                        --Position;
                        break;
                    case TuringMachineAction.Right:
                        ++Position;
                        break;
                    case TuringMachineAction.Print:
                        tape.Print(Position);
                        break;
                    case TuringMachineAction.Erase:
                        tape.Erase(Position);
                        break;
                }

                if (Position >= tape.End)
                    tape[Position] = false;
                else if (Position < tape.Start)
                    tape[Position] = false;
            }

            return false;
        }

        public void Run()
        {
            while (!Halted)
                NextStep();
        }

        public int Value()
        {
            var i = Position;
            while (tape[i])
                i++;
            return i - Position;
        }

        public int[] Values()
        {
            var result = new List<int>();
            var curNum = 0;
            for (var i = tape.Start; i < tape.End; ++i)
                if (tape[i])
                {
                    curNum++;
                }
                else if (curNum > 0)
                {
                    result.Add(curNum);
                    curNum = 0;
                }

            if (curNum > 0)
                result.Add(curNum);
            return result.ToArray();
        }

        public string TapeAsString()
        {
            return tape.AsString();
        }

        public string AsString()
        {
            var result = "";
            for (var i = tape.Start; i < tape.End; ++i)
            {
                result += tape[i] ? "1" : "0";
                if (Position == i)
                    result += "[" + State + "]";
            }

            if (Halted)
                result += "HALT!";
            return result;
        }

        public class Tape
        {
            /// <summary>
            ///     All negative indices of the tape.
            /// </summary>
            private readonly List<bool> left;

            /// <summary>
            ///     All positive indices of the tape.
            /// </summary>
            private readonly List<bool> right;

            public Tape(IReadOnlyList<bool> arguments)
            {
                right = new List<bool>(arguments);
                left = new List<bool>();
            }

            public Tape(params int[] arguments)
            {
                right = new List<bool>();
                left = new List<bool>();
                foreach (var arg in arguments)
                {
                    for (var j = 0; j < arg; ++j)
                        right.Add(true);
                    right.Add(false);
                }
            }

            private Tape(Tape tape)
            {
                right = new List<bool>(tape.right);
                left = new List<bool>(tape.left);
            }

            public int Start => -left.Count;

            public int End => right.Count - 1;

            public bool this[int pos]
            {
                get
                {
                    if (pos >= 0)
                    {
                        if (pos >= End)
                            return false;
                        return right[pos];
                    }

                    if (pos < Start)
                        return false;
                    return left[-pos - 1];
                }
                set
                {
                    if (pos >= 0)
                    {
                        while (pos >= End)
                            right.Add(false);
                        right[pos] = value;
                    }
                    else
                    {
                        while (pos < Start)
                            left.Add(false);
                        left[-pos - 1] = value;
                    }
                }
            }

            public void Print(int pos)
            {
                this[pos] = true;
            }

            public void Erase(int pos)
            {
                this[pos] = false;
            }

            public Tape Copy()
            {
                return new Tape(this);
            }

            public string AsString()
            {
                var result = "";
                for (var i = Start; i < End; ++i)
                    result += this[i] ? "1" : "0";
                return result;
            }
        }
    }
}