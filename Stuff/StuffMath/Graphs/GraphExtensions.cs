﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public static class GraphExtensions
    {
        public static IReadOnlyDictionary<GN, int> BFS<G, GN>(this IGraph<G, GN> graph, GN startNode) where G : IGraph<G, GN> where GN : IGraphNode<GN>
        {
            var queue = new Queue<GN>();
            var shortestPath = new Dictionary<GN, int>();
            foreach (var node in graph)
                shortestPath[node] = -1;
            queue.Enqueue(startNode);
            shortestPath[startNode] = 0;
            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                foreach (var neighbour in node.Destinations)
                {
                    if (shortestPath[neighbour] == -1)
                    {
                        shortestPath[neighbour] = shortestPath[node] + 1;
                        queue.Enqueue(neighbour);
                    }
                }
            }

            return shortestPath;
        }

        public static IReadOnlyDictionary<GN, (int, int)> DFS<G, GN>(this IGraph<G, GN> graph,
            GN startNode, bool startAgain) where G : IGraph<G, GN> where GN : IGraphNode<GN>
        {
            if (!graph.Contains(startNode))
                throw new ArgumentException("startNode must be part of the graph.");

            var stack = new Stack<GN>();
            var visitedNodes = new HashSet<GN>();
            var curTime = -1;
            var discoveryTimes = new Dictionary<GN, int>();
            var finishingTimes = new Dictionary<GN, int>();

            do
            {
                stack.Push(startNode);
                visitedNodes.Add(startNode);
                discoveryTimes[startNode] = ++curTime;

                while (stack.Count > 0)
                {
                    var node = stack.Peek();
                    var neighbourFound = false;
                    foreach (var neighbour in node.Destinations)
                    {
                        if (!visitedNodes.Contains(neighbour))
                        {
                            stack.Push(neighbour);
                            visitedNodes.Add(neighbour);
                            discoveryTimes[neighbour] = ++curTime;
                            neighbourFound = true;
                            break;
                        }
                    }

                    if (!neighbourFound)
                        finishingTimes[stack.Pop()] = ++curTime;
                }
            } while (startAgain && (startNode = graph.FirstOrDefault(n => !visitedNodes.Contains(n))) != null);

            var result = new Dictionary<GN, (int discovery, int finishing)>();
            foreach (var node in visitedNodes)
                result[node] = (discoveryTimes[node], finishingTimes[node]);
            return result;
        }

        public static IEnumerable<GN> DFSOrder<G, GN>(this G graph, GN startNode, bool startAgain) where G : IGraph<G, GN> where GN : IGraphNode<GN>
        {
            var timeStamps = graph.DFS(startNode, startAgain);
            var result = new GN[timeStamps.Keys.Count() * 2];
            foreach (var node in timeStamps.Keys)
            {
                result[timeStamps[node].Item1] = node;
                result[timeStamps[node].Item2] = node;
            }

            return result;
        }

        public static IEnumerable<GN> DFSDiscoveryOrder<G, GN>(this G graph, GN startNode, bool startAgain) where G : IGraph<G, GN> where GN : IGraphNode<GN>
        {
            var timeStamps = graph.DFS(startNode, startAgain);
            var order = new GN[timeStamps.Keys.Count() * 2];
            foreach (var node in timeStamps.Keys)
                order[timeStamps[node].Item1] = node;
            return order.Where(node => node != null);
        }

        public static IEnumerable<GN> DFSFinishOrder<G, GN>(this G graph, GN startNode, bool startAgain) where G : IGraph<G, GN> where GN : IGraphNode<GN>
        {
            var timeStamps = graph.DFS(startNode, startAgain);
            var order = new GN[timeStamps.Keys.Count() * 2];
            foreach (var node in timeStamps.Keys)
                order[timeStamps[node].Item2] = node;
            return order.Where(node => node != null);
        }

        public static bool IsCyclic<G, GN>(this IGraph<G, GN> graph) where G : IGraph<G, GN> where GN : IGraphNode<GN>
        {
            var startNode = graph.First();

            var stack = new Stack<GN>();
            var visitedNodes = new HashSet<GN>();

            do
            {
                stack.Push(startNode);
                visitedNodes.Add(startNode);

                while (stack.Count > 0)
                {
                    var node = stack.Peek();
                    var neighbourFound = false;
                    foreach (var neighbour in node.Destinations)
                    {
                        if (stack.Contains(neighbour))
                            return true;
                        if (!visitedNodes.Contains(neighbour))
                        {
                            stack.Push(neighbour);
                            visitedNodes.Add(neighbour);
                            neighbourFound = true;
                            break;
                        }
                    }

                    if (!neighbourFound)
                        stack.Pop();
                }
            } while ((startNode = graph.FirstOrDefault(n => !visitedNodes.Contains(n))) != null);

            return false;
        }

        private class ShortestPathHeapNode<GN> : IComparable<ShortestPathHeapNode<GN>> where GN : IWeightedGraphNode<GN>
        {
            public GN Node { get; }

            public double Distance { get; }

            public ShortestPathHeapNode(GN node, double distance)
            {
                Node = node;
                Distance = distance;
            }

            public int CompareTo(ShortestPathHeapNode<GN> other)
            {
                if (Distance < other.Distance)
                    return -1;
                return Distance > other.Distance ? 1 : 0;
            }
        }

        public static IReadOnlyDictionary<GN, (GN, double)> ShortestPathsPositive<G, GN>(this G graph, GN startNode)
            where G : IGraph<G, GN> where GN : IWeightedGraphNode<GN>
        {
            var result = graph.Nodes.ToDictionary(node => node, node => (node: node, distance: double.PositiveInfinity));
            result[startNode] = (startNode, 0);

            var heap = new MaxHeap<ShortestPathHeapNode<GN>>();
            heap.Insert(new ShortestPathHeapNode<GN>(startNode, 0));

            while (true)
            {
                var curNode = heap.Extract();
                foreach (var dest in curNode.Node.WeightedDestinations)
                {
                    if (result[dest.Item1].distance == double.PositiveInfinity)
                        ;
                }
            }
        }
    }
}
