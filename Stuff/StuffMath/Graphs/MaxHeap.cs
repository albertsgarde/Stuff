﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace Stuff.StuffMath.Graphs
{
    public class MaxHeap<T> : IEnumerable<T> where T : IComparable<T>
    {
        private readonly List<T> heap;

        public MaxHeap()
        {
            heap = new List<T>();
        }

        public MaxHeap(int capacity)
        {
            heap = new List<T>(capacity);
        }

        public int Count => heap.Count;

        private static int Parent(int index) => (int)Math.Floor((double)(index + 1) / 2) - 1;

        private static int LeftChild(int index) => index * 2 + 1;

        private static int RightChild(int index) => index * 2 + 2;

        private void Swap(int index1, int index2)
        {
            var temp = heap[index1];
            heap[index1] = heap[index2];
            heap[index2] = temp;
        }

        private void BubbleUp(int index)
        {
            int parentIndex;
            while (index != 0 && heap[index].CompareTo(heap[parentIndex = Parent(index)]) > 0)
            {
                Swap(index, parentIndex);
                index = parentIndex;
            }
        }

        private void BubbleDown(int index)
        {
            while (true)
            {
                var left = LeftChild(index);
                var right = RightChild(index);

                var largest = index;
                if (Count > left && heap[largest].CompareTo(heap[left]) < 0) largest = left;
                if (Count > right && heap[largest].CompareTo(heap[right]) < 0) largest = right;
                if (largest != index)
                {
                    Swap(index, largest);
                    index = largest;
                    continue;
                }
                break;
            }
        }

        public T Root
        {
            get => heap.First();
            private set => heap[0] = value;
        }

        public void Insert(T value)
        {
            heap.Add(value);
            BubbleUp(heap.Count - 1);
        }

        public T Extract()
        {
            if (heap.Count == 0)
                throw new InvalidOperationException("Cannot extract an element from an empty heap.");
            var result = Root;
            Root = heap.Last();
            heap.RemoveAt(Count - 1);
            if (Count > 0)
                BubbleDown(0);
            return result;
        }

        public string AsString() => this.Aggregate("", (total, value) => total + value + " ").TrimEnd();

        public IEnumerator<T> GetEnumerator() => heap.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
