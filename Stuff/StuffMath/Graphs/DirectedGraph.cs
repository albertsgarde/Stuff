﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class DirectedGraph : IGraph<DirectedGraph, GraphNode<int>>
    {
        private readonly List<GraphNode<int>> nodes;

        public IEnumerable<GraphNode<int>> Nodes => nodes;

        public GraphNode<int> this[int node] => nodes[node];

        public int NumberOfNodes => nodes.Count;

        public int NumberOfEdges { get; private set; }

        public IEnumerable<(GraphNode<int>, GraphNode<int>)> Edges => Nodes.Aggregate(new LinkedList<(GraphNode<int>, GraphNode<int>)>().AsEnumerable(),
            (total, node) => total.Concat(node.Destinations.Select(dest => (node, dest))));

        public DirectedGraph(int nodes)
        {
            this.nodes = new List<GraphNode<int>>(nodes);
            for (var i = 0; i < nodes; ++i)
                this.nodes.Add(new GraphNode<int>(i));
            NumberOfEdges = 0;
        }

        public GraphNode<int> AddNode()
        {
            nodes.Add(new GraphNode<int>(NumberOfNodes));
            return nodes.Last();
        }

        public void AddEdge(GraphNode<int> node1, GraphNode<int> node2)
        {
            node1.AddEdge(node2);
            ++NumberOfEdges;
        }

        public void AddEdge(int node1, int node2)
        {
            AddEdge(this[node1], this[node2]);
        }

        public bool AreNeighbours(GraphNode<int> node1, GraphNode<int> node2) => node1.IsNeighbour(node2);

        public bool AreNeighbours(int node1, int node2) => AreNeighbours(this[node1], this[node2]);

        public DirectedGraph Reverse()
        {
            var result = new DirectedGraph(NumberOfNodes);
            foreach (var edge in Edges)
                result.AddEdge(edge.Item2.Value, edge.Item1.Value);
            return result;
        }

        public IEnumerable<GraphNode<int>> TopologicalSort()
        {
            if (this.IsCyclic())
                throw new InvalidOperationException("Cannot perform topological sort on cyclic graph.");
            return this.DFSFinishOrder(this.First(), true).Reverse();
        }

        public DirectedGraph TopologicalSortGraph()
        {
            if (this.IsCyclic())
                throw new InvalidOperationException("Cannot perform topological sort on cyclic graph.");

            var order = this.DFSFinishOrder(this.First(), true).Reverse();
            var i = -1;
            var numbering = new int[NumberOfNodes];
            foreach (var node in order)
                numbering[node.Value] = ++i;
            var result = new DirectedGraph(NumberOfNodes);
            foreach (var node in order)
            {
                foreach (var dest in node.Destinations)
                    result.AddEdge(numbering[node.Value], numbering[dest.Value]);
            }

            return result;
        }

        public IEnumerator<GraphNode<int>> GetEnumerator() => nodes.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
