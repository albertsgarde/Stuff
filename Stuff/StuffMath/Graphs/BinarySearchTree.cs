﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class BinarySearchTree
    {
        public Node Root { get; }

        public class Node
        {
            public int Value { get; }

            public Node Parent { get; private set; }

            public Node Left { get; private set; }

            public Node Right { get; private set; }

            public Node(int value, Node parent, Node left, Node right)
            {
                Value = value;
                Parent = parent;
                Left = left;
                Right = right;
            }

            public Node Search(int value)
            {
                if (Value == value)
                    return this;
                if (Value > value)
                    return Left?.Search(value);
                return Right?.Search(value);
            }

            public Node Insert(int value)
            {
                if (value <= Value)
                {
                    if (Left == null)
                        return Left = new Node(value, this, null, null);
                    return Left.Insert(value);
                }
                if (Right == null)
                    return Right = new Node(value, this, null, null);
                return Right.Insert(value);
            }

            public Node Predecessor(int value)
            {
                if (value == Value)
                    return this;
                if (value < Value)
                    return Left?.Predecessor(value);
                return Right?.Predecessor(value) ?? this;
            }

            public Node Successor(int value)
            {
                if (value == Value)
                    return this;
                if (value > Value)
                    return Right?.Predecessor(value);
                return Left?.Predecessor(value) ?? this;
            }

            public IEnumerable<Node> PreorderTreeWalk()
            {
                return new List<Node> { this }.Concat(Left.PreorderTreeWalk()).Concat(Right.InorderTreeWalk());
            }

            public IEnumerable<Node> InorderTreeWalk()
            {
                return Left.InorderTreeWalk().Concat(new List<Node> { this }).Concat(Right.InorderTreeWalk());
            }

            public IEnumerable<Node> PostorderTreeWalk()
            {
                return Left.PreorderTreeWalk().Concat(Right.InorderTreeWalk()).Concat(new List<Node> { this });
            }

            public int Height()
            {
                if (Left == null && Right == null)
                    return 1;
                if (Left == null)
                    return Right.Height() + 1;
                if (Right == null)
                    return Left.Height() + 1;
                return Math.Max(Right.Height(), Left.Height()) + 1;
            }
        }

        public Node Search(int value) => Root.Search(value);

        public Node Insert(int value) => Root.Insert(value);

        public Node Predecessor(int value) => Root.Predecessor(value);

        public Node Successor(int value) => Root.Successor(value);

        public IEnumerable<Node> PreorderTreeWalk() => Root.PreorderTreeWalk();

        public IEnumerable<Node> InorderTreeWalk() => Root.InorderTreeWalk();

        public IEnumerable<Node> PostorderTreeWalk() => Root.PostorderTreeWalk();

        public int Height() => Root.Height();

        public IEnumerable<Node> InorderTreeWalkNonRecursive()
        {
            var cv = Root;
            while (cv.Left != null) // Move cv to the smallest element.
                cv = Root;

            var from = 0; // 0: parent, 1: left, 2: right.
            var done = false;
            while (!done)
            {
                switch (from)
                {
                    case 0:
                        if (cv.Left != null)
                        {
                            from = 0;
                            cv = cv.Left;
                        }
                        else
                        {
                            yield return cv;
                            if (cv.Right != null)
                            {
                                from = 0;
                                cv = cv.Right;
                            }
                            else if (cv.Parent != null)
                                done = true;
                            else
                            {
                                from = cv == cv.Parent.Left ? 1 : 2;
                                cv = cv.Parent;
                            }
                        }
                        break;
                    case 1:
                        yield return cv;
                        if (cv.Right != null)
                        {
                            from = 0;
                            cv = cv.Right;
                        }
                        else if (cv.Parent == null)
                            done = true;
                        else
                        {
                            from = cv == cv.Parent.Left ? 1 : 2;
                            cv = cv.Parent;
                        }
                        break;
                    case 2:
                        if (cv.Parent == null)
                            done = true;
                        else
                        {
                            from = cv == cv.Parent.Left ? 1 : 2;
                            cv = cv.Parent;
                        }
                        break;
                    default:
                        throw new Exception("Code should never get here.");
                }
            }
        }
    }
}
