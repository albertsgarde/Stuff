﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class Graph : IGraph<Graph, GraphNode<int>>
    {
        private readonly List<GraphNode<int>> nodes;

        public IEnumerable<GraphNode<int>> Nodes => nodes;

        public GraphNode<int> this[int node] => nodes[node];

        public int NumberOfNodes => nodes.Count;

        public int NumberOfEdges { get; private set; }

        public Graph(int nodes)
        {
            this.nodes = new List<GraphNode<int>>(nodes);
            for (var i = 0; i < nodes; ++i)
                this.nodes.Add(new GraphNode<int>(i));
            NumberOfEdges = 0;
        }

        public GraphNode<int> AddNode()
        {
            nodes.Add(new GraphNode<int>(NumberOfNodes));
            return nodes.Last();
        }

        public void AddEdge(GraphNode<int> node1, GraphNode<int> node2)
        {
            node1.AddEdge(node2);
            node2.AddEdge(node1);
            ++NumberOfEdges;
        }

        public void AddEdge(int node1, int node2)
        {
            AddEdge(this[node1], this[node2]);
        }

        public bool AreNeighbours(GraphNode<int> node1, GraphNode<int> node2) => node1.IsNeighbour(node2);

        public bool AreNeighbours(int node1, int node2) => AreNeighbours(this[node1], this[node2]);

        public IEnumerator<GraphNode<int>> GetEnumerator() => nodes.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
