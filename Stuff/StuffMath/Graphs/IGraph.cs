﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public interface IGraph<G, out GN> : IEnumerable<GN> where G : IGraph<G, GN> where GN : IGraphNode<GN>
    {
        int NumberOfNodes { get; }

        int NumberOfEdges { get; }

        IEnumerable<GN> Nodes { get; }
    }

    public interface IGraphNode<out GN> where GN : IGraphNode<GN>
    {
        IEnumerable<GN> Destinations { get; }

        int OutDegree { get; }
    }

    public interface IBiGraphNode<GN> : IGraphNode<GN> where GN : IBiGraphNode<GN>
    {
        IEnumerable<GN> Froms { get; }

        int InDegree { get; }
    }

    public interface IMutableGraphNode<GN> : IGraphNode<GN> where GN : IMutableGraphNode<GN>
    {
        void AddDestination(GN destination);
    }

    public interface IMutableBiGraphNode<GN> : IBiGraphNode<GN> where GN : IMutableBiGraphNode<GN>
    {
        void AddFrom(GN from);
    }

    public interface IValueGraphNode<out GN, V> : IGraphNode<GN> where GN : IValueGraphNode<GN, V>
    {
        V Value { get; }
    }

    public interface INumberedGraphNode<out GN> : IGraphNode<GN> where GN : INumberedGraphNode<GN>
    {
        int Number { get; }
    }

    public interface IWeightedGraphNode<GN> : IGraphNode<GN> where GN : IWeightedGraphNode<GN>
    {
        IEnumerable<(GN, int)> WeightedDestinations { get; }
    }

    public interface IMutableWeightedGraphNode<GN> : IWeightedGraphNode<GN> where GN : IMutableWeightedGraphNode<GN>
    {
        void AddDestination(GN destination, int weight);
    }

    public static class BiGraphNodeExtensions
    {
        public static bool AreNeighbours<GN>(this IBiGraphNode<GN> node1, GN node2) where GN : IBiGraphNode<GN> =>
            node1.Destinations.Contains(node2) || node1.Froms.Contains(node2);
    }
}
