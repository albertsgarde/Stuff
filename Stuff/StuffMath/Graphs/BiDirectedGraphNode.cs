﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class BiDirectedGraphNode : IBiGraphNode<BiDirectedGraphNode>, INumberedGraphNode<BiDirectedGraphNode>
    {
        private HashSet<BiDirectedGraphNode> froms;

        private HashSet<BiDirectedGraphNode> destinations;

        public IEnumerable<BiDirectedGraphNode> Froms => froms;

        public int InDegree => froms.Count;

        public IEnumerable<BiDirectedGraphNode> Destinations => destinations;

        public int OutDegree => destinations.Count;

        public int Number { get; }
        
        public BiDirectedGraphNode(int number)
        {
            froms = new HashSet<BiDirectedGraphNode>();
            destinations = new HashSet<BiDirectedGraphNode>();
            Number = number;
        }

        public void AddDestination(BiDirectedGraphNode destination)
        {
            if (destinations.Contains(destination))
                throw new ArgumentException("Node already has destination.");
            destinations.Add(destination);
        }

        public void AddFrom(BiDirectedGraphNode from)
        {
            if (froms.Contains(from))
                throw new ArgumentException("Node already has from.");
            froms.Add(from);
        }

        public void RemoveDestination(BiDirectedGraphNode destination)
        {
            if (!destinations.Contains(destination))
                throw new ArgumentException("Cannot delete destination that doesn't exist.");
            destinations.Remove(destination);
        }

        public void RemoveFrom(BiDirectedGraphNode from)
        {
            if (!froms.Contains(from))
                throw new ArgumentException("Cannot delete from that doesn't exist.");
            froms.Remove(from);
        }
    }

    public class BiDirectedGraphNode<V> : IBiGraphNode<BiDirectedGraphNode<V>>, IValueGraphNode<BiDirectedGraphNode<V>, V>
    {
        public V Value { get; }

        private HashSet<BiDirectedGraphNode<V>> froms;

        private HashSet<BiDirectedGraphNode<V>> destinations;

        public IEnumerable<BiDirectedGraphNode<V>> Froms => froms;

        public int InDegree => froms.Count;

        public IEnumerable<BiDirectedGraphNode<V>> Destinations => destinations;

        public int OutDegree => destinations.Count;

        public BiDirectedGraphNode(V value)
        {
            froms = new HashSet<BiDirectedGraphNode<V>>();
            destinations = new HashSet<BiDirectedGraphNode<V>>();
            Value = value;
        }

        public void AddDestination(BiDirectedGraphNode<V> destination)
        {
            if (destinations.Contains(destination))
                throw new ArgumentException("Node already has destination.");
            destinations.Add(destination);
        }

        public void AddFrom(BiDirectedGraphNode<V> from)
        {
            if (froms.Contains(from))
                throw new ArgumentException("Node already has from.");
            froms.Add(from);
        }

        public void RemoveDestination(BiDirectedGraphNode<V> destination)
        {
            if (!destinations.Contains(destination))
                throw new ArgumentException("Cannot delete destination that doesn't exist.");
            destinations.Remove(destination);
        }

        public void RemoveFrom(BiDirectedGraphNode<V> from)
        {
            if (!froms.Contains(from))
                throw new ArgumentException("Cannot delete from that doesn't exist.");
            froms.Remove(from);
        }
    }
}
