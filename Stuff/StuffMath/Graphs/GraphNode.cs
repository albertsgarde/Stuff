﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class GraphNode<V> : IValueGraphNode<GraphNode<V>, V>, IMutableGraphNode<GraphNode<V>>
    {
        public V Value { get; }

        private readonly List<GraphNode<V>> adjacencies;

        public IReadOnlyList<GraphNode<V>> Adjacencies => adjacencies;

        public IEnumerable<GraphNode<V>> Destinations => adjacencies;

        public int OutDegree => Destinations.Count();

        public GraphNode(V value)
        {
            Value = value;
            adjacencies = new List<GraphNode<V>>();
        }

        public void AddEdge(GraphNode<V> destination)
        {
            adjacencies.Add(destination);
        }

        public bool IsNeighbour(GraphNode<V> node) => adjacencies.Contains(node);

        void IMutableGraphNode<GraphNode<V>>.AddDestination(GraphNode<V> destination) => AddEdge(destination);
    }
}
