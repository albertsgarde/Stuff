﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class DisjointSets : IUnionFind
    {
        private readonly int[] id;

        private readonly int[] sizes;

        public int Count { get; private set; }

        public DisjointSets(int n)
        {
            id = new int[n];
            sizes = new int[n];
            for (var i = 0; i < n; ++i)
            {
                id[i] = i;
                sizes[i] = 1;
            }
        }

        public int Find(int p)
        {
            if (p == id[p])
                return p;
            return id[p] = Find(id[p]);
        }

        public bool Connected(int p, int q) => Find(p) == Find(q);

        public void Union(int p, int q)
        {
            var pId = Find(p);
            var qId = Find(q);
            var pSize = sizes[pId];
            var qSize = sizes[qId];
            if (pId != qId)
            {
                if (pSize < qSize)
                {
                    id[pId] = qId;
                    sizes[qId] += sizes[pId];
                }
                else
                {
                    id[qId] = pId;
                    sizes[pId] += sizes[qId];
                }

                --Count;
            }
        }
    }
}
