﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Graphs
{
    public class BiDirectedGraph : IGraph<BiDirectedGraph, BiDirectedGraphNode>
    {
        private readonly List<BiDirectedGraphNode> nodes;

        public IEnumerable<BiDirectedGraphNode> Nodes => nodes;

        public BiDirectedGraphNode this[int node] => nodes[node];

        public int NumberOfNodes => nodes.Count;

        public int NumberOfEdges { get; private set; }

        public IEnumerable<(BiDirectedGraphNode, BiDirectedGraphNode)> Edges => Nodes.Aggregate(new LinkedList<(BiDirectedGraphNode, BiDirectedGraphNode)>().AsEnumerable(),
            (total, node) => total.Concat(node.Destinations.Select(dest => (node, dest))));

        public BiDirectedGraph(int nodes)
        {
            this.nodes = new List<BiDirectedGraphNode>(nodes);
            for (var i = 0; i < nodes; ++i)
                this.nodes.Add(new BiDirectedGraphNode(i));
        }

        private void AddEdge(BiDirectedGraphNode node1, BiDirectedGraphNode node2)
        {
            node1.AddDestination(node2);
            node2.AddFrom(node1);
            ++NumberOfEdges;
        }

        public void AddEdge(int node1, int node2)
        {
            AddEdge(this[node1], this[node2]);
        }

        public bool AreNeighbours(BiDirectedGraphNode node1, BiDirectedGraphNode node2) => node1.AreNeighbours(node2);

        public bool AreNeighbours(int node1, int node2) => this[node1].AreNeighbours(this[node2]);

        public void RemoveNode(BiDirectedGraphNode node)
        {
            foreach (var from in node.Froms)
                from.RemoveDestination(node);
            foreach (var destination in node.Destinations)
                destination.RemoveFrom(node);
        }

        public IEnumerable<BiDirectedGraphNode> TopologicalSort()
        {
            if (this.IsCyclic())
                throw new InvalidOperationException("Cannot perform topological sort on cyclic graph.");
            return this.DFSFinishOrder(this.First(), true).Reverse();
        }

        public BiDirectedGraph TopologicalSortGraph()
        {
            if (this.IsCyclic())
                throw new InvalidOperationException("Cannot perform topological sort on cyclic graph.");


            var order = this.DFSFinishOrder(this.First(), true).Reverse();
            var i = -1;
            var numbering = new int[NumberOfNodes];
            foreach (var node in order)
                numbering[node.Number] = ++i;
            var result = new BiDirectedGraph(NumberOfNodes);
            foreach (var node in order)
            {
                foreach (var dest in node.Destinations)
                    result.AddEdge(numbering[node.Number], numbering[dest.Number]);
            }

            return result;
        }

        public IEnumerator<BiDirectedGraphNode> GetEnumerator() => nodes.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
