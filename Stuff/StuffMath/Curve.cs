﻿using System;
using System.Linq;
using Stuff.StuffMath.Structures;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.LinearAlgebra;

namespace Stuff.StuffMath
{
    public class Curve
    {
        /// <exception cref="ArgumentException">If the given functions contain other variables that the specified.</exception>
        /// <param name="expVec">The functions that describe the Curve.</param>
        /// <param name="var">The name of the independent variable in the functions.</param>
        public Curve(HilbertVector<ExpressionField> expVec, string var = "t")
        {
            Var = var;
            ExpVec = expVec;
            for (var i = 0; i < expVec.Size; ++i)
            {
                var vars = expVec[i].ContainedVariables();
                if (vars.Count > 1 || vars.Count == 1 && vars.Single() != var)
                    throw new ArgumentException("Curves should only depend on the given variable. Element of vector: " +
                                                i);
            }
        }

        /// <summary>
        ///     The name of the independent variable in the functions.
        /// </summary>
        public string Var { get; }

        /// <summary>
        ///     The functions that describe the Curve.
        /// </summary>
        public HilbertVector<ExpressionField> ExpVec { get; }

        /// <summary>
        ///     The dimension of the Curve.
        /// </summary>
        public int Dim => ExpVec.Size;

        /// <returns>The position of the curve at the specified point.</returns>
        public HilbertVector<FDouble> Value(double t)
        {
            return ExpVec.Apply(v => (FDouble) v.Evaluate((Var, t)));
        }

        /// <returns>A vector function that describes the tangent of the curve at all points.</returns>
        public HilbertVector<ExpressionField> TangentCurve()
        {
            return ExpVec.Apply(v => v.Differentiate(Var));
        }

        /// <returns>The tangent of the curve at the specified point.</returns>
        public HilbertVector<FDouble> Tangent(double t)
        {
            return ExpVec.Apply(v => (FDouble) v.Differentiate(Var).Evaluate((Var, t)));
        }

        /// <returns>The length of the tangent at all points.</returns>
        public ExpressionField Jacobi()
        {
            return TangentCurve().Length;
        }

        /// <returns>The length of the tangent at the specified point.</returns>
        public FDouble Jacobi(double t)
        {
            return Tangent(t).Length;
        }
    }
}