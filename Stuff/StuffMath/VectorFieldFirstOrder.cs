﻿using System;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath
{
    public class VectorFieldFirstOrder<F> where F : IHilbertField<F>, new()
    {
        /// <exception cref="ArgumentException">If the matrix isn't square or the constant vector doesn't have the same dimensions.</exception>
        /// <param name="a">The matrix.</param>
        /// <param name="b">The constant vector.</param>
        public VectorFieldFirstOrder(Matrix<F> a, Vector<F> b)
        {
            A = a;
            if (!a.IsSquare())
                throw new ArgumentException("A vector fields matrix should be square.");
            B = b;
            if (a.N != b.Size)
                throw new ArgumentException("The constant vector should have same dimensions as the matrix.");
        }

        /// <summary>
        ///     Coefficient matrix.
        /// </summary>
        public Matrix<F> A { get; }

        /// <summary>
        ///     The constant.
        /// </summary>
        public Vector<F> B { get; }

        /// <summary>
        ///     The dimension of the vector field.
        /// </summary>
        public int Dim => B.Size;

        /// <exception cref="ArgumentException">If the point doesn't have the same dimensionality as the vector field.</exception>
        /// <param name="point">The point whose vector to return.</param>
        /// <returns>The value of the vector field at the given point.</returns>
        public Vector<F> Value(Vector<F> point)
        {
            return A * point + B;
        }

        /// <summary>
        ///     Multiplies every vector in the field by the specified vector.
        /// </summary>
        /// <returns>m*V</returns>
        public VectorFieldFirstOrder<F> Multiply(Matrix<F> m)
        {
            return new VectorFieldFirstOrder<F>(m * A, m * B);
        }
    }
}