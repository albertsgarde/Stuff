﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath
{
    public class VectorFunction : IVectorFunction
    {
        public HilbertVector<ExpressionField> Functions { get; }

        public ImmutableList<string> VariableOrder { get; }

        public VectorFunction(HilbertVector<ExpressionField> functions, ImmutableList<string> variableOrder)
        {
            VariableOrder = variableOrder;
            Functions = functions;
            if (VariableOrder.Any(v => VariableOrder.Count(var => var == v) > 1))
                throw new ArgumentException("variableOrder may not contain duplicates.");
            if (Functions.Any(func => !VariableOrder.ContainsAll(func.ContainedVariables())))
                throw new ArgumentException("variableOrder must contain all variables in all functions.");
        }

        public VectorFunction(HilbertVector<ExpressionField> functions, params string[] variableOrder) : this(functions,
            variableOrder.ToImmutableList())
        {

        }

        public VectorFunction(HilbertVector<ExpressionField> functions, IReadOnlyList<string> variableOrder) : this(functions,
            variableOrder.ToImmutableList())
        {

        }

        public static VectorFunction New<E>(IReadOnlyList<IExpression<E>> functions,
            ImmutableList<string> variableOrder) where E : IExpression<E>
        {
            return new VectorFunction(new HilbertVector<ExpressionField>(functions.Select(e => (ExpressionField)e.Exp)), variableOrder);
        }

        public int Arguments => VariableOrder.Count;

        public int Outputs => Functions.Count;

        public IVectorFunction Combine(IVectorFunction vf)
        {
            throw new NotImplementedException();
        }

        public string ToLatex()
        {
            return Functions.ToLatex();
        }
    }
}
