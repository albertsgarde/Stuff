﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Stuff.StuffMath.Old;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.LinearAlgebra
{
    public class Matrix<F> : IMatrix<Matrix<F>, F> where F : IField<F>, ILatexable, new()
    {
        private readonly ImmutableList<Vector<F>> rows;

        public int M { get; }

        public int N { get; }

        public Matrix<F> ZERO => this.NullMatrix();

        public F FIELD_ZERO => new F();

        public F FIELD_ONE => new F().ONE;

        public Matrix(ImmutableList<Vector<F>> rows)
        {
            (this.rows, M, N) = Constructor(rows);
        }

        public Matrix(IEnumerable<IEnumerable<F>> rows) : this(rows.Select(r => new Vector<F>(r)).ToImmutableList())
        {

        }

        public Matrix(params Vector<F>[] rows) : this(rows.ToImmutableList())
        {
        }

        public Matrix(IEnumerable<Vector<F>> rows) : this(rows.ToImmutableList())
        {
        }

        public Matrix(IEnumerable<F> values, int rows)
        {
            if (values.Count() % rows != 0)
                throw new ArgumentException(
                    $"The number of values({values.Count()}) must be divisible by the number of rows({rows}).");

            M = rows;
            N = values.Count() / rows;
            var list = new Vector<F>[M];
            for (var i = 0; i < M; ++i)
                list[i] = new Vector<F>(values.Skip(i * N).Take(N));

            (this.rows, M, N) = Constructor(list.ToImmutableList());
        }

        private static (ImmutableList<Vector<F>> rows, int n, int m) Constructor(ImmutableList<Vector<F>> rows)
        {
            var m = rows.Count;
            if (m == 0)
                throw new ArgumentException("Matrices cannot have 0 dimensions.");
            var n = rows[0].Count;
            if (n == 0)
                throw new ArgumentException("Matrices cannot have 0 dimensions.");
            for (var i = 1; i < rows.Count; ++i)
                if (rows[i].Count != n)
                    throw new ArgumentException(
                        $"All rows must have the same length. The first row has length {n}, but row {i} has length {rows[i].Count}.");
            return (rows, m, n);
        }

        public Matrix<F> New(IReadOnlyList<IReadOnlyList<F>> rows) => new Matrix<F>(rows);

        public F this[int row, int column] => rows[row][column];

        public Vector<F> this[int row] => rows[row];

        public ImmutableList<F> Row(int row) => rows[row].ToImmutableList();

        public ImmutableList<F> Column(int column) => rows.Select(r => r[column]).ToImmutableList();

        public ImmutableList<ImmutableList<F>> Rows() => rows.Select(r => r.ToImmutableList()).ToImmutableList();

        public ImmutableList<ImmutableList<F>> Columns()
        {
            var result = new ImmutableList<F>[N];
            for (var i = 0; i < N; ++i)
                result[i] = Column(i);
            return result.ToImmutableList();
        }

        public Matrix<F> Add(Matrix<F> m) => new Matrix<F>(rows.Zip(m.rows).Select(r => r.Item1.Zip(r.Item2).Select(f => f.Item1.Multiply(f.Item2))));

        public Matrix<F> AdditiveInverse() => this.Multiply(FIELD_ONE.AdditiveInverse());

        public static bool operator ==(Matrix<F> m1, Matrix<F> m2) => m1.EqualTo(m2);

        public static bool operator !=(Matrix<F> m1, Matrix<F> m2) => !m1.EqualTo(m2);

        public static Matrix<F> operator +(Matrix<F> m1, Matrix<F> m2) => m1.Add(m2);

        public static Matrix<F> operator -(Matrix<F> m1, Matrix<F> m2) => m1.Subtract(m2);

        public static Matrix<F> operator *(Matrix<F> m, F f) => m.Multiply(f);

        public static Matrix<F> operator *(F f, Matrix<F> m) => m.Multiply(f);

        public static Vector<F> operator *(Matrix<F> m, Vector<F> v)
        {
            if (m.N != v.Count())
                throw new ArgumentException("The vector must have m.N size.");
            var result = new F[m.M];
            for (var j = 0; j < m.M; ++j)
                result[j] = m[j].Zip(v, (a, b) => (a, b))
                    .Aggregate(new F(), (a, f) => a.Add(f.a.Multiply(f.b)));
            return new Vector<F>(result);
        }

        public static Matrix<F> operator *(Matrix<F> m1, Matrix<F> m2) => m1.Multiply(m2);

        public static Matrix<F> UnitMatrix(int dim)
        {
            var result = new Vector<F>[dim];
            for (var j = 0; j < dim; ++j)
            {
                var row = new F[dim];
                for (var i = 0; i < dim; ++i)
                    row[i] = i == j ? new F().ONE : new F();
                result[j] = new Vector<F>(row);
            }

            return new Matrix<F>(result);
        }

        public static Matrix<F> DiagonalMatrix(IEnumerable<F> values)
        {
            var dim = values.Count();
            var result = ContainerUtils.UniformArray(ContainerUtils.UniformArray(new F(), dim), dim);
            var i = 0;
            foreach (var f in values)
            {
                result[i][i] = f;
                ++i;
            }

            return new Matrix<F>(result);
        }

        public static Matrix<F> DiagonalMatrix(params F[] values)
        {
            return DiagonalMatrix(values.AsEnumerable());
        }

        public bool EqualTo(Matrix<F> m)
        {
            if (N != m.N || M != m.M)
                throw new ArgumentException("The two matrices must have the same dimensions.");
            return Rows().Zip(m.Rows())
                .All(r => r.Item1.Zip(r.Item2).All(f => f.Item1.EqualTo(f.Item2)));
        }

        public override bool Equals(object obj) => obj is Matrix<F> m && m.EqualTo(this);

        public override int GetHashCode()
        {
            return Misc.HashCode(17, 23, rows.Aggregate(new List<F>().AsEnumerable(), (total, row) => total.Concat(row)));
        }

        public string ToLatex()
        {
            var result = "\\left[\\begin{array}{" + Misc.UniformString(N, 'c') + "}";
            foreach (var v in rows)
            {
                result += v.Aggregate("", (total, f) => total + f.ToLatex() + "&").CutEnd(1) + "\\\\";
            }

            return result.Substring(0, result.Length - 2) + "\\end{array}\\right]";
        }
    }
}