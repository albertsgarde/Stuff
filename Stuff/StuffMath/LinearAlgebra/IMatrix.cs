﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Old;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.LinearAlgebra
{
    public interface IMatrix<Ma, F> : IGroup<Ma>, ILatexable where Ma : IMatrix<Ma, F> where F : IField<F>, new()
    {
        /// <summary>
        /// The number of rows in the matrix.
        /// </summary>
        int M { get; }

        /// <summary>
        /// The number of columns in the matrix.
        /// </summary>
        int N { get; }
        
        /// <returns>The value at the given row and column.</returns>
        F this[int row, int column] { get; }

        ImmutableList<F> Row(int row);

        ImmutableList<F> Column(int column);

        ImmutableList<ImmutableList<F>> Rows();

        ImmutableList<ImmutableList<F>> Columns();

        /// <summary>
        /// The multiplicative identity of the field.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        F FIELD_ONE { get; }

        /// <summary>
        /// The additive identity of the field.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        F FIELD_ZERO { get; }
        
        /// <param name="matrix">Row major representation of the matrix.</param>
        Ma New(IReadOnlyList<IReadOnlyList<F>> matrix);
    }

    public static class MatrixExtensions
    {
        public static Ma Multiply<Ma, F>(this IMatrix<Ma, F> m, F f)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new() => m.Apply(element => element.Multiply(f));

        public static Ma Divide<Ma, F>(this IMatrix<Ma, F> m, F f)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new() => m.Apply(element => element.Divide(f));

        public static Ma Multiply<Ma, Ma2, F>(this IMatrix<Ma, F> m1, IMatrix<Ma2, F> m2)
            where Ma : IMatrix<Ma, F> where Ma2 : IMatrix<Ma2, F> where F : IField<F>, new()
        {
            if (m1.N != m2.M)
                throw new ArgumentException("Right matrix must have as many columns as left matrix has rows.");

            var result = new ImmutableList<F>[m2.M];
            for (var j = 0; j < m1.M; ++j)
            {
                var row = m1.Row(j);
                var columns = m2.Columns();
                result[j] = row.Zip(columns).Select(fc =>
                        row.Zip(fc.Item2).Aggregate(m1.FIELD_ZERO, (total, f) => total.Add(f.Item1.Multiply(f.Item2))))
                    .ToImmutableList();
            }

            return m1.New(result);
        }

        public static bool IsSquare<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new() => m.N == m.M;
        
        public static Ma NullMatrix<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new F[m.N][];
            for (var j = 0; j < m.N; ++j)
            {
                result[j] = new F[m.N];
                for (var i = 0; i < m.N; ++i)
                    result[j][i] = m.FIELD_ZERO;
            }

            return m.New(result);
        }

        public static bool IsNullMatrix<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            for (var i = 0; i < m.M; ++i)
            {
                for (var j = 0; j < m.N; ++j)
                {
                    if (!m[i, j].IsZero())
                        return false;
                }
            }
            return true;
        }

        public static bool IsUnitMatrix<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (!m.IsSquare())
                return false;
            for (var j = 0; j < m.N; ++j)
            {
                for (var i = 0; i < m.N; ++i)
                    if (!(i == j && m[j, i].IsOne() || i != j && m[j, i].IsZero()))
                        return false;
            }

            return true;
        }

        public static Ma UnitMatrix<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (!m.IsSquare())
                throw new InvalidOperationException("Unit matrices must be square.");
            var result = new F[m.N][];
            for (var j = 0; j < m.N; ++j)
            {
                result[j] = new F[m.N];
                for (var i = 0; i < m.N; ++i)
                    result[j][i] = i == j ? m.FIELD_ONE : m.FIELD_ZERO;
            }

            return m.New(result);
        }

        public static bool IsDiagonalMatrix<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (!m.IsSquare())
                return false;
            for (var j = 0; j < m.N; ++j)
            {
                for (var i = 0; i < m.N; ++i)
                    if (i != j && m[j, i].IsZero())
                        return false;
            }

            return true;
        }

        public static Ma DiagonalMatrix<Ma, F>(this IMatrix<Ma, F> m, F[] values)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (!m.IsSquare())
                throw new InvalidOperationException("Unit matrices must be square.");
            var result = new F[m.N][];
            for (var j = 0; j < m.N; ++j)
            {
                result[j] = new F[m.N];
                for (var i = 0; i < m.N; ++i)
                    result[j][i] = i == j ? values[i] : m.FIELD_ZERO;
            }

            return m.New(result);
        }

        public static Ma SwapRows<Ma, F>(this IMatrix<Ma, F> m, int row1, int row2)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new ImmutableList<F>[m.M];
            for (var j = 0; j < m.M; ++j)
            {
                if (j == row1)
                    result[j] = m.Row(row2);
                else if (j == row2)
                    result[j] = m.Row(row1);
                else
                    result[j] = m.Row(j);
            }
            
            return m.New(result);
        }

        public static Ma ScaleRow<Ma, F>(this IMatrix<Ma, F> m, int row, F scalar)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new ImmutableList<F>[m.M];

            for (var j = 0; j < m.M; ++j)
                result[j] = j == row ? m.Row(j).Select(f => f.Multiply(scalar)).ToImmutableList() : m.Row(j);

            return m.New(result);
        }

        public static Ma AddScaledRow<Ma, F>(this IMatrix<Ma, F> m, int source, F scalar, int dest)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new ImmutableList<F>[m.M];

            for (var j = 0; j < m.M; ++j)
                result[j] = j == dest ? m.Row(j).Zip(m.Row(source)).Select(f => f.Item1.Add(f.Item2.Multiply(scalar))).ToImmutableList() : m.Row(j);

            return m.New(result);
        }

        public static Ma Transpose<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new ImmutableList<F>[m.N];

            for (var i = 0; i < m.N; ++i)
                result[i] = m.Column(i);

            return m.New(result);
        }

        public static Ma MinorMatrix<Ma, F>(this IMatrix<Ma, F> m, int row, int column)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new F[m.M - 1][];

            for (var j = 0; j < m.M; ++j)
            {
                if (j == row)
                    continue;
                result[j - 1] = new F[m.N];
                for (var i = 0; i < m.N; ++i)
                {
                    if (i == column)
                        continue;
                    if (j > row)
                    {
                        if (i > column)
                            result[j - 1][i - 1] = m[j, i];
                        else
                            result[j - 1][i] = m[j, i];
                    }
                    if (i > column)
                        result[j][i - 1] = m[j, i];
                    else
                        result[j][i] = m[j, i];
                }
            }

            return m.New(result);
        }

        public static F Trace<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (!m.IsSquare())
                throw new ArgumentException("Only square matrices have traces.");
            var result = m.FIELD_ZERO;
            for (var i = 0; i < m.N; ++i)
                result = result.Add(m[i, i]);
            return result;
        }

        public static Ma JoinHorizontal<Ma, F>(this IMatrix<Ma, F> m1, IMatrix<Ma, F> m2)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (m1.M != m2.M)
                throw new ArgumentException("To concatenate two matrices horizontally, they must have the same number of rows.");
            var columns = new ImmutableList<F>[m1.N + m2.N];
            for (var i = 0; i < m1.N; ++i)
                columns[i] = m1.Column(i);
            for (var i = 0; i < m2.N; ++i)
                columns[i + m1.N] = m2.Column(i);
            return m1.New(columns).Transpose();
        }

        public static Ma JoinHorizontal<Ma, V, F>(this IMatrix<Ma, F> m, IVector<V, F> v)
            where Ma : IMatrix<Ma, F> where V : IVector<V, F> where F : IField<F>, new()
        {
            if (m.M != v.Size)
                throw new ArgumentException("To concatenate two matrices horizontally, they must have the same number of rows.");
            var columns = new ImmutableList<F>[m.N + 1];
            for (var i = 0; i < m.N; ++i)
                columns[i] = m.Column(i);
            columns[m.N] = v.ToImmutableList();
            return m.New(columns).Transpose();
        }

        public static Ma JoinVertical<Ma, F>(this IMatrix<Ma, F> m1, IMatrix<Ma, F> m2)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (m1.N != m2.N)
                throw new ArgumentException("To concatenate two matrices vertically, they must have the same number of columns.");
            var rows = new ImmutableList<F>[m1.M + m2.M];
            for (var j = 0; j < m1.M; ++j)
                rows[j] = m1.Row(j);
            for (var j = m1.N; j < m2.M; ++j)
                rows[j] = m2.Row(m1.M + j);
            return m1.New(rows);
        }

        public static (Ma left, Ma right) SplitHorizontal<Ma, F>(this IMatrix<Ma, F> m, int splitColumn)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (splitColumn < 0 || splitColumn >= m.N)
                throw new IndexOutOfRangeException("splitIndex must be [0;N-1]");
            var leftColumns = new ImmutableList<F>[splitColumn];

            for (var i = 0; i < splitColumn; ++i)
                leftColumns[i] = m.Column(i);

            var rightColumns = new ImmutableList<F>[m.N - splitColumn];

            for (var i = splitColumn; i < m.N; ++i)
                rightColumns[i - splitColumn] = m.Column(i);

            return (m.New(leftColumns).Transpose(), m.New(rightColumns).Transpose());
        }

        public static (Ma upper, Ma lower) SplitVertical<Ma, F>(this IMatrix<Ma, F> m, int splitRow)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            if (splitRow < 0 || splitRow >= m.M)
                throw new IndexOutOfRangeException("splitIndex must be [0;N-1]");
            var upperRows = new ImmutableList<F>[splitRow];

            for (var j = 0; j < splitRow; ++j)
                upperRows[j] = m.Row(j);

            var lowerRows = new ImmutableList<F>[m.M - splitRow];

            for (var j = splitRow; j < m.M; ++j)
                lowerRows[j - splitRow] = m.Row(j);

            return (m.New(upperRows), m.New(lowerRows));
        }

        public static Ma Apply<Ma, F>(this IMatrix<Ma, F> m, Func<F, F> func)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            return m.New(m.Rows().Select(r => r.Select(func).ToImmutableList()).ToImmutableList());
        }

        public static bool IsTrap<Ma, F>(this IMatrix<Ma, F> m) 
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {

            var leadingOnes = new List<(int j, int i)>();
            {
                var prevLoc = 0;
                var leaded = false;
                var j = 0;
                foreach (var mr in m.Rows())
                {
                    for (var i = 0; i < mr.Count; ++i)
                        if (!mr[i].IsZero())
                        {
                            if (!mr[i].IsOne()) return false;

                            if (i < prevLoc)
                                return false;
                            leadingOnes.Add((j, i));
                            leaded = true;
                            break;
                        }

                    if (!leaded)
                        prevLoc = m.N;
                    leaded = false;
                    ++j;
                }
            }
            foreach (var lead in leadingOnes)
                for (var j = 0; j < lead.j; ++j)
                    if (!m[j, lead.i].EqualTo(new F()))
                        return false;
            return true;

        }

        /*
        // From Numerical Recipes
        public static Ma ToTrap<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, IComparable<F>, new()
        {
            var zero = m[0, 0].ZERO;
            var one = m[0, 0].ONE;

            var indexRow = new int[m.N];
            var indexColumn = new int[m.M];
            var iPivot = ContainerUtils.UniformArray(0, m.N);
            for (var i = 0; i < m.N; ++i)
            {
                var curRow = 0;
                var curColumn = 0;
                var largest = zero;
                for (var j = 0; j < m.N; ++j) // Find pivot
                {
                    if (!iPivot[j].Equals(one))
                    {
                        for (var k = 0; k < m.M; ++k)
                        {
                            if (iPivot[k] == 0)
                            {
                                if (m[j, k].Absolute().CompareTo(largest) > 0)
                                {
                                    largest = m[j, k].Absolute();
                                    curRow = j;
                                    curColumn = k;
                                }
                            }
                        }
                    }
                }
                ++iPivot[curColumn];
                if (curRow != curColumn)
                {
                    m = m.SwapRows(curRow, curColumn);
                    for (var l = 0; l < M; ++l)
                        SWAP(a[irow][l], a[icol][l]);
                }

            }
        }*/

        public static IMatrix<Ma, F> ToTrap<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            for (int i = 0, j = 0; i < m.M && j < m.N; ++i, ++j)
            {
                // Check for 0 column
                while (m.Column(j).Skip(i).All(f => f.IsZero()))
                {
                    ++j;
                    if (j >= m.N)
                        return m;
                }

                // Check for 0
                if (m[i, j].IsZero())
                {
                    // If 0, find row below without zero...
                    var rowBelow = i + 1;
                    for (; m[rowBelow, j].IsZero(); ++rowBelow) ;
                    // ...and swap with it.
                    m = m.SwapRows(i, rowBelow);
                }

                m = m.ScaleRow(i, m[i, j].MultiplicativeInverse());

                // Make the rest of the column 0
                for (var otherRow = 0; otherRow < m.M; ++otherRow)
                {
                    if (otherRow == i)
                        continue;
                    if (!m[otherRow, j].IsZero())
                        m = m.AddScaledRow(i, m[otherRow, j].Divide(m[i, j]).AdditiveInverse(), otherRow);
                }
            }
            return m;
        }


        public static ParametricEquation<V, F> Solve<Ma, V, F>(this IMatrix<Ma, F> a, IVector<V, F> b)
            where Ma : IMatrix<Ma, F> where V : IVector<V, F> where F : IField<F>, new()
        {
            if (a.M != b.Size)
                throw new ArgumentException("Vector must have as many rows as matrix has columns.");
            var fZero = a[0, 0].ZERO;
            var fOne = a[0, 0].ONE;

            var m = a.JoinHorizontal(b).ToTrap();
            var rows = m.Rows();
            var columns = m.Columns();
            var rank = rows.Count(r => r.Take(a.N).Any(f => !f.IsZero()));
            if (rows.Skip(rank).Any(r => r.Skip(a.N).Any(f => !f.IsZero())))
                throw new ArgumentException("No solutions.");

            var oneColumns = new List<int>();
            var constantArray = new F[a.N];
            for (int i = 0, j = 0; j < a.N; ++i, ++j)
            {
                while (j < a.N && !m[i, j].IsOne())
                {
                    constantArray[j] = fZero;
                    ++j;
                }
                if (j >= a.N)
                    break;
                oneColumns.Add(j);
                constantArray[j] = m[i, a.N];
            }
            Debug.Assert(oneColumns.Count() == rank);

            var coefs = new List<V>();
            for (var j = 0; j < a.N; ++j)
            {
                if (oneColumns.Contains(j))
                    continue;

                var coef = ContainerUtils.UniformArray(fZero, a.N);
                for (var i = 0; i < rank; ++i)
                    coef[oneColumns[i]] = m[i, j].AdditiveInverse();
                coef[j] = fOne;
                coefs.Add(b.New(coef));
            }
            Debug.Assert(coefs.Count() == a.N - rank);

            return new ParametricEquation<V, F>(b.New(constantArray), coefs.ToImmutableList());
        }
        

        /*public static Ma ToTrap<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            // TEST!
            var result = m;
            var row = 0;
            for (var column = 0; column < m.N && row < m.M; ++column)
            {
                //Check for zero column
                if (result.Column(column).Skip(row).All(d => d.IsZero()))
                    continue;
                //Put one in space (m,n) 
                if (!result[row, column].IsOne())
                {
                    if (result.Column(column).Skip(row).Contains(m.FIELD_ONE))
                    {
                        result = result.SwapRows(row, result.Column(column).Skip(row).FirstIndexOf(m.FIELD_ONE) + row);
                    }
                    else
                    {
                        result = result.SwapRows(row, result.Column(column).Skip(row).FirstIndexOf(d => !d.IsZero()) + row);
                        result = result.ScaleRow(row, result[row, column].MultiplicativeInverse());
                    }
                }

                //Run through all rows above and set to zero.
                for (var j = 0; j < row; ++j)
                    if (!result[j, column].IsZero())
                        result = result.AddScaledRow(row, result[j, column].AdditiveInverse(), j);
                //Run through all rows below and set to zero.
                for (var j = row + 1; j < m.M; ++j)
                    if (!result[j, column].IsZero())
                        result = result.AddScaledRow(row, result[j, column].AdditiveInverse(), j);
                ++row;
            }

            return (Ma)result;
        }*/

        public static F Determinant<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = new F();
            if (!m.IsSquare())
                throw new InvalidOperationException("Can only find the determinant of square matrices.");
            if (m.N == 2)
                return m[0, 0].Multiply(m[1, 1]).Subtract(m[0, 1].Multiply(m[1, 0]));
            for (var i = 0; i < m.N; ++i)
                if (!m[0, i].IsZero())
                {
                    if (i % 2 == 0)
                        result = result.Add(m.MinorMatrix(0, i).Determinant().Multiply(m[0, i]));
                    else
                        result = result.Add(m.MinorMatrix(0, i).Determinant().Multiply(m[0, i]).AdditiveInverse());
                }

            return result;

        }

        public static ParametricEquation<V, F> Kernel<Ma, V, F>(this IMatrix<Ma, F> m, IVector<V, F> vectorInstance)
            where Ma : IMatrix<Ma, F> where V : IVector<V, F> where F : IField<F>, new()
        {
            return m.Solve(vectorInstance.NullVector(m.M));
        }

        public static V KernelElement<Ma, F, V>(this IMatrix<Ma, F> m, IVector<V, F> vectorInstance)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new() where V : IVector<V, F>
        {
            var kernel = m.Kernel(vectorInstance);
            return kernel.Dimensions > 0 ? kernel.Coefficients[0] : kernel.Constant;
        }

        public static IEnumerable<Vector<F>> EigenVectors<Ma, F>(this IMatrix<Ma, F> m, F eigenValue)
            where Ma : IMatrix<Ma, F> where F : ILatexable, IField<F>, new()
        {
            if (!m.IsSquare())
                throw new InvalidOperationException("Only a square matrix has eigen values or vectors.");
            throw new NotImplementedException();
            //return (m.Subtract(m.UnitMatrix().Multiply(eigenValue))).Kernel().Coefficients;

        }

        public static F EigenValue<Ma, F, V>(this IMatrix<Ma, F> m, IVector<V, F> eigenVector)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new() where V : IVector<V, F>
        {
            if (!m.IsSquare())
                throw new InvalidOperationException("Only a square matrix has eigen values or vectors.");
            if (eigenVector.Size != m.N)
                throw new ArgumentException("An eigen vector always has the same dimensions as its matrix.");
            if (eigenVector.IsNull())
                throw new ArgumentException("Null vectors cannot be eigenvectors.");
            var result = m.Multiply(eigenVector);
            return result[0, 0].Divide(eigenVector[0]);

        }

        public static string ToString<Ma, F>(this IMatrix<Ma, F> m)
            where Ma : IMatrix<Ma, F> where F : IField<F>, new()
        {
            var result = "";
            for (var j = 0; j < m.M; ++j)
            {
                result += "|";
                for (var i = 0; i < m.N; ++i) result += m[j, i] + " ";
                result += "|";
                result += Environment.NewLine;
            }

            return result;
        }
    }
}
