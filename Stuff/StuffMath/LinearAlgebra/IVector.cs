﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.LinearAlgebra
{
    public interface IVector<V, F> : IMatrix<V, F>, IReadOnlyList<F> where V : IVector<V, F> where F : IField<F>, new()
    {
        /// <summary>
        /// The number of dimensions
        /// </summary>
        int Size { get; }

        V New(ImmutableList<F> vector);
    }

    public static class VectorExtensions
    {
        public static V New<V, F>(this IVector<V, F> v, IEnumerable<F> vector) where V : IVector<V, F> where F : IField<F>, new() =>
            v.New(vector.ToImmutableList());

        public static F DotSum<V, F>(this IVector<V, F> v1, IVector<V, F> v2) where V : IVector<V, F> where F : IField<F>, new()
        {
            if (v1.Size != v2.Size)
                throw new ArgumentException("The two vectors must have equal size.");
            if (v1.Size == 0)
                return v1.FIELD_ZERO;
            return v1.Zip(v2, (f1, f2) => (f1, f2)).Aggregate(v1.FIELD_ZERO, (total, f) => total.Add(f.f1.Multiply(f.f2)));
        }

        public static F LengthSquared<V, F>(this IVector<V, F> v) where V : IVector<V, F> where F : IField<F>, new() =>
            v.FIELD_ONE.Multiply(DotSum(v, v));

        public static V Multiply<V, F>(this IVector<V, F> v, F f) where V : IVector<V, F> where F : IField<F>, new() => v.New(v.Select(fv => fv.Multiply(f)));

        public static V Subtract<V, F>(this IVector<V, F> v1, IVector<V, F> v2) where V : IVector<V, F> where F : IField<F>, new()
        {
            if (v1.Size != v2.Size)
                throw new ArgumentException("The two vectors must have equal size.");
            return v1.Add(v2.AdditiveInverse());
        }

        public static V Divide<V, F>(this IVector<V, F> v, F f) where V : IVector<V, F> where F : IField<F>, new() =>
            v.Multiply(f.MultiplicativeInverse());

        public static V Conjugate<V, F>(this IVector<V, F> v) where V : IVector<V, F> where F : IField<F>, new() =>
            v.New(v.Select(f => f.Conjugate()));

        public static F ProjectionLength<V, F>(this IVector<V, F> v1, IVector<V, F> v2)
            where V : IVector<V, F> where F : IField<F>, new()
        {
            if (v1.Size != v2.Size)
                throw new ArgumentException("The two vectors must have equal size.");
            return v2.LengthSquared();
        }

        public static V Project<V, F>(this IVector<V, F> v1, IVector<V, F> v2)
            where V : IVector<V, F> where F : IField<F>, new()
        {
            if (v1.Size != v2.Size)
                throw new ArgumentException("The two vectors must have equal size.");
            return v2.Multiply(v1.DotSum(v2).Divide(v2.LengthSquared()));
        }

        public static F Total<V, F>(this IVector<V, F> v) where V : IVector<V, F> where F : IField<F>, new() =>
            v.Aggregate(v.FIELD_ZERO, (total, f) => total.Add(f));

        public static bool IsNull<V, F>(this IVector<V, F> v) where V : IVector<V, F> where F : IField<F>, new() =>
            v.All(f => f.IsZero());

        public static V UnitVector<V, F>(this IVector<V, F> v, int dim, int size)
            where V : IVector<V, F> where F : IField<F>, new()
        {
            var result = new F[size];
            var i = 0;
            for (; i < dim; ++i)
                result[i] = v.FIELD_ZERO;
            result[++i] = v.FIELD_ONE;
            for (; i < size; ++i)
                result[i] = v.FIELD_ZERO;
            return v.New(result);
        }

        public static V UnitVector<V, F>(this IVector<V, F> v, int dim)
            where V : IVector<V, F> where F : IField<F>, new() => v.UnitVector(dim, v.Size);

        public static V NullVector<V, F>(this IVector<V, F> v, int size)
            where V : IVector<V, F> where F : IField<F>, new() => v.New(ContainerUtils.UniformArray(v.FIELD_ZERO, size));

        public static V NullVector<V, F>(this IVector<V, F> v)
            where V : IVector<V, F> where F : IField<F>, new() => v.NullVector(v.Size);

        public static bool EqualTo<V, F>(this IVector<V, F> v1, IVector<V, F> v2)
            where V : IVector<V, F> where F : IField<F>, new()
        {
            if (v1.Size != v2.Size)
                throw new ArgumentException("The two vectors must have equal size.");
            return v1.Zip(v2).All(f => f.Item1.EqualTo(f.Item2));
        } 
    }
}
