﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.LinearAlgebra
{
    public interface IHilbertSpace<H, F> : IVector<H, F> where H : IHilbertSpace<H, F> where F : IHilbertField<F>, new()
    {

    }

    public static class HilbertSpaceExtensions
    {
        public static F Length<H, F>(this IHilbertSpace<H, F> h)
            where H : IHilbertSpace<H, F> where F : IHilbertField<F>, new() => h.LengthSquared().AbsSqrt();

        public static F Angle<H, F>(this IHilbertSpace<H, F> h1, IHilbertSpace<H, F> h2)
            where H : IHilbertSpace<H, F> where F : IHilbertField<F>, new()
        {
            if (h1.Size != h2.Size)
                throw new ArgumentException("The vectors must have the same number of dimensions.");
            return h1.DotSum(h2).Divide(h1.Length().Multiply(h2.Length()));
        }

        public static F ProjectionLength<H, F>(this IHilbertSpace<H, F> h1, IHilbertSpace<H, F> h2)
            where H : IHilbertSpace<H, F> where F : IHilbertField<F>, new()
        {
            if (h1.Size != h2.Size)
                throw new ArgumentException("The vectors must have the same number of dimensions.");
            return h1.DotSum(h2).Divide(h1.Length());
        }

        public static H Normalize<H, F>(this IHilbertSpace<H, F> h) where H : IHilbertSpace<H, F> where F : IHilbertField<F>, new() =>
            h.IsNull() ? (H)h : h.Divide(h.Length());
    }
}
