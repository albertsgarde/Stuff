﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Xml.Schema;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.LinearAlgebra
{
    public class ParametricEquation<V, F> : ILatexable where V : IVector<V, F> where F : IField<F>, new()
    {
        public V Constant { get; }

        public ImmutableList<V> Coefficients { get; }

        private static (V constant, ImmutableList<V> coefficients) Constructor(V constant, ImmutableList<V> coefficients)
        {
            foreach (var coef in coefficients)
            {
                if (coef.Count != constant.Size)
                    throw new ArgumentException(
                        $"All coefficient vectors must have the same size (dimensionality) as the constant vector. The vector {coef} has the size {coef.Count}, " +
                        $"while the constant vector has size {constant.Size}");
            }

            return (constant, coefficients);
        }

        public ParametricEquation(V constant, ImmutableList<V> coefficients)
        {
            (Constant, Coefficients) = Constructor(constant, coefficients);
        }

        public ParametricEquation(ImmutableList<V> coefficients)
        {
            if (coefficients.Count == 0)
                throw new ArgumentException("Cannot create a ParametricEquation with no coefficients without specifying the constant.");
            (Constant, Coefficients) = Constructor(coefficients[0].ZERO, coefficients);
        }

        public ParametricEquation(V constant, params V[] coefficients) : this(constant, coefficients.ToImmutableList())
        {

        }

        public int Dimensions => Constant.Count;

        public V Value(V values)
        {
            if (values.Size != Coefficients.Count)
                throw new ArgumentException("Scalars must be provided for all coefficients.");
            return Coefficients.Zip(values).Aggregate(Constant, (total, coef) => total.Add(coef.Item1.Multiply(coef.Item2)));
        }

        public override string ToString()
        {
            if (Constant.IsNull() && Coefficients.Count == 0)
                return "Ø";
            var result = Constant.IsNull() ? "" : Constant + " + ";
            var coefNum = 0;
            return Coefficients.Aggregate(result, (current, coef) => current + $" + t{++coefNum}{coef}").CutEnd(3);
        }

        public string ToLatex()
        {
            if (Constant.IsNull() && Coefficients.Count == 0)
                return "\\emptyset";
            var result = Constant.IsNull() ? "" : Constant.ToLatex() + " + ";
            var coefNum = 0;
            return Coefficients.Aggregate(result, (current, coef) => current + $"t_{{{++coefNum}}}{coef.ToLatex()} + ").CutEnd(3);
        }
    }
}