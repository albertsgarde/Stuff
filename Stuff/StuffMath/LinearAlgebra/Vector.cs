﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.LinearAlgebra
{
    public class Vector<F> : IVector<Vector<F>, F> where F : IField<F>, ILatexable, new()
    {
        private readonly ImmutableList<F> vector;

        public Vector()
        {
            vector = ImmutableList<F>.Empty;
        }

        public Vector(IEnumerable<F> vector)
        {
            this.vector = vector.ToImmutableList();
        }

        public Vector(params F[] vector) : this(vector.AsEnumerable())
        {

        }

        public Vector<F> New(ImmutableList<F> vector) => new Vector<F>(vector);

        public Vector<F> New(IReadOnlyList<IReadOnlyList<F>> rows)
        {
            var m = new Matrix<F>(rows);
            if (m.N != 1)
                throw new ArgumentException("Vectors may only have one column.");
            return new Vector<F>(m.Column(0));
        }

        public F this[int row, int column] => vector[row];

        /// <inheritdoc />
        public F this[int i] => vector[i];


        /// <inheritdoc />
        public int Size => vector.Count;

        public int Count => vector.Count;

        public int M => vector.Count;

        public int N => 1;

        public ImmutableList<F> Row(int row) => new List<F> {vector[row]}.ToImmutableList();

        public ImmutableList<F> Column(int row) => vector;

        public ImmutableList<ImmutableList<F>> Rows() =>
            vector.Select(f => new List<F> {f}.ToImmutableList()).ToImmutableList();

        public ImmutableList<ImmutableList<F>> Columns() =>
            new List<ImmutableList<F>>{ vector }.ToImmutableList();

        /// <summary>
        /// 
        /// </summary>
        public F LengthSquared => this.DotSum(this);

        /// <inheritdoc />
        public Vector<F> ZERO => NullVector(Size);

        /// <inheritdoc />
        public F FIELD_ZERO => new F();

        /// <inheritdoc />
        public F FIELD_ONE => new F().ONE;

        public Vector<F> Add(Vector<F> t)
        {
            return this + t;
        }

        public Vector<F> AdditiveInverse()
        {
            return -this;
        }

        public bool EqualTo(Vector<F> v) => this.Zip(v).All(f => f.Item1.EqualTo(f.Item2));

        /// <summary>
        ///     The vectors must have the same number of dimensions.
        /// </summary>
        /// <returns>The total of the two vectors.</returns>
        public static Vector<F> operator +(Vector<F> vecA, Vector<F> vecB)
        {
            if (vecA.Size != vecB.Size)
                throw new ArgumentException("The vectors must be have the same number of dimensions.");
            return new Vector<F>(vecA.Zip(vecB, (x, y) => x.Add(y)).ToArray());
        }

        /// <summary>
        ///     The vectors must have the same number of dimensions.
        /// </summary>
        /// <returns>The right hand subtracted from the left hand.</returns>
        public static Vector<F> operator -(Vector<F> vecA, Vector<F> vecB) => vecA.Subtract(vecB);

        /// <summary>
        ///     The vectors must have the same number of dimensions.
        /// </summary>
        /// <returns>The right hand subtracted from the left hand.</returns>
        public static Vector<F> operator -(Vector<F> vec)
        {
            return new Vector<F>(vec.Select(d => d.AdditiveInverse()).ToArray());
        }

        /// <returns>The vector multiplied by the F.</returns>
        public static Vector<F> operator *(Vector<F> vec, F f) => vec.Multiply(f);

        /// <returns>The vector multiplied by the F.</returns>
        public static Vector<F> operator *(F f, Vector<F> vec) => vec.Multiply(f);

        /// <returns>The vector divided by the F.</returns>
        public static Vector<F> operator /(Vector<F> vec, F f) => vec.Divide(f);

        public static bool operator ==(Vector<F> vec1, Vector<F> vec2) => vec1.EqualTo(vec2);

        public static bool operator !=(Vector<F> vec1, Vector<F> vec2) => !vec1.EqualTo(vec2);

        /// <param name="dim">The size of the vector.</param>
        /// <param name="axis">Which axis this is the unit vector of.</param>
        public static Vector<F> UnitVector(int dim, int axis)
        {
            var result = new List<F>();
            for (var i = 0; i < axis; ++i)
                result.Add(new F());
            result.Add(new F().ONE);
            for (var i = axis + 1; i < dim; ++i)
                result.Add(new F());
            return new Vector<F>(result);
        }

        public static Vector<F> NullVector(int dim)
        {
            return new Vector<F>(ContainerUtils.UniformArray(new F(), dim));
        }

        public Vector<F2> Apply<F2>(Func<F, F2> func) where F2 : IHilbertField<F2>, new() => new Vector<F2>(this.Select(func));

        public ImmutableList<F> ToImmutableList() => vector;

        public override bool Equals(object obj) => obj is Vector<F> v && v.EqualTo(this);

        public override int GetHashCode()
        {
            return Misc.HashCode(17, 23, vector);
        }

        public IEnumerator<F> GetEnumerator() => vector.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public override string ToString()
        {
            var result = "(";
            foreach (var d in vector)
                result += d + ", ";
            return result.Substring(0, result.Length - 2) + ")";
        }

        public string ToLatex()
        {
            var result = "\\left[\\begin{array}{c}";
            foreach (var f in vector)
                result += f.ToLatex() + "\\\\";
            return result.Substring(0, result.Length - 2) + "\\end{array}\\right]";
        }
    }
}