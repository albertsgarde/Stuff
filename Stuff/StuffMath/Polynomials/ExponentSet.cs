﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Stuff.StuffMath.Polynomials
{
    public class ExponentSet : IReadOnlyList<int>, IComparable<ExponentSet>
    {
        public ImmutableList<int> Exponents { get; }

        public ExponentSet(ImmutableList<int> exponents)
        {
            Exponents = exponents;
            if (Exponents.Any(i => i < 0))
                throw new ArgumentException("Exponents may not be negative.");
        }

        public ExponentSet(IEnumerable<int> exponents) : this(exponents.ToImmutableList())
        {

        }

        public ExponentSet(params int[] exponents) : this(exponents.ToImmutableList())
        {

        }

        public int this[int variable] => Exponents[variable];

        public int Dimensions => Exponents.Count;

        public int Degree => Exponents.Sum();

        public int Count => Dimensions;

        public override string ToString() => Exponents.Aggregate("(", (total, e) => total + e + ", ").CutEnd(2) + ")";

        public IEnumerator<int> GetEnumerator() => Exponents.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public int CompareTo(ExponentSet other)
        {
            if (Degree > other.Degree)
                return -1;
            if (other.Degree > Degree)
                return 1;
            if (this.Max() > other.Max())
                return -1;
            if (other.Max() > this.Max())
                return 1;
            for (var i = 0; i < Degree; ++i)
            {
                if (this[i] == other[i])
                    continue;
                return other[i] - this[i];
            }

            return 0;
        }
    }
}
