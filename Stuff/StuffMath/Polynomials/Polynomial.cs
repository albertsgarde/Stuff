﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.Polynomials
{
    public class Polynomial<F> : IPolynomial<Polynomial<F>, F> where F : IField<F>, ILatexable, new()
    {
        public ImmutableDictionary<ExponentSet, F> Coefficients { get; }

        public int Dimensions { get; }

        public int Degree { get; }

        public Polynomial(ImmutableDictionary<ExponentSet, F> coefficients)
        {
            Coefficients = coefficients;
            Dimensions = Coefficients.Keys.First().Dimensions;
            if (Coefficients.Keys.Any(es => es.Dimensions != Dimensions))
                throw new ArgumentException("All ExponentSets must have the same number of variables.");
            Degree = Coefficients.Keys.Max(es => es.Degree);
        }

        public F this[ExponentSet exponents] => Coefficients[exponents];

        public IEnumerable<ExponentSet> Exponents() => Coefficients.Keys;

        public string ToLatex()
        {
            return Coefficients.Where(coef => !coef.Value.IsZero()).OrderBy(coef => coef.Key).Aggregate("", (total, exp) => total + PartToLatex(exp.Key) + " + ").CutEnd(3);
        }

        private string PartToLatex(ExponentSet es)
        {
            return es.Zip(ContainerUtils.Count()).Where(x => x.Item1 != 0).Aggregate(!this[es].IsOne() ? "" + this[es] : "",
                (total, exp) => total + "x_{" + (exp.Item2 + 1) + "}" + (exp.Item1 != 1 ? "^{" + exp.Item1 + "}" : ""));
        }
    }
}
