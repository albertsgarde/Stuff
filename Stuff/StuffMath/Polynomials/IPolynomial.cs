﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.Expressions.Operators;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.Polynomials
{
    public interface IPolynomial<P, out F> : ILatexable where P : IPolynomial<P, F> where F : IField<F>, new()
    {
        /// <summary>
        /// The coefficient of the part with the given exponents.
        /// Should be zero if no such part exists.
        /// </summary>
        F this[ExponentSet exponents] { get; }

        /// <summary>
        /// The maximum non-zero exponent.
        /// </summary>
        int Degree { get; }

        /// <summary>
        /// The number of variables.
        /// </summary>
        int Dimensions { get; }
        
        /// <returns>An IEnumerable of all non-zero exponents.</returns>
        IEnumerable<ExponentSet> Exponents();
    }

    public static class PolynomialExtensions
    {
        /// <summary>
        /// The coefficient of the part with the given exponents.
        /// Should be zero if no such part exists.
        /// </summary>
        public static F Coefficient<P, F>(this IPolynomial<P, F> p, ExponentSet exponents)
            where P : IPolynomial<P, F> where F : IField<F>, new() => p[exponents];

        public static Expression ToExpression<P, F>(this IPolynomial<P, F> p, string[] varNames) where P : IPolynomial<P, F>
            where F : IField<F>, IExpressionable, new()
        {
            if (varNames.Length != p.Dimensions)
                throw new ArgumentException("varNames must contain a variable name for every variable in p.");
            return new Sum(p.Exponents().Select(exponents =>
                p[exponents].ToExpression() * new Product(exponents.Zip(varNames)
                    .Select(var => new Power(new Variable(var.Item2), var.Item1)))));
        }
    }
}
