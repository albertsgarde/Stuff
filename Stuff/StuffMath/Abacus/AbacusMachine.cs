﻿using System;
using System.Collections.Generic;
using System.Linq;
using Stuff.StuffMath.Turing;

namespace Stuff.StuffMath.Abacus
{
    public class AbacusMachine
    {
        private readonly IReadOnlyList<Node> nodes;

        public AbacusMachine(params Node[] nodes) : this(nodes.AsEnumerable())
        {
        }

        public AbacusMachine(params IEnumerable<Node>[] nodes)
        {
            var result = new List<Node>();
            foreach (var nodeList in nodes)
                result.AddRange(new List<Node>(nodeList));
            this.nodes = result;
            Registers = this.nodes.Max(n => n.Register);
        }

        public int Registers { get; }

        public int Nodes => nodes.Count;

        public Node this[int i]
        {
            get
            {
                if (i == 0)
                    throw new ArgumentException("Nodes start from 1");
                return nodes[i - 1];
            }
        }

        public List<Node> Translate(int nodeNumStart, int endNode, params int[] registers)
        {
            return Translate(nodeNumStart, endNode, registers.ToList());
        }

        public List<Node> Translate(int nodeNumStart, int endNode, List<int> registers)
        {
            if (registers.Count < Registers)
                throw new ArgumentException("Not enough registers.");
            var result = new List<Node>();
            foreach (var node in nodes)
                if (node.Type == Node.NodeType.Add)
                    result.Add(new Node(registers[node.Register - 1], NodeNum(node.Dest, nodeNumStart, endNode)));
                else if (node.Type == Node.NodeType.Subtract)
                    result.Add(new Node(registers[node.Register - 1], NodeNum(node.Dest, nodeNumStart, endNode),
                        NodeNum(node.FailDest, nodeNumStart, endNode)));
                else
                    throw new Exception($"Node type {node.Type} did not exist at the time of this methods creation");
            return result;
        }

        private int NodeNum(int node, int nodeNumStart, int endNode)
        {
            if (node == 0)
                return endNode;
            return node + nodeNumStart - 1;
        }

        private List<State> AddToTuring(int reg, int startNode)
        {
            var result = new List<State>();
            var node = startNode;

            for (var i = 0; i < reg - 1; ++i)
            {
                result.Add(new State((TuringMachineAction.Right, node + 1), (TuringMachineAction.Right, node)));
                node++;
            }

            for (var i = 0; i < Registers - reg; ++i)
            {
                result.Add(new State((TuringMachineAction.Print, node + 1), (TuringMachineAction.Right, node)));
                node++;
                result.Add(new State((TuringMachineAction.Halt, -1), (TuringMachineAction.Right, node + 1)));
                node++;
                result.Add(new State((TuringMachineAction.Right, node + 1), (TuringMachineAction.Erase, node)));
                node++;
            }

            result.Add(new State((TuringMachineAction.Print, node + 1), (TuringMachineAction.Right, node)));
            node++;

            for (var i = 0; i < Registers - 1; ++i)
            {
                result.Add(new State((TuringMachineAction.Left, node + 1), (TuringMachineAction.Left, node)));
                node++;
            }

            result.Add(new State((TuringMachineAction.Right, node + 1 /* TODO: Replace with addNodes target node */),
                (TuringMachineAction.Left, node)));
            node++;
            return result;
        }

        private List<State> SubtractToTuring(int reg, int startNode)
        {
            var result = new List<State>();
            var node = startNode;

            for (var i = 0; i < reg - 1; ++i)
            {
                result.Add(new State((TuringMachineAction.Right, node + 1), (TuringMachineAction.Right, node)));
                node++;
            }

            result.Add(new State((TuringMachineAction.Halt, -1), (TuringMachineAction.Right, node + 1)));
            node++;
            var ifNodeNum = node;
            //If node
            node++;
            var zNodes = new List<State>();
            var zeroNodeNum = node;

            for (var i = 0; i < reg - 1; ++i)
            {
                zNodes.Add(new State((TuringMachineAction.Left, node + 1), (TuringMachineAction.Left, node)));
                node++;
            }

            zNodes.Add(new State((TuringMachineAction.Right, node + 1 /* TODO: Replace with subtractNodes e-target node */),
                (TuringMachineAction.Left, node)));
            node++;

            var oNodes = new List<State>();
            var oneNodeNum = node;

            for (var i = 0; i < Registers - reg - 1; ++i)
            {
                oNodes.Add(new State((TuringMachineAction.Right, node + 1), (TuringMachineAction.Right, node)));
                node++;
            }

            oNodes.Add(new State((TuringMachineAction.Left, node + 1), (TuringMachineAction.Right, node)));
            node++;

            for (var i = 0; i < Registers - reg; ++i)
            {
                oNodes.Add(new State((TuringMachineAction.Left, node + 1), (TuringMachineAction.Erase, node)));
                node++;
                oNodes.Add(new State((TuringMachineAction.Print, node + 1), (TuringMachineAction.Left, node)));
                node++;
                oNodes.Add(new State((TuringMachineAction.Halt, -1), (TuringMachineAction.Left, node + 1)));
                node++;
            }

            oNodes.Add(new State((TuringMachineAction.Left, node + 1), (TuringMachineAction.Erase, node)));
            node++;

            for (var i = 0; i < reg - 1; ++i)
            {
                oNodes.Add(new State((TuringMachineAction.Left, node + 1), (TuringMachineAction.Left, node)));
                node++;
            }

            oNodes.Add(new State((TuringMachineAction.Right, node + 1 /* TODO: Replace with subtractNodes target node */),
                (TuringMachineAction.Left, node)));

            result.Add(new State((TuringMachineAction.Left, zeroNodeNum), (TuringMachineAction.Right, oneNodeNum)));
            result.Concat(zNodes);
            result.Concat(oNodes);
            return result;
        }

        public string AsString()
        {
            var result = "";
            for (var i = 1; i <= Nodes; ++i)
                result += $"{i}: {this[i].AsString()}{Environment.NewLine}";
            return result;
        }
    }
}