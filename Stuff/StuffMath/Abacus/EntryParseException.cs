﻿using System;

namespace Stuff.StuffMath.Abacus
{
    public class EntryParseException : Exception
    {
        public EntryParseException(string functionName, int instructionNumber, Exception cause) : base(
            $"{functionName} {instructionNumber}: {cause.Message}", cause)
        {
            FunctionName = functionName;
            InstructionNumber = instructionNumber;
        }

        public string FunctionName { get; }

        public int InstructionNumber { get; }
    }
}