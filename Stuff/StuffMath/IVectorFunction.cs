﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath
{
    public interface IVectorFunction
    {
        int Arguments { get; }

        int Outputs { get; }

        ImmutableList<string> VariableOrder { get; }

        HilbertVector<ExpressionField> Functions { get; }

        /// <summary>
        /// Gives this vector function the given vector function as argument.
        /// </summary>
        IVectorFunction Combine(IVectorFunction vf);
    }

    public static class VectorFunctionExtensions
    {
        public static HilbertVector<FDouble> Evaluate(this IVectorFunction vf, ImmutableList<double> args)
        {
            if (args.Count != vf.Arguments)
                throw new ArgumentException("There must be one argument for each variable.");
            return vf.Functions.Evaluate(vf.VariableOrder.Zip(args, (v, arg) => new KeyValuePair<string, double>(v, arg)).ToDictionary());
        }

        public static HilbertVector<FDouble> Evaluate(this IVectorFunction vf, params double[] args) =>
            vf.Evaluate(args.ToImmutableList());

        public static HilbertVector<FDouble> Evaluate(this IVectorFunction vf, IReadOnlyList<double> args) =>
            vf.Evaluate(args.ToImmutableList());

        public static HilbertVector<FDouble> Evaluate(this IVectorFunction vf, IReadOnlyList<FDouble> args) =>
            vf.Evaluate(args.Select(fd => fd.Value).ToImmutableList());

        public static VectorFunction Combine(this IVectorFunction vf1, IVectorFunction vf2)
        {
            if (vf1.Arguments != vf2.Outputs)
                throw new ArgumentException("vf2 must have as many outputs as vf1 has inputs.");
            return new VectorFunction(vf1.Functions.Apply(ef => ef.Reduce(vf1.VariableOrder.Zip(vf2.Functions, (var, func) => (var, func.Exp)))), vf2.VariableOrder);
        }
    }
}
