﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath
{
    public class LookupCosine
    {
        public LookupCosine(int length)
        {
            var table = new float[length];
            var increment = Math.PI * 2 / length;
            for (var i = 0; i < length; ++i)
                table[i] = (float) Math.Cos(increment * i);
            Table = table;
        }

        public IReadOnlyList<float> Table { get; }

        public float this[int i] => Table[i];
    }
}