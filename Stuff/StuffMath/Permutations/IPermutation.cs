﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.Permutations
{
    public interface IPermutation<P> : IGroup<P> where P : IPermutation<P>
    {
        int MaxValue { get; }

        int this[int index] { get; }

        P Compose(P p);

        P Inverse();

        bool Disjunct(P p);

        Permutation ToPermutation();
    }
}
