﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.Permutations
{
    public class Permutation : IPermutation<Permutation>, IEnumerable<int>
    {
        private readonly IReadOnlyList<int> permutation;

        public int MaxValue => permutation.Count;

        public Permutation(IEnumerable<int> permutation)
        {
            this.permutation = permutation.ToList();
            var elements = new HashSet<int>();
            foreach (var i in this.permutation)
            {
                if (i < 0 || i >= this.permutation.Count)
                    throw new ArgumentException("All values must be between 0 and the number of elements.");
                if (elements.Contains(i))
                    throw new ArgumentException("All values must be unique.");
                elements.Add(i);
            }
        }

        public Permutation(params int[] permutation) : this(permutation.AsEnumerable())
        {

        }

        public static Permutation Cycle(IEnumerable<int> cycle)
        {
            var cycleList = cycle.ToList();
            var size = cycleList.Max() + 1;
            var result = ContainerUtils.Count().Take(size).ToArray();
            for (var i = 0; i < cycleList.Count - 1; ++i)
            {
                result[cycleList[i]] = cycleList[i + 1];
            }

            result[cycleList[cycleList.Count - 1]] = cycleList[0];

            return new Permutation(result);
        }

        public int this[int index] => index >= MaxValue ? index : permutation[index];

        public static Permutation Identity(int size) => new Permutation(ContainerUtils.Count(0).Take(size));

        public Permutation Identity() => Identity(MaxValue);

        public Permutation ZERO => Identity();

        public Permutation Compose(Permutation p)
        {
            var size = Math.Max(MaxValue, p.MaxValue);
            var result = new int[size];
            for (var i = 0; i < size; ++i)
                result[i] = p[this[i]];
            return new Permutation(result);
        }

        public Permutation Add(Permutation p) => Compose(p);

        public Permutation Inverse()
        {
            var result = new int[MaxValue];
            for (var i = 0; i < MaxValue; ++i)
                result[this[i]] = i;
            return new Permutation(result);
        }

        public Permutation AdditiveInverse() => Inverse();

        public IEnumerable<Permutation> CycleDecomposition()
        {
            var result = new List<Permutation>();
            var usedElements = new HashSet<int>();
            for (var i = 0; i < MaxValue; ++i)
            {
                if (!usedElements.Contains(i))
                {
                    var cycle = new List<int>();
                    var j = i;
                    do
                    {
                        cycle.Add(j);
                        usedElements.Add(j);
                        j = this[j];
                    } while (j != i);
                    result.Add(Permutation.Cycle(cycle));
                }
            }

            return result;
        }

        public bool Disjunct(Permutation p)
        {
            var size = Math.Max(p.MaxValue, MaxValue);
            for (var i = 0; i < size; ++i)
            {
                if (this[i] != i && p[i] != i)
                    return false;
            }

            return true;
        }

        public bool EqualTo(Permutation p)
        {
            var size = Math.Max(p.MaxValue, MaxValue);
            for (var i = 0; i < size; ++i)
            {
                if (this[i] != p[i])
                    return false;
            }

            return true;
        }

        public Permutation ToPermutation() => this;

        public IEnumerator<int> GetEnumerator() => permutation.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public static string CycleDecompositionToString(IEnumerable<Permutation> cycleDecomposition) =>
            cycleDecomposition.Aggregate("", (total, current) => total + current);

        public override string ToString()
        {
            return permutation.Aggregate("(", (total, current) => total + current + " ").TrimEnd(' ') + ")";
        }
    }
}
