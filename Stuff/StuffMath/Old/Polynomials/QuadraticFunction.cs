﻿using System;
using System.Collections;
using System.Collections.Generic;
using Stuff.StuffMath.Complex;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.Expressions.Operators;
using Stuff.StuffMath.Old;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.Old.Polynomials
{
    public class QuadraticFunction : IPolynomial
    {
        private static readonly QuadraticFunction NULL = new QuadraticFunction(0, 0, 0);

        public QuadraticFunction(double a, double b, double c)
        {
            A = a;
            B = b;
            C = c;
        }

        public double A { get; }

        public double B { get; }

        public double C { get; }

        // ReSharper disable once InconsistentNaming
        public IPolynomial ZERO => NULL;

        // ReSharper disable once InconsistentNaming
        public Real ONE { get; } = 1;

        public int Degree => 2;

        public double this[int exponent] => Coefficient(exponent);

        public double Y(double x)
        {
            return A * x * x + B * x + C;
        }

        public IPolynomial Differentiate()
        {
            return new LineEquation(A * 2, B);
        }

        public IPolynomial Integrate()
        {
            return new Polynomial(0, C, B / 2, A / 3);
        }

        public double Integrate(double a, double b)
        {
            return Math.Pow(a, 3) * A / 3 - Math.Pow(b, 3) * A / 3 + Math.Pow(a, 2) * B / 2 - Math.Pow(b, 2) * B / 2 +
                   a * C - b * C;
        }

        public double Coefficient(int exponent)
        {
            switch (exponent)
            {
                case 0:
                    return C;
                case 1:
                    return B;
                case 2:
                    return A;
                default:
                    return 0;
            }
        }

        public Polynomial AsPolynomial()
        {
            return new Polynomial((2, A), (1, B), (0, C));
        }

        public IPolynomial MoveVertical(double k)
        {
            return new QuadraticFunction(A, B, C + k);
        }

        public IPolynomial MoveHoriz(double k)
        {
            k = -k;
            return new QuadraticFunction(A, 2 * A * k + B, A * k * k + B * k + C);
        }

        public Expression ToExpression(string variableName)
        {
            return A * new Power(new Variable(variableName), 2) + B * new Variable(variableName) + C;
        }

        public override string ToString()
        {
            return "y = " + A + "x^2 " + (B >= 0 ? "+ " + B : "- " + B * -1) + "x " +
                   (C >= 0 ? "+ " + C : "- " + C * -1);
        }

        public IEnumerator<double> GetEnumerator()
        {
            yield return C;
            yield return B;
            yield return A;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static bool operator ==(QuadraticFunction qf1, QuadraticFunction qf2)
        {
            return qf1.A == qf2.A && qf1.B == qf2.B && qf1.C == qf2.C;
        }

        public static bool operator !=(QuadraticFunction qf1, QuadraticFunction qf2)
        {
            return qf1.A != qf2.A || qf1.B != qf2.B || qf1.C != qf2.C;
        }

        public LEVector ToVector()
        {
            return new LEVector(C, B, A);
        }

        public IPolynomial Transform(Vector2D v)
        {
            return MoveVertical(v.Y).MoveHoriz(v.X);
        }

        public double[] Roots()
        {
            var d = Math.Pow(B, 2) - 4 * A * C;
            if (d < 0)
                return new double[0];
            if (d == 0)
                return new[] {(Math.Sqrt(d) - B) / (2 * A)};
            return new[] {(Math.Sqrt(d) - B) / (2 * A), (-Math.Sqrt(d) - B) / (2 * A)};
        }

        public Complex2D[] Complex2DRoots()
        {
            var d = Math.Pow(B, 2) - 4 * A * C;
            return new[]
            {
                (Complex2D.Sqrt(Math.Pow(B, 2) - 4 * A * C) - B) / (2 * A),
                (-Complex2D.Sqrt(Math.Pow(B, 2) - 4 * A * C) - B) / (2 * A)
            };
        }

        public Location2D TopPoint()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            return obj is IPolynomial p && AsPolynomial() == p;
        }

        public override int GetHashCode()
        {
            return Misc.HashCode(17, 23, A, B, C);
        }

        public IPolynomial Add(IPolynomial t)
        {
            return AsPolynomial() + t.AsPolynomial();
        }

        public IPolynomial AdditiveInverse()
        {
            return new QuadraticFunction(-A, -B, -C);
        }

        public IPolynomial Multiply(Real s)
        {
            return new QuadraticFunction(A * (double) s, B * (double) s, C * (double) s);
        }

        public bool EqualTo(IPolynomial t)
        {
            return Equals(t);
        }
    }
}