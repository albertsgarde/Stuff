﻿using System;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Structures;

namespace Stuff.StuffMath.Old
{
    public class Vector2D : IVectorSpace<Vector2D, FDouble>
    {
        public Vector2D()
        {
            X = 0;
            Y = 0;
        }

        public Vector2D(Angle angle)
        {
            X = Math.Cos(angle);
            Y = Math.Sin(angle);
            Length = 1;
            Angle = angle;
        }

        public Vector2D(double x, double y)
        {
            X = x;
            Y = y;
            Length = Math.Sqrt(LengthSquared());
            Angle = y < 0 ? 2 * Math.PI - Math.Acos(x / Length) : Math.Acos(x / Length);
        }

        /// <summary>
        ///     Creates a new vector with the same angle, but with the specified length.
        /// </summary>
        /// <param name="length"></param>
        public Vector2D(Vector2D vec, double length)
        {
            X = vec.X / vec.Length * length;
            Y = vec.Y / vec.Length * length;
            Length = length;
            Angle = vec.Angle;
        }

        /// <summary>
        ///     Creates a new Vector from locA to locB
        /// </summary>
        /// <param name="locA">Start location</param>
        /// <param name="locB">End location</param>
        public Vector2D(Location2D locA, Location2D locB)
        {
            X = locB.X - locA.X;
            Y = locB.Y - locA.Y;
            Length = Math.Sqrt(LengthSquared());
            Angle = Y < 0 ? 2 * Math.PI - Math.Acos(X / Length) : Math.Acos(X / Length);
        }

        public Vector2D(Location2D coords)
        {
            X = coords.X;
            Y = coords.Y;
            Length = Math.Sqrt(LengthSquared());
            Angle = Y < 0 ? 2 * Math.PI - Math.Acos(X / Length) : Math.Acos(X / Length);
        }

        public double X { get; }

        public double Y { get; }

        public Angle Angle { get; }

        public double Length { get; }

        public double Degrees => Angle / (Math.PI / 180);

        public static Vector2D UnitX { get; } = new Vector2D(1, 0);

        public static Vector2D UnitY { get; } = new Vector2D(0, 1);

        public FDouble ONE => 1;

        public Vector2D ZERO => new Vector2D();

        public Vector<FDouble> ToVector()
        {
            return new Vector<FDouble>(X, Y);
        }

        public Vector2D Add(Vector2D t)
        {
            return this + t;
        }

        public Vector2D AdditiveInverse()
        {
            return -this;
        }

        public Vector2D Multiply(FDouble s)
        {
            return this * s;
        }

        public bool EqualTo(Vector2D t)
        {
            return X == t.X && Y == t.Y;
        }

        public static Vector2D AngularVector(double radians, double length)
        {
            return new Vector2D(radians) * length;
        }

        public double LengthSquared()
        {
            return X * X + Y * Y;
        }

        public static Vector2D operator +(Vector2D vecA, Vector2D vecB)
        {
            return new Vector2D(vecA.X + vecB.X, vecA.Y + vecB.Y);
        }

        public static Vector2D operator -(Vector2D vecA, Vector2D vecB)
        {
            return new Vector2D(vecA.X - vecB.X, vecA.Y - vecB.Y);
        }

        public static Vector2D operator -(Vector2D vec)
        {
            return new Vector2D(-vec.X, -vec.Y);
        }

        public static Vector2D operator *(Vector2D vec, double multiplier)
        {
            return new Vector2D(vec.X * multiplier, vec.Y * multiplier);
        }

        public static Vector2D operator /(Vector2D vec, double divisor)
        {
            return new Vector2D(vec.X / divisor, vec.Y / divisor);
        }

        public static Vector2D operator *(double multiplier, Vector2D vec)
        {
            return new Vector2D(vec.X * multiplier, vec.Y * multiplier);
        }


        //add this code to class ThreeDPoint as defined previously
        //
        public static bool operator ==(Vector2D a, Vector2D b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b)) return true;

            // If one is null, but not both, return false.
            if ((object) a == null || (object) b == null) return false;

            // Return true if the fields match:
            return a.Equals(b);
        }

        public static bool operator !=(Vector2D a, Vector2D b)
        {
            return !(a == b);
        }

        public static implicit operator LEVector(Vector2D v)
        {
            return new LEVector(v.X, v.Y);
        }

        public bool IsParallel(Vector2D vec)
        {
            return X * Y == vec.X * vec.Y;
        }

        /// <returns>The positive orthogonal vector.</returns>
        public Vector2D OrthogonalVector()
        {
            return new Vector2D(-Y, X);
        }

        /*
         * dotSum(vector) == 0;
         * @param vector - the other Vector.
         * @return whether or not the Vectors are orthogonal.
        */
        public bool IsOrthogonal(Vector2D vec)
        {
            return -Y / vec.X == X / vec.Y;
        }

        /// <summary>
        ///     Projects this Vector onto the specified Vector.
        ///     BEVIS:
        ///     Projektion af b ligger på b, og er derfor parallel med.
        ///     Vectoren c er a - a.project(b)
        ///     c.dotSum(b) = 0 da de er ortogonale.
        ///     a.project(b) = b*k <=>
        ///     b = a.project(b)/k
        ///     a = a.project(b) + c
        ///     (a-a.project(b))*b = 0 <=>
        ///     a.dotSum(b) - a.project(b).dotSum(b) = 0 <=>
        ///     a.dotSum(b)-b.dotSum(k*b) = 0 <=>
        ///     b.dotSum(k*b) = a.dotSum(b) <=>
        ///     k = a.dotSum(b)/b.dotSum(b) <=>
        ///     k = a.dotSum(b)/|b|*|b| 		Da v.dotSum(v) = |v|*|v|
        ///     c = b*(a.dotSum(b)/(|b|*|b|))
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public Vector2D Project(Vector2D vec)
        {
            return vec * (DotSum(vec) / vec.LengthSquared());
        }

        public double ProjectionLength(Vector2D vec)
        {
            return Basic.Norm(DotSum(vec)) / vec.Length;
        }

        public double DotSum(Vector2D vec)
        {
            return X * vec.X + Y * vec.Y;
        }

        /// <summary>
        ///     Angiver arealet af det parallelogram de to vektorer danner.
        ///     DotSum(Tværvector til a, b)
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        public double Determinant(Vector2D vec)
        {
            return X * vec.Y - Y * vec.X;
        }

        public Vector2D Scale(double length)
        {
            return new Vector2D(length * X / Length, length / Length * Y);
        }

        /// <summary>
        /// Adds an orthogonal vector so the resulting vector has the specified angle.
        /// </summary>
        /// <param name="angle">The angle of the resulting vector. The difference between this and the vector's current angle must be no more than π/2.</param>
        /// <returns>The resulting vector.</returns>
        /// <exception cref="ArgumentException">If the difference between the angle and the vector's current angle is more than π/2.</exception>
        public Vector2D OrthogonalExpansion(Angle angle)
        {
            var turnAngle = angle - Angle;
            if (turnAngle.IsObtuse())
                throw new ArgumentException("Difference between the angle and the vector's current angle must be no more than π/2.");
            return new Vector2D(angle) * Length / Math.Cos(turnAngle);
        }

        public Location2D Location()
        {
            return new Location2D(X, Y);
        }

        public static implicit operator Location2D(Vector2D vec)
        {
            return vec.Location();
        }

        public static explicit operator Vector2F(Vector2D vec)
        {
            return new Vector2F((float) vec.X, (float) vec.Y);
        }

        public override bool Equals(object vec)
        {
            Vector2D vecVec;
            return vec != null && (vecVec = vec as Vector2D) != null && Misc.DoubleEquals(Length, vecVec.Length) &&
                   Misc.DoubleEquals(Angle, vecVec.Angle);
        }

        public override int GetHashCode()
        {
            return Misc.HashCode(17, 23, X, Y);
        }

        public string ToLatex()
        {
            return "\\begin{ pmatrix}" + X + "\\\\" + Y + "\\end{ pmatrix}";
        }

        public override string ToString()
        {
            return "(" + X + "," + Y + ")";
        }
    }
}