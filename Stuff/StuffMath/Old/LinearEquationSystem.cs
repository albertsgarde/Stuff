﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Stuff.StuffMath.Old
{
    public class LinearEquationSystem
    {
        public LinearEquationSystem(IEnumerable<LinearEquation> equations)
        {
            Equations = equations.ToList();
            var length = equations.First().Coefficients.Count;
            foreach (var le in Equations.Skip(1))
                if (le.Coefficients.Count != length)
                    throw new ArgumentException("All equations must have the same length.");
        }

        public IReadOnlyList<LinearEquation> Equations { get; }

        public LEMatrix CoefficientMatrix()
        {
            return new LEMatrix(Equations.Select(le => le.CoefficientMatrixRow()));
        }

        public LEMatrix BMatrix()
        {
            return new LEMatrix(Equations.Select(le => le.BMatrixRow()));
        }

        public LEMatrix ToMatrix()
        {
            return new LEMatrix(Equations.Select(le => le.ToMatrixRow()));
        }
    }
}