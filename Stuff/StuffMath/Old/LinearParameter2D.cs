﻿using Stuff.StuffMath.Old.Polynomials;

namespace Stuff.StuffMath.Old
{
    public class LinearParameter2D
    {
        public LinearParameter2D(double xA, double xB, double yA, double yB)
        {
            A = new Vector2D(xA, yA);
            B = new Vector2D(xB, yB);
        }

        public LinearParameter2D(Vector2D b, Vector2D a)
        {
            A = a;
            B = b;
        }

        public LinearParameter2D(Location2D loc1, Location2D loc2)
        {
            B = new Vector2D(loc1);
            A = new Vector2D(loc1, loc2);
        }

        public Vector2D A { get; }

        public Vector2D B { get; }

        public bool CollidesWith(LinearParameter2D param)
        {
            var t = (param.A.X * (B.Y - param.B.Y) - param.A.Y * (B.Y - param.B.X)) /
                    (A.X * param.A.Y - param.A.X * A.Y);
            var s = (A.X * (B.Y - param.B.Y) - A.Y * (B.X - param.B.X)) / (A.X * param.A.Y - param.A.X * A.Y);
            return s == t;
        }

        public double XAxisIntersection()
        {
            var t = -B.Y / A.Y;
            return X(t);
        }

        public double YAxisIntersection()
        {
            var t = -B.X / A.X;
            return Y(t);
        }

        public double X(double t)
        {
            return A.X * t + B.X;
        }

        public double Y(double t)
        {
            return A.Y * t + B.Y;
        }

        public Location2D Point(double t)
        {
            return new Location2D(B + t * A);
        }

        public Vector2D DirectionVector()
        {
            return A;
        }

        public Vector2D OrthogonalVector()
        {
            return A.OrthogonalVector();
        }

        public LineEquation LinearFunction()
        {
            return new LineEquation(B, A.OrthogonalVector());
        }

        public static implicit operator LineEquation(LinearParameter2D lp)
        {
            return lp.LinearFunction();
        }

        public override string ToString()
        {
            return "{" + B + " + t" + A + "}";
        }
    }
}