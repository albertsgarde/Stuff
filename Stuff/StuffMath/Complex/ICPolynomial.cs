﻿using System.Collections.Generic;

namespace Stuff.StuffMath.Complex
{
    public interface ICPolynomial : IEnumerable<(int exponent, Complex2D coefficient)>
    {
        Complex2D this[int exponent] { get; }

        int Degree { get; }

        /// <param name="x">The point at which to calculate the value.</param>
        /// <returns>Returns the polynomials value at the specified point.</returns>
        Complex2D Y(Complex2D x);

        /// <param name="exponent">The exponent whose coefficient is to be returned.</param>
        /// <returns>The coefficient of the specified exponent.</returns>
        Complex2D Coefficient(int exponent);

        /// <returns>The polynomial as an instance of the generic Polynomial class.</returns>
        CPolynomial AsPolynomial();

        string ToString();
    }
}