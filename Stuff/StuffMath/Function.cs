﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Stuff;
using Stuff.StuffMath.Expressions;
using Stuff.StuffMath.Expressions.Functions;
using Stuff.StuffMath.Expressions.Operators;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Old;
using Stuff.StuffMath.Structures;


namespace Stuff.StuffMath
{
    public class Function : IExpression<Function>, IVectorFunction//, IHilbertField<Function>
    {
        public Expression Exp { get; }

        public ImmutableList<string> VariableOrder { get; }

        public Function()
        {
            VariableOrder = ImmutableList<string>.Empty;
            Exp = 0;
        }

        public Function(Expression expression, ImmutableList<string> variableOrder)
        {
            VariableOrder = variableOrder;
            Exp = expression;
            if (VariableOrder.Any(v => VariableOrder.Count(var => var == v) > 1))
                throw new ArgumentException("variableOrder may not contain duplicates.");
            if (!VariableOrder.ContainsAll(Exp.ContainedVariables()))
                throw new ArgumentException("variableOrder must contain all variables in expression. Missing variables: " + Exp.ContainedVariables().Except(VariableOrder).AsString());
        }

        public Function(Expression expression, params string[] variableOrder) : this(expression, variableOrder.ToImmutableList())
        {

        }

        public Function(Expression expression, IReadOnlyList<string> variableOrder) : this(expression, variableOrder.ToImmutableList())
        {
        }

        public int Arguments => VariableOrder.Count;

        public int Outputs { get; } = 1;

        public HilbertVector<ExpressionField> Functions => new HilbertVector<ExpressionField>(Exp);

        public Function NewExpression(Expression e) => new Function(e, VariableOrder);

        public double Evaluate(ImmutableList<double> args)
        {
            if (args.Count != Arguments)
                throw new ArgumentException("There must be one argument for each variable.");
            return Exp.Evaluate(VariableOrder.Zip(args, (v, arg) => new KeyValuePair<string, double>(v, arg)).ToDictionary());
        }

        public Function Combine(IVectorFunction vf)
        {
            if (vf.Outputs != Arguments)
                throw new ArgumentException("vf must have as many outputs as this function has inputs.");
            return new Function(Exp.Reduce(VariableOrder.Zip(vf.Functions, (v, func) => new KeyValuePair<string, Expression>(v, func)).ToDictionary()), vf.VariableOrder);
        }

        IVectorFunction IVectorFunction.Combine(IVectorFunction vf) => Combine(vf);

        
        public string ToLatex(string functionName)
        {
            var result = VariableOrder.Aggregate(functionName + "\\left(", (total, arg) => total + arg + ", ");
            return result.Substring(0, result.Length - 2) + "\\right) = " + Exp.ToLatex();
        }
    }

    public static class FunctionExtensions
    {
        public static double Evaluate(this Function f, params double[] args) => f.Evaluate(args.ToImmutableList());

        public static double Evaluate(this Function f, IReadOnlyList<double> args) =>
            f.Evaluate(args.ToImmutableList());

        public static double Evaluate(this Function f, IReadOnlyList<FDouble> args) =>
            f.Evaluate(args.Select(fd => fd.Value).ToImmutableList());

        public static VectorFunction VectorField(this Function f)
        {
            return VectorFunction.New(f.VariableOrder.Select(v => (ExpressionField)f.Differentiate(v).Exp).ToImmutableList(), f.VariableOrder);
        }

        public static HilbertVector<FDouble> GradientVector(this Function f, IReadOnlyList<FDouble> args)
        {
            if (args.Count != f.Arguments)
                throw new ArgumentException("args must have as many values as f has inputs.");
            return f.VectorField().Evaluate(args);
        }

        public static double DirectionalDerivative(this Function f, IReadOnlyList<FDouble> args, HilbertVector<FDouble> dir)
        {
            if (dir.Size != f.Arguments)
                throw new ArgumentException("Direction vector must have the same dimensionality as the function.");
            return f.GradientVector(args).DotSum(dir.Normalize());
        }
    }
}
