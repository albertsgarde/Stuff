﻿namespace Stuff.StuffMath
{
    public interface ILatexable
    {
        string ToLatex();
    }
}