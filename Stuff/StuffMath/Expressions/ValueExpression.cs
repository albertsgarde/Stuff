﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions
{
    public class ValueExpression : Expression
    {
        public ValueExpression(double value)
        {
            Value = value;
        }

        public double Value { get; }

        public override double Priority
        {
            get => 0;

            protected set => throw new NotImplementedException();
        }

        public static ValueExpression operator +(ValueExpression v1, ValueExpression v2)
        {
            return new ValueExpression(v1.Value + v2.Value);
        }

        public static ValueExpression operator -(ValueExpression v1, ValueExpression v2)
        {
            return new ValueExpression(v1.Value - v2.Value);
        }

        public static ValueExpression operator *(ValueExpression v1, ValueExpression v2)
        {
            return new ValueExpression(v1.Value * v2.Value);
        }

        public static ValueExpression operator /(ValueExpression v1, ValueExpression v2)
        {
            return new ValueExpression(v1.Value / v2.Value);
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            return Value;
        }

        public override Expression Differentiate(string variable)
        {
            return new ValueExpression(0);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            return this;
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is ValueExpression && ((ValueExpression) exp).Value == Value;
        }

        public override bool ContainsVariable(string variable)
        {
            return false;
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return vars;
        }

        public override string ToString()
        {
            return "" + Value;
        }

        public override string ToLatex()
        {
            return "" + Value;
        }
    }
}