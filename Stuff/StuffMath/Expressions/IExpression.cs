﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.StuffMath.Expressions
{
    public interface IExpression<out E> where E : IExpression<E>
    {
        Expression Exp { get; }

        E NewExpression(Expression e);
    }
}
