﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Stuff.StuffMath.Expressions.Operators
{
    public class Product : Expression
    {
        public Product(IEnumerable<Expression> numerator, IEnumerable<Expression> denominator, double constant = 1)
        {
            Numerator = numerator.ToList();
            Denominator = denominator.ToList();
            Const = constant;
        }

        public Product(IEnumerable<Expression> numerator, double constant = 1) : this(numerator, new List<Expression>(),
            constant)
        {
        }

        public Product(params (Expression exp, bool inverse)[] exps)
        {
            var num = new List<Expression>();
            var den = new List<Expression>();
            foreach (var (exp, inverse) in exps)
                if (inverse)
                    den.Add(exp);
                else
                    num.Add(exp);
            Numerator = num;
            Denominator = den;
            Const = 1;
        }

        public Product(params Expression[] numerator)
        {
            Numerator = numerator.Copy();
            Denominator = new List<Expression>();
            Const = 1;
        }

        public IReadOnlyList<Expression> Numerator { get; }

        public IReadOnlyList<Expression> Denominator { get; }

        public double Const { get; }

        public override double Priority
        {
            get => 2;
            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            var num = Const;
            foreach (var exp in Numerator)
                num *= exp.Evaluate(values);
            var den = 1d;
            foreach (var exp in Denominator)
                den *= exp.Evaluate(values);
            return num / den;
        }

        public override Expression Differentiate(string variable)
        {
            if (Denominator.Count > 0)
            {
                var result = (Expression) 0;
                var all = Numerator.Concat(Denominator);
                foreach (var exp in Numerator)
                    result += new Product(all.Select(e => e == exp ? e.Differentiate(variable) : e));
                foreach (var exp in Denominator)
                    result -= new Product(all.Select(e => e == exp ? e.Differentiate(variable) : e));
                return new Product((result, false), (new Power(new Product(Denominator), 2), true), (Const, false));
            }
            else
            {
                var result = (Expression) 0;
                foreach (var exp in Numerator)
                    result += new Product(Numerator.Select(e => e == exp ? e.Differentiate(variable) : e), Const);
                return result;
            }
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var redNum = Numerator.Select(e => e.Reduce(values)).ToList();
            var redDen = Denominator.Select(e => e.Reduce(values)).ToList();
            var resultConst = Const;

            // Remove negations.
            redNum = RemoveNegations(redNum, ref resultConst);
            redDen = RemoveNegations(redDen, ref resultConst);

            // Include internal products.
            {
                var newNum = new List<Expression>();
                var newDen = new List<Expression>();
                foreach (var exp in redNum)
                    if (exp is Product prod)
                    {
                        newNum.AddRange(prod.Numerator);
                        newDen.AddRange(prod.Denominator);
                        resultConst *= prod.Const;
                    }
                    else
                    {
                        newNum.Add(exp);
                    }

                foreach (var exp in redDen)
                    if (exp is Product prod)
                    {
                        newDen.AddRange(prod.Numerator);
                        newNum.AddRange(prod.Denominator);
                        resultConst /= prod.Const;
                    }
                    else
                    {
                        newDen.Add(exp);
                    }

                redNum = newNum;
                redDen = newDen;
            }

            // Condense all constants.
            {
                var newNum = new List<Expression>();
                var newDen = new List<Expression>();
                foreach (var exp in redNum)
                    switch (exp)
                    {
                        case ValueExpression value:
                            resultConst *= value.Evaluate();
                            break;
                        case Negate neg:
                            resultConst *= -1;
                            newNum.Add(neg.Arg);
                            break;
                        case Inverse inv:
                            newDen.Add(inv.Arg);
                            break;
                        default:
                            newNum.Add(exp);
                            break;
                    }

                foreach (var exp in redDen)
                    switch (exp)
                    {
                        case ValueExpression value:
                            resultConst /= value.Evaluate();
                            break;
                        case Negate neg:
                            resultConst *= -1;
                            newDen.Add(neg.Arg);
                            break;
                        case Inverse inv:
                            newNum.Add(inv.Arg);
                            break;
                        default:
                            newDen.Add(exp);
                            break;
                    }

                redNum = newNum;
                redDen = newDen;
            }

            // Equal expressions on different sides of the divisor annihilate.
            {
                var newNum = new List<Expression>();
                foreach (var exp in redNum)
                    if (redDen.Any(e => e.EqualTo(exp)))
                        redDen.Remove(redDen.First(e => e.EqualTo(exp)));
                    else
                        newNum.Add(exp);
                redNum = newNum;
            }

            // TODO: Equal expressions should be exponentiated.

            if (resultConst == 0) // If constant is 0, everything is 0.
                return new ValueExpression(0);
            if (redNum.Count == 0 && redDen.Count == 0) // Only the constant remains.
                return new ValueExpression(resultConst);
            if (redNum.Count == 1 && redDen.Count == 0) // Only one expression remains. It is in the numerator.
            {
                if (resultConst == 1)
                    return redNum.Single();
                if (resultConst == -1)
                    return new Negate(redNum.Single());
            }

            if (redNum.Count == 0 && redDen.Count == 1) // Only one expression remains. It is in the denominator.
            {
                if (resultConst == 1)
                    return new Inverse(redDen.Single());
                if (resultConst == -1)
                    return new Negate(new Inverse(redDen.Single()));
            }
            else if (redNum.Count == 0 && Math.Abs(resultConst) == 1)
            {
                if (resultConst == 1)
                    return new Inverse(new Product(redDen));
                return new Negate(new Inverse(new Product(redDen)));
            }

            if (resultConst < 0) // If the resulting constant is negative.
                return new Negate(new Product(redNum, redDen, -resultConst));
            return new Product(redNum, redDen, resultConst);
        }

        private List<Expression> RemoveNegations(List<Expression> exps, ref double constant)
        {
            var result = new List<Expression>();
            foreach (var exp in exps)
                if (exp is Negate neg)
                {
                    constant *= -1;
                    result.Add(neg.Arg);
                }
                else
                {
                    result.Add(exp);
                }

            return result;
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Product prod && Numerator.Count == prod.Numerator.Count &&
                Denominator.Count == prod.Denominator.Count)
            {
                if (Const != prod.Const)
                    return false;
                var used = new List<Expression>();
                foreach (var thisE in Numerator)
                    if (prod.Numerator.Any(prodE => !used.Contains(prodE) && thisE.EqualTo(prodE)))
                        used.Add(prod.Numerator.First(prodE => !used.Contains(prodE) && thisE.EqualTo(prodE)));
                    else
                        return false;
                used = new List<Expression>();
                foreach (var thisE in Denominator)
                    if (prod.Denominator.Any(prodE => !used.Contains(prodE) && thisE.EqualTo(prodE)))
                        used.Add(prod.Denominator.First(prodE => !used.Contains(prodE) && thisE.EqualTo(prodE)));
                    else
                        return false;
                return true;
            }

            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return Numerator.Concat(Denominator).Any(e => e.ContainsVariable(variable));
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Numerator.Concat(Denominator).Aggregate(vars, (current, exp) => exp.ContainedVariables(current));
        }

        public override string ToString()
        {
            var result = "(" + (Const == 1 ? "" : Const + " * ");
            foreach (var exp in Numerator)
                result += exp + " * ";
            result = result.Substring(0, result.Length - (Numerator.Count > 0 ? 3 : 0));

            if (Denominator.Count == 0)
                return result + ")";
            if (Denominator.Count == 1)
                return result + "/" + Denominator.Single() + ")";

            foreach (var exp in Denominator)
                result += exp + " * ";
            return result.Substring(0, result.Length - (Numerator.Count > 0 ? 3 : 0)) + "))";
        }

        public override string ToLatex()
        {
            var cdot = "\\cdot ";
            var num = Const == 1 ? "" : "" + Const;
            foreach (var exp in Numerator)
                num += (exp.Priority > Priority ? "(" + exp.ToLatex() + ")" : exp.ToLatex()) + cdot;
            if (Denominator.Count == 0)
                return num.Substring(0, num.Length - (Numerator.Count > 0 ? 6 : 0));

            var den = "";
            foreach (var exp in Denominator)
                den += (exp.Priority > Priority ? "(" + exp.ToLatex() + ")" : exp.ToLatex()) + cdot;
            return "\\frac{" + num.Substring(0, num.Length - (Numerator.Count > 0 ? 6 : 0)) + "}{" +
                   den.Substring(0, den.Length - (Denominator.Count > 0 ? 6 : 0)) + "}";
        }
    }
}