﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Functions;

namespace Stuff.StuffMath.Expressions.Operators
{
    public class Negate : Expression
    {
        public Negate(Expression arg)
        {
            Arg = arg;
        }

        public Expression Arg { get; }

        public override double Priority
        {
            get => 1.5;
            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            return -Arg.Evaluate(values);
        }

        public override Expression Differentiate(string variable)
        {
            return -Arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var redExp = Arg.Reduce(values);
            switch (redExp)
            {
                case Negate neg:
                    return neg.Arg;
                case ValueExpression value:
                    return new ValueExpression(-value.Evaluate());
                case Cosine cos:
                    return cos;
                default:
                    return new Negate(redExp);
            }
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Negate neg)
                return Arg.EqualTo(neg.Arg);
            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return Arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "(-" + Arg + ")";
        }

        public override string ToLatex()
        {
            if (Arg.Priority > Priority)
                return "-(" + Arg.ToLatex() + ")";
            return "-" + Arg.ToLatex();
        }
    }
}