﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Operators
{
    public class Inverse : Expression
    {
        public Inverse(Expression arg)
        {
            Arg = arg;
        }

        public Expression Arg { get; }

        public override double Priority
        {
            get => 1.2;
            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            return 1 / Arg.Evaluate(values);
        }

        public override Expression Differentiate(string variable)
        {
            return new Negate(new Product(new Power(Arg, -2), Arg.Differentiate(variable)));
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var redExp = Arg.Reduce(values);
            switch (redExp)
            {
                case Inverse inv:
                    return inv.Arg;
                case ValueExpression value:
                    return new ValueExpression(1 / value.Evaluate());
                case Power pow:
                    return new Power(pow.Left, -pow.Right);
                case Product prod:
                    return new Product(prod.Denominator, prod.Numerator);
                default:
                    return new Inverse(redExp);
            }
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Inverse inv)
                return Arg.EqualTo(inv.Arg);
            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return Arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "(1/" + Arg + ")";
        }

        public override string ToLatex()
        {
            return "\\frac{1}{" + Arg.ToLatex() + "}";
        }
    }
}