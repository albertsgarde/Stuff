﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Stuff.StuffMath.Expressions.Operators
{
    public class Sum : Expression
    {
        /// <summary>
        /// All expressions in the sum. True if added, false if subtracted.
        /// </summary>
        public IReadOnlyList<(Expression exp, bool sign)> Expressions { get; }

        /// <summary>
        /// Added to the sum.
        /// </summary>
        public double Const { get; }

        public Sum(IEnumerable<(Expression exp, bool sign)> expressions, double constant = 0)
        {
            Expressions = expressions.ToList();
            Const = constant;
        }

        public Sum(IEnumerable<Expression> expressions, double constant = 0) : this(
            expressions.Select(exp => (exp, true)), constant)
        {
        }

        public Sum(params (Expression exp, bool sign)[] expressions) : this(expressions.AsEnumerable())
        {
        }

        public Sum(params Expression[] expressions) : this(expressions.AsEnumerable())
        {
        }

        public override double Priority
        {
            get => 3;
            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            var result = Const;
            foreach (var exp in Expressions)
                if (exp.sign)
                    result += exp.exp.Evaluate(values);
                else
                    result -= exp.exp.Evaluate(values);
            return result;
        }

        public override Expression Differentiate(string variable)
        {
            return new Sum(Expressions.Select(exp => (exp.exp.Differentiate(variable), exp.sign)));
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            List<(Expression exp, bool sign)> result = Expressions.Select(e => (e.exp.Reduce(values), e.sign)).ToList();
            var resultConst = Const;

            // Remove negations.
            {
                var newResult = new List<(Expression exp, bool sign)>();
                foreach (var (exp, sign) in result)
                    if (exp is Negate neg)
                        newResult.Add((neg.Arg, !sign));
                    else
                        newResult.Add((exp, sign));
                result = newResult;
            }

            // Include internal products.
            {
                var newResult = new List<(Expression exp, bool sign)>();
                foreach (var (exp, sign) in result)
                    if (exp is Sum sum)
                    {
                        newResult.AddRange(sum.Expressions);
                        resultConst += sum.Const;
                    }
                    else
                    {
                        newResult.Add((exp, sign));
                    }

                result = newResult;
            }

            // Condense all constants.
            {
                var newResult = new List<(Expression exp, bool sign)>();
                foreach (var (exp, sign) in result)
                    if (exp is ValueExpression value)
                        resultConst += value.Evaluate();
                    else if (exp is Negate neg)
                        newResult.Add((neg.Arg, !sign));
                    else
                        newResult.Add((exp, sign));
                result = newResult;
            }

            if (result.Count == 1 && resultConst == 0)
            {
                if (result.Single().sign)
                    return result.Single().exp;
                return new Negate(result.Single().exp);
            }

            if (result.Count == 0)
                return new ValueExpression(resultConst);

            return new Sum(result, resultConst);
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Sum sum && Expressions.Count == sum.Expressions.Count && Const == sum.Const)
            {
                var used = new List<(Expression exp, bool sign)>();
                foreach (var thisE in Expressions)
                    if (sum.Expressions.Any(sumE =>
                        !used.Contains(sumE) && thisE.exp.EqualTo(sumE.exp) && thisE.sign == sumE.sign))
                        used.Add(sum.Expressions.First(sumE =>
                            !used.Contains(sumE) && thisE.exp.EqualTo(sumE.exp) && thisE.sign == sumE.sign));
                    else
                        return false;
            }

            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return Expressions.Any(e => e.exp.ContainsVariable(variable));
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Expressions.Aggregate(vars, (current, exp) => exp.exp.ContainedVariables(current));
        }

        public override string ToString()
        {
            var result = "";
            foreach (var exp in Expressions)
                result += (exp.sign ? " + " : " - ") + exp.exp;
            if (Const == 0)
            {
                if (result.StartsWith(" + "))
                    result = result.Substring(3);
                else if (result.StartsWith(" - "))
                    result = "-" + result.Substring(3);
                else
                    throw new Exception("Invalid state");
            }
            else
            {
                result = Const + result;
            }

            return "(" + result + ")";
        }

        public override string ToLatex()
        {
            var result = "";
            foreach (var exp in Expressions)
                result += (exp.sign ? "+" : "-") + exp.exp.ToLatex();
            if (Const == 0 && result.StartsWith("+"))
                result = result.Substring(1);
            else if (Const != 0)
                result = Const + result;
            return result;
        }
    }
}