﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Functions;

namespace Stuff.StuffMath.Expressions.Operators
{
    public class Power : Expression
    {
        public Power(Expression leftHand, Expression rightHand)
        {
            Left = leftHand;
            Right = rightHand;
        }

        public override double Priority
        {
            get => 1;

            protected set => throw new NotImplementedException();
        }

        public Expression Left { get; }

        public Expression Right { get; }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            return Math.Pow(Left.Evaluate(values), Right.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            if (Right is ValueExpression value)
                return new Power(Left, Right.Evaluate() - 1) * new ValueExpression(Right.Evaluate()) *
                   Left.Differentiate(variable);
            return this * (Right.Differentiate(variable) * new Ln(Left) + Right * Left.Differentiate(variable) / Left);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var leftReduced = Left.Reduce(values);
            var rightReduced = Right.Reduce(values);
            switch (leftReduced)
            {
                case ValueExpression _ when rightReduced is ValueExpression:
                    return new ValueExpression(Math.Pow(leftReduced.Evaluate(), rightReduced.Evaluate()));
                case Power power:
                    return new Power(power.Left, (rightReduced * power.Right).Reduce());
            }

            if (rightReduced is ValueExpression exponent)
            {
                if (exponent.Evaluate() == 1)
                    return leftReduced;
                if (exponent.Evaluate() == 0)
                    return 1;
                if (exponent.Evaluate() == -1)
                    return new Inverse(leftReduced);
            }

            return new Power(leftReduced, rightReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Power power)
                return power.Left.EqualTo(Left) && power.Right.EqualTo(Right);

            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return Left.ContainsVariable(variable) || Right.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Left.ContainedVariables(Right.ContainedVariables(vars));
        }

        public override string ToString()
        {
            return "(" + Left + " ^ " + Right + ")";
        }

        public override string ToLatex()
        {
            if (Left.Priority > Priority)
                return "(" + Left.ToLatex() + ")" + "^{" + Right.ToLatex() + "}";
            return Left.ToLatex() + "^{" + Right.ToLatex() + "}";
        }
    }
}