﻿using System;
using System.Collections.Generic;
using System.Linq;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions
{
    public abstract class Expression : ILatexable, IExpression<Expression>
    {
        /// <summary>
        ///     The priority of evaluation when the math is written out.
        ///     Plus would have lower priority than multiply would have lower priority than power.
        ///     Lower number is higher priority.
        /// </summary>
        public abstract double Priority { get; protected set; }

        public abstract string ToLatex();

        public Expression Exp => this;

        public Expression NewExpression(Expression exp) => exp;

        public static Expression operator +(Expression left, Expression right)
        {
            return new Sum(left, right);
        }

        public static Expression operator -(Expression left, Expression right)
        {
            return new Sum((left, true), (right, false));
        }

        public static Expression operator *(Expression left, Expression right)
        {
            return new Product(left, right);
        }

        public static Expression operator /(Expression left, Expression right)
        {
            return new Product((left, true), (right, false));
        }

        public static Expression operator +(Expression left, double right)
        {
            return new Sum(left, new ValueExpression(right));
        }

        public static Expression operator -(Expression left, double right)
        {
            return new Sum(left, new ValueExpression(-right));
        }

        public static Expression operator *(Expression left, double right)
        {
            return new Product(left, new ValueExpression(right));
        }

        public static Expression operator /(Expression left, double right)
        {
            return new Product((left, true), (new ValueExpression(right), false));
        }

        public static Expression operator +(double left, Expression right)
        {
            return new Sum(new ValueExpression(left), right);
        }

        public static Expression operator -(double left, Expression right)
        {
            return new Sum((new ValueExpression(left), true), (right, false));
        }

        public static Expression operator *(double left, Expression right)
        {
            return new Product(new ValueExpression(left), right);
        }

        public static Expression operator /(double left, Expression right)
        {
            return new Product((new ValueExpression(left), true), (right, false));
        }

        public static Expression operator -(Expression exp)
        {
            return new Negate(exp);
        }

        public static implicit operator Expression(double d)
        {
            return new ValueExpression(d);
        }

        public abstract double Evaluate(Dictionary<string, double> values = null);

        public double Evaluate(double x)
        {
            return Evaluate(new Dictionary<string, double> {{"x", x}});
        }

        public abstract Expression Differentiate(string variable);

        public Expression Differentiate(string variable, int degree)
        {
            if (degree <= 0)
                throw new ArgumentException("Degree must be at least 1.");
            if (degree == 1)
                return Differentiate(variable);
            return Differentiate(variable, --degree).Differentiate(variable);
        }

        /// <summary>
        ///     Reduces the expression as much as possible, checking for various special cases.
        ///     Does not guarantee that the expression is in its most reduced form.
        /// </summary>
        public abstract Expression Reduce(Dictionary<string, Expression> values = null);

        public Expression Reduce(params (string, Expression)[] values)
        {
            return Reduce(values.ToDictionary());
        }

        public Expression Reduce(IEnumerable<(string, Expression)> values)
        {
            return Reduce(values.ToDictionary());
        }

        public Expression Reduce(Dictionary<string, double> values)
        {
            var result = new Dictionary<string, Expression>();
            foreach (var (name, value) in values)
                result[name] = value;
            return Reduce(result);
        }

        public Expression Reduce(params (string, double)[] values)
        {
            return Reduce(values.ToDictionary());
        }

        /// <summary>
        ///     Checks whether this expression is mathematically equal to the specified expression.
        ///     Will not always return true if they are, but will never return true if they aren't.
        ///     Will mostly return far better results if both expressions are reduced first.
        /// </summary>
        /// <param name="exp">The expression to compare this expression to.</param>
        /// <returns>Returns false if the expressions are unequal. Might return true if they are equal.</returns>
        public abstract bool EqualTo(Expression exp);

        public abstract bool ContainsVariable(string variable);

        public bool ContainsVariables(params string[] variables)
        {
            return variables.All(ContainsVariable);
        }

        public abstract HashSet<string> ContainedVariables(HashSet<string> vars);

        public HashSet<string> ContainedVariables()
        {
            return ContainedVariables(new HashSet<string>());
        }

        /// <summary>
        ///     Meant to show the structure of the expression. Not to write it in a human readable format. Use ToLatex for that.
        /// </summary>
        public abstract override string ToString();
    }
}