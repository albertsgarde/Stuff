﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace Stuff.StuffMath.Expressions
{
    public class Operator
    {
        public Operator(string className, string symbol, double priority)
        {
            Class = Type.GetType(className);

            Symbol = symbol;

            Priority = priority;

            var operators = ExpressionCompiler.Operators;
            if (!ExpressionCompiler.Operators.Contains(Symbol))
                throw new ArgumentException(
                    "An operator can only have a symbol contained in ExpressionCompiler's list of operators.");
        }

        public Type Class { get; }

        public string Symbol { get; }

        public double Priority { get; }

        public static Operator Load(XElement element)
        {
            var className = element.ElementValue("className");
            var symbol = element.ElementValue("symbol");
            var priority = double.Parse(element.ElementValue("priority"));
            return new Operator(className, symbol, priority);
        }

        public Expression Create(params Expression[] args)
        {
            return (Expression) Activator.CreateInstance(Class, args.ToArray<object>());
        }
    }
}