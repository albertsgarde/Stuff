﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions
{
    public class Variable : Expression
    {
        private readonly string name;

        public Variable(string name)
        {
            this.name = name;
        }

        public override double Priority
        {
            get => 0;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            if (values == null || !values.ContainsKey(name))
                throw new Exception(
                    "Cannot evaluate an expression containing a variable without assigning the variable a value.");
            return values[name];
        }

        public override Expression Differentiate(string variable)
        {
            return variable == name ? 1 : 0;
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            if (values != null && values.ContainsKey(name))
                return values[name];
            return this;
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Variable && ((Variable) exp).name == name;
        }

        public override bool ContainsVariable(string variable)
        {
            return name == variable;
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            vars.Add(name);
            return vars;
        }

        public override string ToString()
        {
            return name;
        }

        public override string ToLatex()
        {
            return name;
        }
    }
}