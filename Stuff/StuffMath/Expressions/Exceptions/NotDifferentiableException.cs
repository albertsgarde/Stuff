﻿using System;
using System.Runtime.Serialization;

namespace Stuff.StuffMath.Expressions
{
    [Serializable]
    public class NotDifferentiableException : Exception
    {
        public NotDifferentiableException()
        {
        }

        public NotDifferentiableException(string message) : base(message)
        {
        }

        protected NotDifferentiableException(SerializationInfo si, StreamingContext sc) : base(si, sc)
        {
        }
    }
}