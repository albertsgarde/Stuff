﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Security.Policy;
using Stuff.StuffMath.Structures;
using Stuff.StuffMath.Expressions.Functions;
using Stuff.StuffMath.LinearAlgebra;

namespace Stuff.StuffMath.Expressions
{
    public static class ExpressionExtensions
    {
        public static double Evaluate<E>(this E exp, Dictionary<string, double> values = null) where E : IExpression<E> =>
            exp.Exp.Evaluate(values);

        public static double Evaluate<E>(this E exp, params (string, double)[] values) where E : IExpression<E> =>
            exp.Exp.Evaluate(values);

        public static double Evaluate<E>(this E exp, double x) where E : IExpression<E> => exp.Exp.Evaluate(x);

        public static E Differentiate<E>(this E exp, string variable, int degree = 1) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Differentiate(variable, degree));

        public static E Reduce<E>(this E exp, Dictionary<string, Expression> values = null) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Reduce(values));

        public static E Reduce<E>(this E exp, Dictionary<string, double> values) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Reduce(values));

        public static E Reduce<E>(this E exp, params (string, Expression)[] values) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Reduce(values));

        public static E Reduce<E>(this E exp, params (string, double)[] values) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Reduce(values));

        public static E Reduce<E>(this E exp, IEnumerable<(string, Expression)> values) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Reduce(values));

        public static E Reduce<E>(this E exp, IEnumerable<(string s, double d)> values) where E : IExpression<E> =>
            exp.NewExpression(exp.Exp.Reduce(values.Select(v => new KeyValuePair<string, double>(v.s, v.d)).ToDictionary()));

        public static bool EqualTo<E>(this E exp1, E exp2) where E : IExpression<E> => exp1.Exp.EqualTo(exp2.Exp);

        public static bool ContainsVariable<E>(this E exp, string variable) where E : IExpression<E> =>
            exp.Exp.ContainsVariable(variable);

        public static bool ContainsVariables<E>(this E exp, params string[] variables) where E : IExpression<E> =>
            exp.Exp.ContainsVariables(variables);

        public static HashSet<string> ContainedVariables<E>(this E exp, HashSet<string> variables) where E : IExpression<E> =>
            exp.Exp.ContainedVariables(variables);

        public static HashSet<string> ContainedVariables<E>(this E exp) where E : IExpression<E> =>
            exp.Exp.ContainedVariables();


        public static HilbertVector<FDouble> Evaluate(this HilbertVector<ExpressionField> vec,
            Dictionary<string, double> values = null)
        {
            return vec.Apply(exp => (FDouble) exp.Evaluate(values));
        }

        /// <returns>The gradient vector of a function taking varNames arguments and defined by the given IExpression.</returns>
        public static HilbertVector<ExpressionField> VectorField<E>(this E exp, ImmutableList<string> varNames)
            where E : IExpression<E>
        {
            return new HilbertVector<ExpressionField>(varNames.Select(s => new ExpressionField(exp.Exp.Differentiate(s))));
        }

        public static HilbertVector<ExpressionField> VectorField<E>(this E exp, IReadOnlyList<string> varNames)
            where E : IExpression<E> => exp.VectorField(varNames.ToImmutableList());

        public static HilbertVector<ExpressionField> VectorField<E>(this E exp, params string[] varNames)
            where E : IExpression<E> => exp.VectorField(varNames.ToImmutableList());

        /// <returns>The hessian matrix for the given variables.</returns>
        public static Matrix<ExpressionField> HessianMatrix<E>(this E exp, IEnumerable<string> varNames)
            where E : IExpression<E>
        {
            return new Matrix<ExpressionField>(varNames.Select(v1 =>
                new MatrixRow<ExpressionField>(varNames.Select(v2 =>
                    (ExpressionField) exp.Exp.Differentiate(v1).Differentiate(v2)))));
        }

        /// <returns>The hessian matrix for the given variables.</returns>
        public static Matrix<ExpressionField> HessianMatrix<E>(this E exp, params string[] varNames) 
            where E : IExpression<E> => exp.HessianMatrix(varNames.AsEnumerable());

        public static HilbertVector<ExpressionField> Reduce(this HilbertVector<ExpressionField> vector) =>
            vector.Apply(exp => exp.Reduce());

        public static Matrix<ExpressionField> Reduce(this Matrix<ExpressionField> matrix) =>
            matrix.Apply(exp => exp.Reduce());
    }
}