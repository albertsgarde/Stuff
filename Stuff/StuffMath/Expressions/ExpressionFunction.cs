﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace Stuff.StuffMath.Expressions
{
    public class ExpressionFunction
    {
        public ExpressionFunction(int args, string className, params string[] aliases)
        {
            Args = args;

            Class = Type.GetType(className);

            Aliases = aliases;
        }

        public int Args { get; }

        public Type Class { get; }

        public string[] Aliases { get; }

        public static ExpressionFunction Load(XElement element)
        {
            var args = int.Parse(element.ElementValue("args"));
            var className = element.ElementValue("className");
            var aliases = element.Element("aliases").ElementValues("alias").ToArray();
            return new ExpressionFunction(args, className, aliases);
        }

        public Expression Create(params Expression[] args)
        {
            return (Expression) Activator.CreateInstance(Class, args.ToArray<object>());
        }
    }
}