﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class BinomCoef : Expression
    {
        private readonly Expression n;

        private readonly Expression r;

        public BinomCoef(Expression n, Expression r)
        {
            this.n = n;
            this.r = r;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            var nValue = n.Evaluate(values);
            var rValue = r.Evaluate(values);
            if (nValue % 1 != 0)
                throw new Exception("binomCoef only takes integer arguments. n = " + nValue);
            if (rValue % 1 != 0)
                throw new Exception("binomCoef only takes integer arguments. r = " + rValue);
            return Basic.BinomialCoefficient((long) nValue, (long) rValue);
        }

        public override Expression Differentiate(string variable)
        {
            throw new NotDifferentiableException();
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var nReduced = n.Reduce(values);
            var rReduced = r.Reduce(values);
            if (nReduced is ValueExpression)
            {
                if (rReduced is ValueExpression) return new ValueExpression(Evaluate());

                var nValue = nReduced.Evaluate();
                if (nValue % 1 != 0)
                    throw new Exception("binomCoef only takes integer arguments. n = " + nValue);
                return Basic.Factorial((long) nValue) / (new Factorial(rReduced) * new Factorial(nValue - rReduced));
            }

            if (rReduced is ValueExpression)
            {
                var rValue = rReduced.Evaluate();
                if (rValue % 1 != 0)
                    throw new Exception("binomCoef only takes integer arguments. r = " + rValue);
                return nReduced / (Basic.Factorial((long) rValue) * new Factorial(nReduced - rValue));
            }

            return new BinomCoef(nReduced, rReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is BinomCoef)
            {
                var binomCoef = (BinomCoef) exp;
                return binomCoef.n.EqualTo(n) && binomCoef.r.EqualTo(r);
            }

            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return n.ContainsVariable(variable) || r.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return n.ContainedVariables(r.ContainedVariables(vars));
        }

        public override string ToString()
        {
            return "binomCoef(" + n + "," + r + ")";
        }

        public override string ToLatex()
        {
            throw new NotImplementedException();
        }
    }
}