﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Cosine : Expression
    {
        private readonly Expression arg;

        public Cosine(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Cos(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return 0-new Sine(arg) * arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Cos(argReduced.Evaluate()));
            return new Cosine(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Cosine)
            {
                var cosine = (Cosine) exp;
                return cosine.arg.EqualTo(arg);
            }

            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "cos(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "cos(" + arg.ToLatex() + ")";
        }
    }
}