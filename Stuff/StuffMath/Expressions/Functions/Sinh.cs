﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Sinh : Expression
    {
        private readonly Expression arg;

        public Sinh(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Sinh(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return new Cosh(arg) * arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Sin(argReduced.Evaluate()));
            return new Sinh(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Sinh && ((Sinh) exp).arg.EqualTo(arg);
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "sinh(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "sinh(" + arg.ToLatex() + ")";
        }
    }
}