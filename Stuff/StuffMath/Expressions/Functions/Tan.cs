﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Tan : Expression
    {
        private readonly Expression arg;

        public Tan(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            return Math.Tan(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return 1 / new Power(new Cosine(arg), 2) * arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Tan(argReduced.Evaluate()));
            return new Tan(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Tan tan && tan.arg.EqualTo(arg);
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "tan(" + arg + ")";
        }

        public override string ToLatex()
        {
            throw new NotImplementedException();
        }
    }
}