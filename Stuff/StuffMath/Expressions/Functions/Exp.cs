﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Exp : Expression
    {
        private readonly Expression arg;

        public Exp(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 1;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Exp(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return this * arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Exp(argReduced.Evaluate()));
            return new Exp(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Exp expo && expo.arg.EqualTo(arg) || exp is Power power &&
                   power.Left is ValueExpression && power.Left.Evaluate() == Math.E;
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "e^" + arg;
        }

        public override string ToLatex()
        {
            return "e^{" + arg.ToLatex() + "}";
        }
    }
}