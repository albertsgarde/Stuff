﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Factorial : Expression
    {
        private readonly Expression arg;

        public Factorial(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            var argValue = arg.Evaluate(values);
            if (argValue % 1 != 0)
                throw new Exception("factorial only takes integer arguments. arg = " + argValue);
            return Basic.Factorial((long) argValue);
        }

        public override Expression Differentiate(string variable)
        {
            throw new NotDifferentiableException();
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Evaluate());
            return new Factorial(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Factorial)
            {
                var factorial = (Factorial) exp;
                return factorial.arg.EqualTo(arg);
            }

            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "factorial(" + arg + ")";
        }

        public override string ToLatex()
        {
            return arg + "!";
        }
    }
}