﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Conjugate : Expression
    {
        private readonly Expression arg;

        public Conjugate(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 1;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return arg.Evaluate();
        }

        public override Expression Differentiate(string variable)
        {
            throw new NotDifferentiableException();
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            return arg;
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Conjugate con && arg.EqualTo(con.arg);
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "conjugate(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "\\overline{" + arg.ToLatex() + "}";
        }
    }
}