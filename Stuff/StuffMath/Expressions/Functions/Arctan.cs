﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Arctan : Expression
    {
        private readonly Expression arg;

        public Arctan(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values = null)
        {
            return Math.Atan(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return arg.Differentiate(variable) / (1+new Power(arg, 2));
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Atan(argReduced.Evaluate()));
            return new Arctan(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Arctan arctan && arctan.arg.EqualTo(arg);
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "arctan(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "arctan\\left(" + arg + "\\right)";
        }
    }
}