﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Sqrt : Expression
    {
        private readonly Expression arg;

        public Sqrt(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 1;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Sqrt(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return arg.Differentiate(variable)/(2*new Sqrt(arg));
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            switch (argReduced)
            {
                case ValueExpression _:
                    return new ValueExpression(Math.Sqrt(argReduced.Evaluate()));
                case Power power when power.Right is ValueExpression && power.Right.Evaluate() == 2:
                    return power.Left;
                default:
                    return new Sqrt(argReduced);
            }
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Sqrt sqrt && sqrt.arg.EqualTo(arg) || exp is Power power &&
                   power.Right is ValueExpression && power.Right.Evaluate() == 0.5;
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "sqrt(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "\\sqrt{" + arg.ToLatex() + "}";
        }
    }
}