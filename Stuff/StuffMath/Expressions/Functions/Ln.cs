﻿using System;
using System.Collections.Generic;
using Stuff.StuffMath.Expressions.Operators;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Ln : Expression
    {
        private readonly Expression arg;

        public Ln(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Log(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return arg.Differentiate(variable) / arg;
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Log(argReduced.Evaluate()));
            return new Ln(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Ln && ((Ln) exp).arg.EqualTo(arg);
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "ln(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "ln\\left(" + arg + "\\right)";
        }
    }
}