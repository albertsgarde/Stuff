﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Cosh : Expression
    {
        private readonly Expression arg;

        public Cosh(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Cosh(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return new Sinh(arg) * arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Cos(argReduced.Evaluate()));
            return new Cosh(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            if (exp is Cosh cosh) return cosh.arg.EqualTo(arg);
            return false;
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "cosh(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "cosh(" + arg.ToLatex() + ")";
        }
    }
}