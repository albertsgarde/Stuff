﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Expressions.Functions
{
    public class Sine : Expression
    {
        private readonly Expression arg;

        public Sine(Expression arg)
        {
            this.arg = arg;
        }

        public override double Priority
        {
            get => 4;

            protected set => throw new NotImplementedException();
        }

        public override double Evaluate(Dictionary<string, double> values)
        {
            return Math.Sin(arg.Evaluate(values));
        }

        public override Expression Differentiate(string variable)
        {
            return new Cosine(arg) * arg.Differentiate(variable);
        }

        public override Expression Reduce(Dictionary<string, Expression> values = null)
        {
            var argReduced = arg.Reduce(values);
            if (argReduced is ValueExpression)
                return new ValueExpression(Math.Sin(argReduced.Evaluate()));
            return new Sine(argReduced);
        }

        public override bool EqualTo(Expression exp)
        {
            return exp is Sine && ((Sine) exp).arg.EqualTo(arg);
        }

        public override bool ContainsVariable(string variable)
        {
            return arg.ContainsVariable(variable);
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return arg.ContainedVariables(vars);
        }

        public override string ToString()
        {
            return "sin(" + arg + ")";
        }

        public override string ToLatex()
        {
            return "sin(" + arg.ToLatex() + ")";
        }
    }
}