﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Stuff.StuffMath.Expressions
{
    public class FunctionManager
    {
        private readonly List<ExpressionFunction> functions;

        private readonly List<Operator> operators;

        public FunctionManager(params string[] functionDirs)
        {
            functions = new List<ExpressionFunction>();
            operators = new List<Operator>();
            foreach (var directory in functionDirs)
            foreach (var functionFile in from filePath in Directory.EnumerateFiles(directory, "*.xml",
                    SearchOption.AllDirectories)
                where !filePath.Substring(filePath.LastIndexOf('/')).StartsWith("!")
                select filePath)
                LoadFile(functionFile);
            LoadFile("Assets/ExpressionFunctions/BasicMath.xml");
            LoadFile("Assets/ExpressionOperators/BasicMath.xml");
        }

        public IEnumerable<ExpressionFunction> Functions => functions;

        public IEnumerable<Operator> Operators => operators;

        private void LoadFile(string file)
        {
            foreach (var element in XElement.Load(file).Elements("function"))
                functions.Add(Expressions.ExpressionFunction.Load(element));
            foreach (var element in XElement.Load(file).Elements("operator"))
                operators.Add(Expressions.Operator.Load(element));
        }

        public ExpressionFunction Function(string alias)
        {
            return functions.First(f => f.Aliases.Contains(alias));
        }

        public Operator Operator(string symbol)
        {
            return operators.First(o => o.Symbol == symbol);
        }
    }
}