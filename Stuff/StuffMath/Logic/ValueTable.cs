﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Stuff.StuffMath.Logic
{
    public class ValueTable
    {
        private readonly IReadOnlyList<TruthAssignment> rows;

        public ValueTable(IReadOnlyList<string> variables, IReadOnlyList<IReadOnlyList<bool>> values)
        {
            Variables = new List<string>(variables);
            var tempValues = new List<List<bool>>();

            var rows = new List<TruthAssignment>();

            foreach (var value in values)
            {
                Debug.Assert(value.Count == variables.Count);
                tempValues.Add(new List<bool>(value));
                rows.Add(new TruthAssignment(variables, value));
            }

            Values = tempValues;
            this.rows = rows;
        }

        public IReadOnlyList<string> Variables { get; }

        public IReadOnlyList<IReadOnlyList<bool>> Values { get; }

        private bool Valid(TruthAssignment ta)
        {
            foreach (var thisTa in rows)
                if (thisTa.Agrees(ta))
                    return true;
            return false;
        }

        public string ToLatex()
        {
            return ToLatex(Variables.ToDictionary(v => v, v => v));
        }

        public string ToLatex(Dictionary<string, string> latexVarNames)
        {
            foreach (var name in Variables)
                if (!latexVarNames.ContainsKey(name))
                    latexVarNames[name] = name;

            var result = "\\begin{tabular}{|";
            for (var i = 0; i < Variables.Count; ++i)
                result += "c|";
            result += "}\n";
            result += "\\hline\n";
            foreach (var name in Variables)
                result += "$" + latexVarNames[name] + "$ & ";
            result = result.Substring(0, result.Length - 3) + "\\\\\n";
            result += "\\hline\n";
            foreach (var values in Values)
            {
                var line = "";
                foreach (var value in values)
                    line += (value ? "\\top" : "\\bot") + "&";
                result += line.Substring(0, line.Length - 1) + "\\\\\n";
                result += "\\hline\n";
            }

            result += "\\end{tabular}\\\\";
            return result;
        }

        public override string ToString()
        {
            var result = Variables.Aggregate("|", (total, var) => total + var + "|");
            foreach (var value in Values)
            {
                Debug.Assert(value.Count == Variables.Count);
                result += "|";
                for (var j = 0; j < Variables.Count; ++j)
                {
                    result += value[j] ? "1" : "0";
                    result += Misc.EmptyString(Variables[j].Length - 1);
                    result += "|";
                }

                result += Environment.NewLine;
            }
            {
                
            }

            return result;
        }

        private class TruthAssignment
        {
            private readonly Dictionary<string, bool> values;

            public TruthAssignment(IReadOnlyList<string> variables, IReadOnlyList<bool> values)
            {
                Debug.Assert(variables.Count == values.Count);
                this.values = new Dictionary<string, bool>();
                for (var i = 0; i < variables.Count; ++i)
                    this.values[variables[i]] = values[i];
            }

            public bool this[string variable] => values[variable];

            public bool Agrees(TruthAssignment ta)
            {
                foreach (var key in values.Keys)
                    if (ta.ContainsVar(key) && ta[key] != values[key])
                        return false;
                return true;
            }

            public bool ContainsVar(string var)
            {
                return values.ContainsKey(var);
            }
        }
    }
}