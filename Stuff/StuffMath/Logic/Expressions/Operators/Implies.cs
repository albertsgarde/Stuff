﻿using System.Collections.Generic;

namespace Stuff.StuffMath.Logic.Expressions.Operators
{
    public class Implies : Expression
    {
        public Implies(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public override string Name => "Implies";

        public Expression Left { get; }

        public Expression Right { get; }

        public override double Priority => 4;

        public override bool Evaluate(Dictionary<string, bool> values = null)
        {
            return !Left.Evaluate(values) || Right.Evaluate(values);
        }

        public override Expression Reduce(Dictionary<string, bool> values = null)
        {
            var leftReduced = Left.Reduce(values);
            var rightReduced = Right.Reduce(values);
            if (leftReduced is ValueExpression leftValue)
            {
                if (leftValue.Evaluate())
                    return rightReduced;
                return true;
            }

            if (rightReduced is ValueExpression rightValue)
            {
                if (rightValue.Evaluate())
                    return true;
                return !leftReduced;
            }

            if (leftReduced.IsEqual(rightReduced))
                return true;
            return new Implies(leftReduced, rightReduced);
        }

        public override Expression ToNormalForm()
        {
            return new Implies(Left.ToNormalForm(), Right.ToNormalForm());
        }

        public override Expression Negate()
        {
            return new And(Left.ToNormalForm(), Right.Negate());
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Left.ContainedVariables(Right.ContainedVariables(vars));
        }

        public override bool ContainsVariable(string variable)
        {
            return Left.ContainsVariable(variable) || Right.ContainsVariable(variable);
        }

        protected override bool InternalTableau(IReadOnlyList<(Expression exp, bool value)> expressions,
            IReadOnlyDictionary<string, bool> values, bool value)
        {
            if (value)
            {
                if (!InternalTableauNextExp(expressions, values, (Left, false)))
                    return InternalTableauNextExp(expressions, values, (Right, true));
                return true;
            }

            return InternalTableauNextExp(expressions, values, (Left, true), (Right, false));
        }

        public override string ToString()
        {
            return
                $"{(Left.Priority < Priority ? Left.ToString() : $"({Left})")}>{(Right.Priority < Priority ? Right.ToString() : $"({Right})")}";
        }

        public override string ToLatex()
        {
            return
                $"{(Left.Priority < Priority ? Left.ToLatex() : $"({Left.ToLatex()})")} \\implies {(Right.Priority < Priority ? Right.ToLatex() : $"({Right.ToLatex()})")}";
        }
    }
}