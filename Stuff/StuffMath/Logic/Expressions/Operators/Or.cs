﻿using System.Collections.Generic;

namespace Stuff.StuffMath.Logic.Expressions.Operators
{
    public class Or : Expression
    {
        public Or(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public override string Name => "Or";

        public Expression Left { get; }

        public Expression Right { get; }

        public override double Priority => 3;

        public override bool Evaluate(Dictionary<string, bool> values = null)
        {
            return Left.Evaluate(values) || Right.Evaluate(values);
        }

        public override Expression Reduce(Dictionary<string, bool> values = null)
        {
            var leftReduced = Left.Reduce(values);
            var rightReduced = Right.Reduce(values);
            if (leftReduced is ValueExpression leftValue)
            {
                return leftValue.Evaluate() ? true : rightReduced;
            }

            if (rightReduced is ValueExpression rightValue)
            {
                return rightValue.Evaluate() ? true : leftReduced;
            }

            return leftReduced.IsEqual(rightReduced) ? leftReduced : new Or(leftReduced, rightReduced);
        }

        public override Expression ToNormalForm()
        {
            return new Or(Left.ToNormalForm(), Right.ToNormalForm());
        }

        public override Expression Negate()
        {
            return new And(Left.Negate(), Right.Negate());
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Left.ContainedVariables(Right.ContainedVariables(vars));
        }

        public override bool ContainsVariable(string variable)
        {
            return Left.ContainsVariable(variable) || Right.ContainsVariable(variable);
        }

        protected override bool InternalTableau(IReadOnlyList<(Expression exp, bool value)> expressions,
            IReadOnlyDictionary<string, bool> values, bool value)
        {
            if (value)
            {
                if (!InternalTableauNextExp(expressions, values, (Left, true)))
                    return InternalTableauNextExp(expressions, values, (Right, true));
                return true;
            }

            return InternalTableauNextExp(expressions, values, (Left, false), (Right, false));
        }

        public override string ToString()
        {
            return
                $"{(Left.Priority < Priority || Left is Or ? Left.ToString() : $"({Left})")}+{(Right.Priority < Priority || Right is Or ? Right.ToString() : $"({Right})")}";
        }

        public override string ToLatex()
        {
            return
                $"{(Left.Priority < Priority || Left is Or ? Left.ToLatex() : $"({Left.ToLatex()})")} \\lor {(Right.Priority < Priority || Right is Or ? Right.ToLatex() : $"({Right.ToLatex()})")}";
        }
    }
}