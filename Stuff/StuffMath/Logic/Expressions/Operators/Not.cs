﻿using System.Collections.Generic;

namespace Stuff.StuffMath.Logic.Expressions.Operators
{
    public class Not : Expression
    {
        public Not(Expression arg)
        {
            Arg = arg;
        }

        public override string Name => "Not";

        public Expression Arg { get; }

        public override double Priority => 1;

        public override bool Evaluate(Dictionary<string, bool> values = null)
        {
            return !Arg.Evaluate(values);
        }

        public override Expression Reduce(Dictionary<string, bool> values = null)
        {
            var argReduced = Arg.Reduce(values);
            if (argReduced is ValueExpression argValue)
                return !argReduced.Evaluate();
            if (argReduced is NAnd nand)
                return new And(nand.Left, nand.Right);
            if (argReduced is NOr nor)
                return new Or(nor.Left, nor.Right);
            if (argReduced is XOr xor)
                return new Iff(xor.Left, xor.Right);
            if (argReduced is And and)
                return new NAnd(and.Left, and.Right);
            if (argReduced is Or or)
                return new NOr(or.Left, or.Right);
            if (argReduced is Iff iff)
                return new XOr(iff.Left, iff.Right);
            return !argReduced;
        }

        public override Expression ToNormalForm()
        {
            return Arg.Negate();
        }

        public override Expression Negate()
        {
            return Arg.ToNormalForm();
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Arg.ContainedVariables(vars);
        }

        public override bool ContainsVariable(string variable)
        {
            return Arg.ContainsVariable(variable);
        }

        protected override bool InternalTableau(IReadOnlyList<(Expression exp, bool value)> expressions,
            IReadOnlyDictionary<string, bool> values, bool value)
        {
            return InternalTableauNextExp(expressions, values, (Arg, !value));
        }

        public override string ToString()
        {
            return $"!{(Arg.Priority < Priority || Arg is Not ? Arg.ToString() : $"({Arg})")}";
        }

        public override string ToLatex()
        {
            return $"\\neg {(Arg.Priority < Priority || Arg is Not ? Arg.ToLatex() : $"({Arg.ToLatex()})")}";
        }
    }
}