﻿using System;
using System.Collections.Generic;

namespace Stuff.StuffMath.Logic.Expressions.Operators
{
    public class And : Expression
    {
        public And(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public And(params Expression[] expressions)
        {
            Left = expressions[0];
            for (var i = 1; i < expressions.Length - 1; ++i)
                Left = new And(Left, expressions[i]);
            Right = expressions[expressions.Length - 1];
        }

        public override string Name => "And";

        public Expression Left { get; }

        public Expression Right { get; }

        public override double Priority => 2;

        public override bool Evaluate(Dictionary<string, bool> values = null)
        {
            return Left.Evaluate(values) && Right.Evaluate(values);
        }

        public override Expression Reduce(Dictionary<string, bool> values = null)
        {
            var leftReduced = Left.Reduce(values);
            var rightReduced = Right.Reduce(values);
            if (leftReduced is ValueExpression leftValue)
                return leftValue.Evaluate() ? rightReduced : false;

            if (rightReduced is ValueExpression rightValue)
                return rightValue.Evaluate() ? leftReduced : false;

            return rightReduced.IsEqual(leftReduced) ? rightReduced : new And(leftReduced, rightReduced);
        }

        public override Expression ToNormalForm()
        {
            return new And(Left.ToNormalForm(), Right.ToNormalForm());
        }

        public override Expression Negate()
        {
            return new Or(Left.Negate(), Right.Negate());
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Left.ContainedVariables(Right.ContainedVariables(vars));
        }

        public override bool ContainsVariable(string variable)
        {
            return Left.ContainsVariable(variable) || Right.ContainsVariable(variable);
        }

        protected override bool InternalTableau(IReadOnlyList<(Expression exp, bool value)> expressions,
            IReadOnlyDictionary<string, bool> values, bool value)
        {
            if (value) return InternalTableauNextExp(expressions, values, (Left, true), (Right, true));

            if (!InternalTableauNextExp(expressions, values, (Left, false)))
                return InternalTableauNextExp(expressions, values, (Right, false));
            return true;
        }

        public override string ToString()
        {
            return
                $"{(Left.Priority < Priority || Left is And ? Left.ToString() : $"({Left})")}*{(Right.Priority < Priority || Right is And ? Right.ToString() : $"({Right})")}";
        }

        public override string ToLatex()
        {
            return
                $"{(Left.Priority < Priority || Left is And ? Left.ToLatex() : $"({Left.ToLatex()})")} \\land {(Right.Priority < Priority || Right is And ? Right.ToLatex() : $"({Right.ToLatex()})")}";
        }
    }
}