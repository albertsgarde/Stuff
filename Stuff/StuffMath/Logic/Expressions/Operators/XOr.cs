﻿using System.Collections.Generic;

namespace Stuff.StuffMath.Logic.Expressions.Operators
{
    public class XOr : Expression
    {
        public XOr(Expression left, Expression right)
        {
            Left = left;
            Right = right;
        }

        public override string Name => "XOr";

        public Expression Left { get; }

        public Expression Right { get; }

        public override double Priority => 5;

        public override bool Evaluate(Dictionary<string, bool> values = null)
        {
            return Left.Evaluate(values) != Right.Evaluate(values);
        }

        public override Expression Reduce(Dictionary<string, bool> values = null)
        {
            var leftReduced = Left.Reduce(values);
            var rightReduced = Right.Reduce(values);
            if (leftReduced.IsEqual(rightReduced)) return false;

            if (leftReduced.IsNegation(rightReduced)) return true;

            if (leftReduced is ValueExpression leftValue)
            {
                if (leftValue.Evaluate())
                    return !rightReduced;
                return rightReduced;
            }

            if (rightReduced is ValueExpression rightValue)
            {
                if (rightValue.Evaluate())
                    return !leftReduced;
                return leftReduced;
            }

            return new XOr(leftReduced, rightReduced);
        }

        public override Expression ToNormalForm()
        {
            return new Or(new And(Left.Negate(), Right.ToNormalForm()), new And(Left.ToNormalForm(), Right.Negate()));
        }

        public override Expression Negate()
        {
            return new Iff(Left.ToNormalForm(), Right.ToNormalForm());
        }

        public override HashSet<string> ContainedVariables(HashSet<string> vars)
        {
            return Left.ContainedVariables(Right.ContainedVariables(vars));
        }

        public override bool ContainsVariable(string variable)
        {
            return Left.ContainsVariable(variable) || Right.ContainsVariable(variable);
        }

        protected override bool InternalTableau(IReadOnlyList<(Expression exp, bool value)> expressions,
            IReadOnlyDictionary<string, bool> values, bool value)
        {
            if (!value)
            {
                if (!InternalTableauNextExp(expressions, values, (Left, true), (Right, true)))
                    return InternalTableauNextExp(expressions, values, (Left, false), (Right, false));
                return true;
            }

            if (!InternalTableauNextExp(expressions, values, (Left, true), (Right, false)))
                return InternalTableauNextExp(expressions, values, (Left, false), (Right, true));
            return true;
        }

        public override string ToString()
        {
            return
                $"{(Left.Priority < Priority ? Left.ToString() : $"({Left})")}!={(Right.Priority < Priority ? Right.ToString() : $"({Right})")}";
        }

        public override string ToLatex()
        {
            return
                $"{(Left.Priority < Priority ? Left.ToLatex() : $"({Left.ToLatex()})")} \\otimes {(Right.Priority < Priority ? Right.ToLatex() : $"({Right.ToLatex()})")}";
        }
    }
}