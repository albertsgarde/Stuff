﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Stuff;

namespace Stuff.Chemistry
{
    public static class Misc
    {
        /* public struct Reaction
        {
            public Dictionary<Formula, int> reactants { get; private set; }

            public Dictionary<Formula, int> products { get; private set; }

            public Reaction(string reaction)
            {
                string[] parts = reaction.Split('=');

                string[] reactantFormulas = parts[0].Split(new string[] { " + " }, StringSplitOptions.None);
                string[] productFormulas = parts[1].Split(new string[] { " + " }, StringSplitOptions.None);

                reactants = new Dictionary<Formula, int>();
                foreach (string reactant in reactantFormulas)
                {
                    string multiplier = "";
                    int index = 0;
                    for (; Regex.Matches("" + reactant[index], "[A-Z]").Count == 1; index++)
                        multiplier += reactant[index];
                    reactants[new Formula(reactant.Substring(index))] = int.Parse(multiplier);
                }

                products = new Dictionary<Formula, int>();
                foreach (string product in productFormulas)
                {
                    string multiplier = "";
                    int index = 0;
                    for (; Regex.Matches("" + product[index], "[A-Z]").Count == 1; index++)
                        multiplier += product[index];
                    products[new Formula(product.Substring(index))] = int.Parse(multiplier);
                }
            }*/

        /*public string Redox(Acidity acidity)
            {
                int h = 0;
                int oh = 0;

                foreach(KeyValuePair<Formula, int> reactant in reactants)
                {
                    if (reactant.Key.Elements[Elements.Single(e => e.AtomicNumber == 1)] == 1 && reactant.Key.Elements.Count() == 1 && reactant.Key.Charge == 1)
                        h += reactant.Value;
                    else if (reactant.Key.Elements[Elements.Single(e => e.AtomicNumber == 1)] == 1 && reactant.Key.Elements[Elements.Single(e => e.AtomicNumber == 8)] == 1 && reactant.Key.Elements.Count() == 2 && reactant.Key.Charge == -1)
                        oh += reactant.Value;
                }

                foreach (KeyValuePair<Formula, int> product in products)
                {
                    if (product.Key.Elements[Elements.Single(e => e.AtomicNumber == 1)] == 1 && product.Key.Elements.Count() == 1 && product.Key.Charge == 1)
                        h -= product.Value;
                    else if (product.Key.Elements[Elements.Single(e => e.AtomicNumber == 1)] == 1 && product.Key.Elements[Elements.Single(e => e.AtomicNumber == 8)] == 1 && product.Key.Elements.Count() == 2 && product.Key.Charge == -1)
                        oh -= product.Value;
                }


            }*/
        //}

        public const double MOL = 6.0221415e23;

        public const double GRAMS_PER_UNIT = 1/MOL;

        /// <summary>
        ///     L*bar/mol*K
        ///     V*p/n*T
        /// </summary>
        public const double GAS_CONSTANT = 0.0831;

        /// <summary>
        ///     K
        /// </summary>
        public const double STANDARD_TEMPERATURE = 293.15;

        /// <summary>
        ///     bar
        /// </summary>
        public const double STANDARD_PRESSURE = 1;

        public const double CELCIUS0 = 273.15;

        /// <summary>
        ///     L/mol
        /// </summary>
        public const double STANDARD_VOLUME_PER_MOL = 24.4;

        public static IReadOnlyDictionary<Element, int> ElementsFromString(string s)
        {
            return ElementsFromString(s, new Dictionary<Element, int>(), 1);
        }

        /// <summary>
        ///     Nested [] brackets do not work.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="elements"></param>
        /// <param name="multiplier"></param>
        /// <returns></returns>
        private static Dictionary<Element, int> ElementsFromString(string s, Dictionary<Element, int> elements,
            int multiplier = 1)
        {
            var result = new Dictionary<Element, int>();

            var startBracket = -1;
            var braces = 0;
            for (var index = 0; index < s.Length; index++)
            {
                if (s[index] == '[')
                {
                    if (startBracket == -1)
                        startBracket = index;
                    else 
                        throw new ArgumentException("Nested [] brackets do not work.");
                    braces++;
                }

                if (s[index] == ']')
                {
                    braces--;
                    if (braces < 0)
                    {
                        throw new ArgumentException("Bracket at " + index + " missing start bracket.");
                    }

                    if (braces == 0)
                    {
                        var bracketEnd = index;
                        var subMultiplier = "";
                        index++;
                        for (; index != s.Length && Regex.Matches("" + s[index], "[0-9]").Count == 1; index++)
                            subMultiplier += s[index];
                        result = ElementsFromString(s.Substring(startBracket + 1, bracketEnd - (startBracket + 1)),
                            result, int.Parse(subMultiplier));
                        index -= index - startBracket;
                        s = s.Substring(0, startBracket) + s.Substring(bracketEnd + subMultiplier.Length + 1,
                                s.Length - (bracketEnd + subMultiplier.Length + 1));
                    }
                }
            }

            for (var index = 0; index < s.Length;)
            {
                var part = "" + s[index++];

                while (index < s.Length && Regex.Matches("" + s[index], "[A-Z]").Count < 1)
                {
                    part += s[index];
                    ++index;
                }

                var elementSymbol = "";
                var partIndex = 0;
                for (; partIndex != part.Length && Regex.Matches("" + part[partIndex], "[0-9]").Count < 1; partIndex++)
                    elementSymbol += part[partIndex];
                var amount = part.Substring(partIndex);
                
                if (!PeriodicTable.IsElement(elementSymbol))
                    throw new ArgumentException("Element " + elementSymbol + " does not exist.");
                var element = PeriodicTable.Element(elementSymbol);

                if (result.ContainsKey(element))
                    result[element] += amount == "" ? 1 : int.Parse(amount);
                else
                    result[element] = amount == "" ? 1 : int.Parse(amount);
            }

            foreach (var (k, v) in result)
                if (elements.ContainsKey(k))
                    elements[k] += v * multiplier;
                else
                    elements[k] = v * multiplier;
            return elements;
        }
    }
}