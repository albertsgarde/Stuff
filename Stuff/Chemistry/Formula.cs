﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Stuff.StuffMath;

namespace Stuff.Chemistry
{
    public class Formula : IComparable<Formula>, IReadOnlyDictionary<Element, int>, ILatexable
    {
        public ImmutableDictionary<Element, int> Elements { get; }

        public int Charge { get; }

        public static readonly Formula EMPTY = new Formula(new Dictionary<Element, int>(), 0);

        public Formula(string formula)
        {
            if (formula.Contains('*'))
            {
                var parts = formula.Split('*');
                Charge = parts[1] == "" ? 1 : int.Parse(parts[1]);
                formula = parts[0];
            }

            if (formula.Contains('-'))
            {
                var parts = formula.Split('-');
                Charge = parts[1] == "" ? -1 : 0 - int.Parse(parts[1]);
                formula = parts[0];
            }

            Elements = Misc.ElementsFromString(formula).ToImmutableDictionary();
        }

        public Formula(IReadOnlyDictionary<Element, int> elements, int charge = 0)
        {
            if (elements.Any(kvp => kvp.Value < 0))
                throw new ArgumentException("Indexes must be non-negative.");
            Elements = elements.Where(kvp => kvp.Value != 0).ToImmutableDictionary();
            Charge = charge;
        }

        public static ImmutableDictionary<Element, double> FromMassDistribution(IReadOnlyDictionary<ElementArg, double> elements, int charge = 0)
        {
            var mols = elements
                .Select(kvp => new KeyValuePair<Element, double>(kvp.Key, kvp.Value / kvp.Key.Element.AtomicMass))
                .ToImmutableDictionary();
            var min = mols.Values.Min();
            return mols.Select(kvp => new KeyValuePair<Element, double>(kvp.Key, kvp.Value / min)).ToImmutableDictionary();
        }

        public double MolarMass => Elements.Sum(kvp => kvp.Key.AtomicMass * kvp.Value);

        public int this[ElementArg ea] => Elements.ContainsKey(ea) ? Elements[ea] : 0;

        public static bool operator ==(Formula formula1, Formula formula2) =>
            formula1.Elements.EqualTo(formula2.Elements) && formula1.Charge == formula2.Charge;


        public static bool operator !=(Formula formula1, Formula formula2) => !(formula1 == formula2);

        public bool ContainsElement(ElementArg ea) => Elements.ContainsKey(ea);

        public double MassPercent(ElementArg ea) => this[ea] * ea.Element.AtomicMass / MolarMass;

        public double Mols(double mass) => mass / MolarMass;

        public double Mass(double mols, ElementArg ea) => mols * MolarMass * MassPercent(ea);

        public IEnumerable<Element> ContainedElements() => Elements.Keys;

        /// <returns>The total oxidation state of all non-oxygen and non-hydrogen elements.</returns>
        public int OxidationState() => Charge - this[1] + this[8] * 2;

        public Formula React(Formula formula)
        {
            return new Formula(Elements.Add(formula.Elements), Charge + formula.Charge);
        }

        public string ToHillNotation(string subscriptStart = "", string subscriptEnd = "")
        {
            // Formula contains Carbon
            if (ContainsElement("C"))
            {
                var result = "C" + (this["C"] == 1 ? "" : subscriptStart + this["C"] + subscriptEnd);
                if (ContainsElement("H"))
                    result += "H" + (this["H"] == 1 ? "" : subscriptStart + this["H"] + subscriptEnd);
                return Elements.Where(kvp => kvp.Key.Symbol != "C" && kvp.Key.Symbol != "H").OrderBy(x => x.Key.Symbol)
                    .Aggregate(result, (total, kvp) => total + kvp.Key.Symbol + (kvp.Value == 0 ? "" : subscriptStart + kvp.Value + subscriptEnd));
            }
            // Formula doesn't contain Carbon.
            return Elements.OrderBy(x => x.Key.Symbol).Aggregate("", (total, kvp) => total + kvp.Key.Symbol + (kvp.Value == 1 ? "" : subscriptStart + kvp.Value + subscriptEnd));
        }

        public int CompareTo(Formula other)
        {
            // Convert both formulas to Hill notation.
            var thisHill = ToHillNotation();
            var otherHill = other.ToHillNotation();

            for (var i = 0; i < Math.Min(thisHill.Length, otherHill.Length); ++i)
            {
                if (thisHill[i] < otherHill[i])
                    return -1;
                if (thisHill[i] > otherHill[i])
                    return 1;
            }

            return thisHill.Length - otherHill.Length;
        }


        public int Count => Elements.Count;

        bool IReadOnlyDictionary<Element, int>.ContainsKey(Element key) => ContainsElement(key);

        public bool TryGetValue(Element key, out int value) => Elements.TryGetValue(key, out value);

        public int this[Element key] => Elements.ContainsKey(key) ? Elements[key] : 0;

        public IEnumerable<Element> Keys => Elements.Keys;
        public IEnumerable<int> Values => Elements.Values;

        public override bool Equals(object obj)
        {
            return obj is Formula formula && this == formula;
        }

        public override int GetHashCode()
        {
            var hashFields = new List<object>();
            foreach (var e in Elements.Keys.OrderBy(e => e.AtomicNumber))
            {
                hashFields.Add(e);
                hashFields.Add(Elements[e]);
            }

            hashFields.Add(Charge);
            return Stuff.Misc.HashCode(17, 23, hashFields);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<KeyValuePair<Element, int>> GetEnumerator() => Elements.GetEnumerator();

        public override string ToString()
        {
            var result = ToHillNotation();
            if (Charge == 0)
                return result;
            if (Charge > 0)
                return result + "+" + Charge;
            return result + "-" + -Charge;
        }

        public string ToLatex()
        {
            var result = ToHillNotation("_{", "}");
            if (Charge == 0)
                return result;
            if (Charge > 0)
                return result + "^{" + (Charge == 1 ? "" : "" + Charge) + "+}";
            return result + "^{" + (Charge == -1 ? "" : "" + -Charge) + "-}";
        }
    }

    public static class FormulaExtensions
    {
        public static ImmutableDictionary<Element, double> MassPercents(this Formula formula) => formula.Keys
            .Select(e => new KeyValuePair<Element, double>(e, formula.MassPercent(e)))
            .ToImmutableDictionary();

        public static Formula EmpiricalFormula(this Formula f)
        {
            var gcd = Basic.GCD(f.Elements.Values);
            return new Formula(f.Select(kvp => new KeyValuePair<Element, int>(kvp.Key, kvp.Value/gcd)).ToImmutableDictionary(), f.Charge);
        }
    }

    public class FormulaArg
    {
        public Formula Formula { get; }

        private FormulaArg(Formula formula)
        {
            Formula = formula;
        }

        public static implicit operator Formula(FormulaArg fa) => fa.Formula;

        public static implicit operator FormulaArg(Formula f) => new FormulaArg(f);

        public static implicit operator FormulaArg(string s) => new Formula(s);
    }
}
