﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath;
using Stuff;
using Stuff.StuffMath.Structures;
using Stuff.StuffMath.LinearAlgebra;

namespace Stuff.Chemistry
{

    public class FormulaList : IReadOnlyDictionary<Formula, int>, ILatexable
    {
        public ImmutableDictionary<Formula, int> List { get; }

        public FormulaList(IEnumerable<KeyValuePair<Formula, int>> list)
        {
            List = ListFromIEnumerable(list);
        }

        public FormulaList(IEnumerable<Formula> list) : this(list.Select(f => new KeyValuePair<Formula, int>(f, 1)).ToImmutableDictionary())
        {
        }

        public FormulaList(params Formula[] formula) : this(formula.Select(f => new KeyValuePair<Formula, int>(f, 1)))
        {
        }

        public FormulaList(string s)
        {
            var formulas = s.Split('+').Select(f => f.Trim());
            if (formulas.Any(f => f.Any(c => !char.IsLetterOrDigit(c) && c != '*' && c != '-')))
                throw new ArgumentException("Invalid string.");
            var dict = new Dictionary<Formula, int>();
            List = ListFromIEnumerable(from f in formulas
                select new KeyValuePair<Formula, int>(new Formula(f.Substring(f.FirstIndexOf(char.IsLetter))),
                    char.IsDigit(f[0]) ? int.Parse(new string(f.TakeWhile(char.IsDigit).ToArray())) : 1));
        }

        private ImmutableDictionary<Formula, int> ListFromIEnumerable(IEnumerable<KeyValuePair<Formula, int>> list)
        {
            if (list.Any(kvp => kvp.Value < 0))
                throw new ArgumentException("Negative amounts of formulas are impossible.");
            var dict = new Dictionary<Formula, int>();
            foreach (var (f, amount) in list.Where(kvp => kvp.Value != 0))
            {
                if (dict.ContainsKey(f))
                    dict[f] += amount;
                else
                    dict[f] = amount;
            }

            return dict.ToImmutableDictionary();
        }

        public int this[Formula key] => List[key];

        public int Count => List.Count;

        public int TotalCharge => List.Sum(kvp => kvp.Key.Charge * kvp.Value);

        public IEnumerable<Formula> Formulas => List.Keys;

        public bool ContainsElement(ElementArg ea) => List.Keys.Any(f => f.ContainsElement(ea));

        public IEnumerable<Element> ContainedElements() => List.Keys.Aggregate((IEnumerable<Element>)new HashSet<Element>(), (total, f) => total.Union(f.ContainedElements()));

        public int ElementAmount(ElementArg ea) => List.Sum(kvp => kvp.Key[ea] * kvp.Value);

        public FormulaList RemoveCoefficients() => new FormulaList(Formulas);

        public Formula ToFormula() => Formulas.Aggregate(Formula.EMPTY, (total, f) => total.React(f));

        public double MolarMass => this.Sum(kvp => kvp.Key.MolarMass * kvp.Value);

        public double MassPercent(Formula f) => f.MolarMass * this[f] / MolarMass;

        public FormulaList Union(FormulaList fl) => new FormulaList(List.Union(fl.List));


        bool IReadOnlyDictionary<Formula, int>.ContainsKey(Formula key) => List.ContainsKey(key);

        public bool TryGetValue(Formula key, out int value) => List.TryGetValue(key, out value);

        public IEnumerable<Formula> Keys => List.Keys;

        public IEnumerable<int> Values => List.Values;
        
        /// <param name="elements">Which elements to include in which order.</param>
        public Matrix<Rational> ToMatrix(IReadOnlyList<Element> elements) =>
            new Matrix<Rational>(new List<IEnumerable<Rational>> {List.Keys.Select(f => new Rational(f.Charge))}
                .Concat(elements.Select(e => List.Keys.Select(f => new Rational(f[e])))));

        public Matrix<Rational> ToMatrix() => ToMatrix(PeriodicTable.Elements);

        public IEnumerator<KeyValuePair<Formula, int>> GetEnumerator() => List.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public override string ToString()
        {
            return Count == 0 ? "" : List.Aggregate("", (total, kvp) => total + (kvp.Value == 1 ? "" : "" + kvp.Value) + kvp.Key + " + ").CutEnd(3);
        }

        public string ToLatex()
        {
            return Count == 0 ? "" : List.Aggregate("", (total, kvp) => total + (kvp.Value == 1 ? "" : "" + kvp.Value) + kvp.Key.ToLatex() + " + ").CutEnd(3);
        }
    }
}
