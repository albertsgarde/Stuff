﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath.Expressions;

namespace Stuff.Chemistry
{
    public class Element
    {
        public string Name { get; }

        public string Symbol { get; }

        public int AtomicNumber { get; }

        public double AtomicMass { get; }

        public Element(string name, string symbol, int atomicNumber, double atomicMass)
        {
            Name = name;
            Symbol = symbol;
            AtomicNumber = atomicNumber;
            AtomicMass = atomicMass;
        }

        public override int GetHashCode() => AtomicNumber.GetHashCode();

        public override string ToString() => $"{{{Name}: {Symbol}, {AtomicNumber}, {AtomicMass}}}";
    }

    public class ElementArg
    {
        public Element Element { get; }

        private ElementArg(Element element)
        {
            Element = element;
        }

        public static implicit operator Element(ElementArg ea) => ea.Element;

        public static implicit operator ElementArg(Element e) => new ElementArg(e);

        public static implicit operator ElementArg(string nameOrSymbol) => new ElementArg(PeriodicTable.Element(nameOrSymbol));

        public static implicit operator ElementArg(int number) => new ElementArg(PeriodicTable.Element(number));
    }
}
