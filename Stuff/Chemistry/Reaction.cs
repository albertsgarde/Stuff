﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Stuff.StuffMath;
using Stuff.StuffMath.LinearAlgebra;
using Stuff.StuffMath.Structures;

namespace Stuff.Chemistry
{
    public class Reaction : ILatexable
    {
        public FormulaList Reactants { get; }

        public FormulaList Products { get; }

        public enum RedoxType { Acidic, Basic, Neutral }

        private static readonly FormulaList H2O = new FormulaList("H2O");

        private static readonly FormulaList HPlus = new FormulaList("H*");

        private static readonly FormulaList OHMinus = new FormulaList("OH-");

        public Reaction(IEnumerable<KeyValuePair<Formula, int>> reactants,
            IEnumerable<KeyValuePair<Formula, int>> products)
        {
            Reactants = new FormulaList(reactants);
            Products = new FormulaList(products);
        }

        public Reaction(IEnumerable<Formula> reactants, IEnumerable<Formula> products)
        {
            Reactants = new FormulaList(reactants);
            Products = new FormulaList(products);
        }

        public Reaction(string s)
        {
            var sides = s.Split("->");
            Reactants = new FormulaList(sides[0].Trim());
            Products = new FormulaList(sides[1].Trim());
        }

        public bool IsBalanced()
        {
            return Reactants.TotalCharge == Products.TotalCharge &&
                   Reactants.ContainedElements().Union(Products.ContainedElements())
                       .All(e => Reactants.ElementAmount(e) == Products.ElementAmount(e));
        }

        /// <summary>
        /// Balances the reaction.
        /// </summary>
        /// <exception cref="InvalidOperationException">If there is not a single solution.</exception>
        /// <returns>This reaction with the minimal coefficients necessary to be balanced.</returns>
        public Reaction Balance()
        {
            var reactantMatrix = Reactants.ToMatrix();
            var productMatrix = Products.ToMatrix();
            var reactionMatrix = reactantMatrix.JoinHorizontal(productMatrix.AdditiveInverse());

            var kernel = reactionMatrix.Kernel(new Vector<Rational>());
            Console.WriteLine(kernel.ToLatex());
            if (!kernel.Constant.IsNull())
                throw new Exception("Something wrong with Kernel function.");
            if (kernel.Coefficients.Count == 0)
                throw new InvalidOperationException("No solutions exist.");
            if (kernel.Coefficients.Count > 1)
                throw new InvalidOperationException("Multiple solutions found.");
            var solution = kernel.Coefficients[0];
            if (solution.All(r => r > 0) || solution.All(r => r < 0))
                throw new InvalidOperationException("No solutions exist.");
            solution *= Basic.LCM(solution.Select(r => (int)r.Denominator));

            var counter = ContainerUtils.Count();
            var newReactants = Reactants.Formulas.Zip(counter).Select(f => new KeyValuePair<Formula, int>(f.Item1, (int)solution[f.Item2].Numerator));
            counter = ContainerUtils.Count();
            var newProducts = Products.Formulas.Zip(counter.Skip(Reactants.Count)).Select(f => new KeyValuePair<Formula, int>(f.Item1, (int)solution[f.Item2].Numerator));
            return new Reaction(new FormulaList(newReactants), new FormulaList(newProducts));
        }

        public Reaction BalanceRedox(RedoxType redoxType)
        {
            Reaction reaction;
            switch (redoxType)
            {
                case RedoxType.Acidic:
                    reaction = new Reaction(Reactants.Union(HPlus), Products.Union(H2O));
                    break;
                case RedoxType.Basic:
                    reaction = new Reaction(Reactants.Union(OHMinus), Products.Union(H2O));
                    break;
                case RedoxType.Neutral:
                    reaction = new Reaction(Reactants.Union(H2O), Reactants.Union(HPlus.Union(OHMinus)));
                    break;
                default:
                    throw new Exception("Unsupported redoxType.");
            }

            return reaction.Balance();
        }

        /// <returns>The amount of unknownFormula needed or created when using or creating knownMass of knownFormula assuming all other formulas are in excess.</returns>
        /// <remarks>Assumes the mass of the reactants is equal to the mass of the products.</remarks>
        public double Mass(FormulaArg knownFormula, double knownMass, FormulaArg unknownFormula)
        {
            var formulas = ToFormulaList();
            var mols = knownMass / (knownFormula.Formula.MolarMass * formulas[knownFormula]);
            return mols * formulas[unknownFormula] * unknownFormula.Formula.MolarMass;
        }

        public ImmutableDictionary<Formula, double> Mass(FormulaArg knownFormula, double knownMass)
        {
            return ToFormulaList().Keys
                .Select(f => new KeyValuePair<Formula, double>(f, Mass(knownFormula, knownMass, f)))
                .ToImmutableDictionary();
        }
        
        public FormulaList ToFormulaList() => Reactants.Union(Products);

        public override string ToString() => "" + Reactants + " -> " + Products;

        public string ToLatex() => Reactants.ToLatex() + " \\rightarrow " + Products.ToLatex();
    }
}
