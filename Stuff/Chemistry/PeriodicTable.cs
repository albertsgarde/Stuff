﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.Chemistry
{
    public static class PeriodicTable
    {
        private static ElementCollection elements;

        public static ElementCollection Elements => elements ?? (elements = new ElementCollection("Assets/Chemistry/periodicTable.csv"));

        public static Element ByName(string name) => Elements.Single(e => e.Name == name);

        public static Element ByNumber(int number) => Elements.Single(e => e.AtomicNumber == number);

        public static Element BySymbol(string symbol) => Elements.Single(e => e.Symbol == symbol);

        public static Element Element(string nameOrSymbol) =>
            Elements.Single(e => e.Name == nameOrSymbol || e.Symbol == nameOrSymbol);

        public static Element Element(int number) => ByNumber(number);

        public static bool IsElement(string nameOrSymbol) =>
            Elements.Any(e => e.Name == nameOrSymbol || e.Symbol == nameOrSymbol);

        public static bool IsElement(int number) =>
            Elements.Any(e => e.AtomicNumber == number);

        public class ElementCollection : IReadOnlyList<Element>
        {
            private readonly IReadOnlyList<Element> elements;

            public ElementCollection(string path)
            {
                elements = CSVReader.ReadFile(path, LoadElement).ToList();
                foreach (var element in elements)
                {
                    if (elements.Where(e => e != element).Any(e => e.Symbol == element.Symbol))
                        throw new InvalidDataException("Element symbols must be unique. Symbol: " + element.Symbol);
                    if (elements.Where(e => e != element).Any(e => e.Name == element.Name))
                        throw new InvalidDataException("Element names must be unique. Name: " + element.Name);
                    if (elements.Where(e => e != element).Any(e => e.AtomicNumber == element.AtomicNumber))
                        throw new InvalidDataException("Atomic numbers must be unique. Number: " + element.AtomicNumber);
                }
            }

            public Element this[int index] => elements[index];

            public int Count => elements.Count;

            private static Element LoadElement(IReadOnlyList<string> data)
            {
                var name = data[2].Trim();
                var symbol = data[1].Trim();
                var numberString = data[0].Trim();
                var massString = data[3].Trim();

                if (!int.TryParse(numberString, out var number))
                    throw new ArgumentException("Atomic number must be an integer.");

                if (massString.Contains("("))
                    massString = massString.Substring(0, massString.IndexOf("("));
                if (massString.StartsWith("["))
                    massString = massString.Substring(1, massString.Length - 2);

                if (!double.TryParse(massString, out var mass))
                    throw new ArgumentException("Mass must be a number.");

                return new Element(name, symbol, number, mass);
            }

            public IEnumerator<Element> GetEnumerator() => elements.GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        }
    }
}
