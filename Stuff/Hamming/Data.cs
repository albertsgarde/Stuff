﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Stuff.Hamming
{
    public class Data
    {
        private object addLoc = new object();

        private readonly List<DataBatch> dataBatches;

        private readonly List<TimeSpan> times;

        public Data(double p, int batchSize)
        {
            P = p;
            BatchSize = batchSize;
            dataBatches = new List<DataBatch>();
            times = new List<TimeSpan>();
        }

        public double P { get; }

        public int BatchSize { get; }

        public IEnumerable<DataBatch> DataBatches()
        {
            return dataBatches;
        }

        public void AddBatch(DataBatch batch)
        {
            dataBatches.Add(batch);
        }

        public void AddTime(TimeSpan time)
        {
            times.Add(time);
        }

        public int Failures()
        {
            return dataBatches.Select(x => x.Failures).Sum();
        }

        public int Total()
        {
            return dataBatches.Select(x => x.Total).Sum();
        }

        public double TotalRatio()
        {
            return (double) Failures() / Total();
        }

        public void PrintTimes()
        {
            foreach (var time in times)
                Console.WriteLine(time);
        }

        public void Print()
        {
            foreach (var batch in dataBatches)
                Console.WriteLine(batch);
            Console.WriteLine("Total: " + Failures() + "/" + Total() + " = " + TotalRatio());
        }

        public class DataBatch
        {
            public DataBatch(int total, int failures)
            {
                Total = total;
                Failures = failures;
            }

            public int Total { get; }

            public int Failures { get; }

            public double Ratio()
            {
                return (double) Failures / Total;
            }

            public override string ToString()
            {
                return "" + Failures + "/" + Total + " = " + Ratio();
            }
        }
    }
}