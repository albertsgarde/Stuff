﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Stuff
{
    public static class Misc
    {
        private const long DATE_TIME_TICKS_PER_MICRO = 10;

        public static string EmptyString(int length = 0)
        {
            return UniformString(length, ' ');
        }

        public static string UniformString(int length = 0, char symbol = ' ')
        {
            var result = "";
            for (var i = 0; i < length; ++i)
                result += symbol;
            return result;
        }

        public static bool DoubleEquals(double value1, double value2, double tolerance = 0.00000001)
        {
            return value1 - value2 < tolerance && value2 - value1 < tolerance;
        }

        public static bool FloatEquals(float value1, float value2, float tolerance = 0.000001f)
        {
            return value1 - value2 < tolerance && value2 - value1 < tolerance;
        }

        public static string Flip(this string s)
        {
            var result = "";
            for (var loop = s.Length - 1; loop >= 0; loop--)
                result += s[loop];
            return result;
        }

        public static List<double>[] ReadData(string dataFile)
        {
            var file = new StreamReader(dataFile);
            var line = file.ReadLine();
            var datums = line.Split('	');
            var result = new List<double>[datums.Length];
            for (var i = 0; i < result.Length; i++)
            {
                result[i] = new List<double>
                {
                    double.Parse(datums[i])
                };
            }

            while (!file.EndOfStream)
            {
                line = file.ReadLine();
                datums = line.Split('	');
                for (var i = 0; i < result.Length; i++)
                    result[i].Add(double.Parse(datums[i]));
            }

            file.Close();
            return result;
        }

        public static IEnumerable<KeyValuePair<double, double>> ReadData(string dataFile, int xIndex, int yIndex)
        {
            var data = ReadData(dataFile);
            return data[xIndex].Zip(data[yIndex], (x, y) => new KeyValuePair<double, double>(x, y));
        }

        public static string Indent(this string s, string indent)
        {
            return indent + s.Replace(Environment.NewLine, Environment.NewLine + indent);
        }

        public static IList<T> TopologicalSort<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> getDependencies)
        {
            var sorted = new List<T>();
            var visited = new Dictionary<T, bool>();

            foreach (var item in source) Visit(item, getDependencies, sorted, visited);

            return sorted;
        }

        private static void Visit<T>(T item, Func<T, IEnumerable<T>> getDependencies, ICollection<T> sorted,
            IDictionary<T, bool> visited)
        {
            var alreadyVisited = visited.TryGetValue(item, out var inProcess);

            if (alreadyVisited)
            {
                if (inProcess) throw new ArgumentException("Cyclic dependency found.");
            }
            else
            {
                visited[item] = true;

                var dependencies = getDependencies(item);
                if (dependencies != null)
                    foreach (var dependency in dependencies)
                        Visit(dependency, getDependencies, sorted, visited);

                visited[item] = false;
                sorted.Add(item);
            }
        }

        public static long Microseconds(this DateTime dt)
        {
            return dt.Ticks / DATE_TIME_TICKS_PER_MICRO;
        }

        public static long Milliseconds(this DateTime dt)
        {
            return dt.Ticks / TimeSpan.TicksPerMillisecond;
        }

        public static int HashCode(int start, int multiConstant, IEnumerable<object> objects)
        {
            unchecked
            {
                var hash = start;
                foreach (var obj in objects)
                    hash = hash * multiConstant + obj.GetHashCode();
                return hash;
            }
        }

        public static int HashCode(int start, int multiConstant, params object[] objects)
        {
            return HashCode(start, multiConstant, objects.AsEnumerable());
        }

        public static string RemoveWhiteSpace(this string s)
        {
            return Regex.Replace(s, @"\s+", "");
        }

        public static string[] Split(this string s, params string[] splitter) =>
            s.Split(splitter, StringSplitOptions.None);

        public static string CutEnd(this string s, int amount) => s.Substring(0, s.Length - amount);
    }
}