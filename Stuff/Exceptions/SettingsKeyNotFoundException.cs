﻿using System;
using System.Runtime.Serialization;

namespace Stuff.Exceptions
{
    [Serializable]
    public class SettingsKeyNotFoundException : SettingsException
    {
        public SettingsKeyNotFoundException(string category, string key) : base(category, key)
        {
        }

        protected SettingsKeyNotFoundException(SerializationInfo si, StreamingContext sc) : base(si, sc)
        {
        }
    }
}