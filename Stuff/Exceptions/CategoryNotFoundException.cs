﻿using System;
using System.Runtime.Serialization;

namespace Stuff.Exceptions
{
    [Serializable]
    public class CategoryNotFoundException : SettingsException
    {
        public CategoryNotFoundException(string category, string key) : base(category, key)
        {
        }

        protected CategoryNotFoundException(SerializationInfo si, StreamingContext sc) : base(si, sc)
        {
        }
    }
}