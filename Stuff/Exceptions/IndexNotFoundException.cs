﻿using System;
using System.Runtime.Serialization;

namespace Stuff.Exceptions
{
    [Serializable]
    public class IndexNotFoundException : SettingsException
    {
        public IndexNotFoundException(string category, string key, int index) : base(category, key, index)
        {
        }

        protected IndexNotFoundException(SerializationInfo si, StreamingContext sc) : base(si, sc)
        {
        }
    }
}