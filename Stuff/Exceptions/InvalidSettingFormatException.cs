﻿namespace Stuff.Exceptions
{
    public class InvalidSettingFormatException : SettingsException
    {
        public InvalidSettingFormatException(string category, string key, int index, string value) : base(category, key,
            index)
        {
            Value = value;
        }

        public string Value { get; }
    }
}