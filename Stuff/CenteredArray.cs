﻿using System.Collections;
using System.Collections.Generic;

namespace Stuff
{
    public class CenteredArray<T> : IEnumerable<T>
    {
        /// <summary>
        ///     All the negative indices.
        /// </summary>
        private readonly T[] left;

        /// <summary>
        ///     All the positive indices and 0.
        /// </summary>
        private readonly T[] right;

        public CenteredArray(int length)
        {
            Length = length;

            left = new T[Length == 0 ? 0 : Length - 1];
            right = new T[Length];
        }

        public CenteredArray(int leftLength, int rightLength)
        {
            left = new T[leftLength];
            right = new T[rightLength];

            Length = left.Length + right.Length;
        }

        public int Length { get; }

        public int Start => -left.Length;

        public int End => right.Length;

        public T this[int index]
        {
            get
            {
                if (index >= 0)
                    return right[index];
                return left[index * -1 - 1];
            }
            set
            {
                if (index >= 0)
                    right[index] = value;
                else
                    left[index * -1 - 1] = value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var loop = Start; loop < End; loop++)
                yield return this[loop];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public string AsString()
        {
            var result = "{";
            foreach (var value in this) result += value + ",";
            return result.Substring(0, result.Length - 1) + "}";
        }
    }
}