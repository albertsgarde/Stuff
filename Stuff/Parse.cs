﻿using System;

namespace Stuff
{
    public static class Parse
    {
        public static int ParseInt(string s, string errorMessage)
        {
            if (!int.TryParse(s, out var result))
                throw new InvalidCastException(errorMessage);
            return result;
        }

        public static int ParseInt(string s)
        {
            return ParseInt(s, s + " is not an int");
        }

        public static float ParseFloat(string s, string errorMessage)
        {
            if (!float.TryParse(s, out var result))
                throw new InvalidCastException(errorMessage);
            return result;
        }

        public static float ParseFloat(string s)
        {
            return ParseFloat(s, s + " is not an float");
        }

        public static double ParseDouble(string s, string errorMessage)
        {
            if (!double.TryParse(s, out var result))
                throw new InvalidCastException(errorMessage);
            return result;
        }

        public static double ParseDouble(string s)
        {
            return ParseDouble(s, s + " is not an double");
        }
    }
}