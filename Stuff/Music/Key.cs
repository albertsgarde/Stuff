﻿namespace Stuff.Music
{
    public class Key
    {
        public Key(Tone note, Scale scale)
        {
            Root = note;
            Scale = scale;
        }

        public Scale Scale { get; }

        public Tone Root { get; }

        public Tone Step(int step, int modifier)
        {
            return Root + Scale.Step(step) + modifier;
        }
    }
}