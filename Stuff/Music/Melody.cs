﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Stuff.Music
{
    public class Melody : IEnumerable
    {
        private readonly List<Note> notes;

        public Melody(XElement element, IReadOnlyDictionary<string, double> noteValues)
        {
            notes = new List<Note>();
            foreach (var note in element.Elements("note"))
                notes.Add(new Note(note, noteValues));
        }

        /// <summary>
        ///     The duration of the melody in whole notes.
        /// </summary>
        public double Length
        {
            get { return notes.Sum(note => note.Value); }
        }

        public IEnumerator GetEnumerator()
        {
            return notes.GetEnumerator();
        }

        public void Play(BPM bpm)
        {
            foreach (var note in notes)
                note.Beep(bpm);
        }

        public override string ToString()
        {
            var result = "";
            foreach (var note in notes)
                result += note.Tone;
            return result;
        }
    }
}