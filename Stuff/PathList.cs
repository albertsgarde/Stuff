﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Stuff
{
    /// <summary>
    /// A class that make it easy to treat a collection of paths as though they were one directory.
    /// </summary>
    public class PathList : IEnumerable<string>
    {
        private readonly List<string> paths;

        public string Root { get; }

        /// <summary>
        /// The first path is taken as the root.
        /// This means that any other paths that are not rooted will be combined with that path.
        /// If the first path is not itself rooted, it will be combined with superRoot.
        /// </summary>
        /// <param name="paths">The paths to add.</param>
        /// <param name="superRoot">Serves as root for first path if it is not already rooted.</param>
        public PathList(IEnumerable<string> paths, string superRoot = "")
        {
            var firstPath = paths.First();
            Root = Path.IsPathRooted(firstPath) ? firstPath : Path.Combine(superRoot, firstPath);
            if (!Path.IsPathRooted(Root))
               throw new ArgumentException("Either the first path or superRoot must be rooted. First path: " + firstPath + " superRoot: " + superRoot);
            if (!File.GetAttributes(Root).HasFlag(FileAttributes.Directory))
                throw new DirectoryNotFoundException();
            this.paths = new List<string>
            {
                Root
            };
            foreach (var path in from p in paths where p != firstPath select FilePath(p))
                if (File.GetAttributes(path).HasFlag(FileAttributes.Directory) && !path.EndsWith("/"))
                    Add(path + "/");
                else
                    Add(path);
        }

        public PathList(params string[] paths) : this(paths.AsEnumerable())
        {
        }

        public IEnumerator<string> GetEnumerator()
        {
            return paths.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(string path)
        {
            paths.Add(Path.IsPathRooted(path) ? path : Path.Combine(Root, path));
        }

        public void Remove(string path)
        {
            paths.Remove(path);
        }

        public string FirstFile()
        {
            return Files().First();
        }

        public string FilePath(string path)
        {
            return Path.IsPathRooted(path) ? path : Path.Combine(Root, path);
        }

        public int DirCount()
        {
            return paths.Count(path => File.GetAttributes(path).HasFlag(FileAttributes.Directory));
        }

        public IEnumerable<string> Files()
        {
            foreach (var path in paths)
                if (File.GetAttributes(path).HasFlag(FileAttributes.Directory))
                    foreach (var file in Directory.EnumerateFiles(path, "*", SearchOption.AllDirectories))
                        yield return file;
                else
                    yield return path;
        }
    }
}