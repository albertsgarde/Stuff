﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Stuff
{
    public class CrossReferencedDictionary<V1, V2> : IEnumerable<KeyValuePair<V1, V2>>
    {
        private readonly Dictionary<V1, V2> mapper1;

        private readonly Dictionary<V2, V1> mapper2;

        public CrossReferencedDictionary()
        {
            mapper1 = new Dictionary<V1, V2>();
            mapper2 = new Dictionary<V2, V1>();
        }

        public V2 this[V1 key]
        {
            get => mapper1[key];
            set => Add(key, value);
        }

        public V1 this[V2 key]
        {
            get => mapper2[key];
            set => Add(value, key);
        }

        public Dictionary<V1, V2>.KeyCollection Keys1 => mapper1.Keys;

        public Dictionary<V2, V1>.KeyCollection Keys2 => mapper2.Keys;

        public IEnumerator<KeyValuePair<V1, V2>> GetEnumerator()
        {
            return mapper1.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Clear()
        {
            mapper1.Clear();
            mapper2.Clear();
        }

        public int Count()
        {
            return mapper1.Count();
        }

        public bool Contains(V1 v1)
        {
            return mapper1.ContainsKey(v1);
        }

        public bool Contains(V2 v2)
        {
            return mapper2.ContainsKey(v2);
        }

        private void Add(V1 v1, V2 v2)
        {
            Remove(v1);
            Remove(v2);
            mapper1[v1] = v2;
            mapper2[v2] = v1;
        }

        public void Remove(V1 key)
        {
            if (Contains(key))
            {
                mapper2.Remove(mapper1[key]);
                mapper1.Remove(key);
            }
        }

        public void Remove(V2 key)
        {
            if (Contains(key))
            {
                mapper1.Remove(mapper2[key]);
                mapper2.Remove(key);
            }
        }
    }
}