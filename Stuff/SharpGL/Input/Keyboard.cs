﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Stuff.SharpGL.Input
{
    public class Keyboard
    {
        public const char Up = (char)Keys.Up;
        public const char Left = (char)Keys.Left;
        public const char Down = (char)Keys.Down;
        public const char Right = (char)Keys.Right;

        private readonly bool[] keys;

        private readonly List<KeyboardEvent> events;

        public IEnumerable<KeyboardEvent> Events => events;

        public Keyboard(Control control)
        {
            keys = new bool[256];
            events = new List<KeyboardEvent>();
            control.KeyDown += KeyDown;
            control.KeyUp += KeyUp;
            control.KeyPress += KeyPress;
        }

        public bool this[char b] => keys[b];

        public void KeyDown(object sender, KeyEventArgs kea)
        {
            events.Add(new KeyboardEvent((char)kea.KeyValue, KeyboardEvent.EventType.Down));
            keys[kea.KeyValue] = true;
        }

        public void KeyUp(object sender, KeyEventArgs kea)
        {
            events.Add(new KeyboardEvent((char)kea.KeyValue, KeyboardEvent.EventType.Up));
            keys[kea.KeyValue] = false;
        }

        public void ClearEvents() => events.Clear();

        private void KeyPress(object sender, KeyPressEventArgs kpea)
        {
        }
    }
}
