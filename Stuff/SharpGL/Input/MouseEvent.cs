﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.SharpGL.Graphics;

namespace Stuff.SharpGL.Input
{
    public class MouseEvent
    {
        public enum EventType { Up, Down, Wheel }

        public const EventType Up = EventType.Up;
        public const EventType Down = EventType.Down;
        public const EventType Wheel = EventType.Wheel;

        public Mouse.Button Button { get; }

        public EventType Type { get; }

        public Vector2F GameLocation { get; }

        public Vector2F ScreenPosition { get; }

        public int WheelDelta { get; }

        public MouseEvent(Mouse.Button button, EventType type, Vector2F gameLocation, Vector2F screenPosition, int wheelDelta)
        {
            Button = button;
            Type = type;
            GameLocation = gameLocation;
            ScreenPosition = screenPosition;
            WheelDelta = wheelDelta;
            if (WheelDelta > 0 && type != EventType.Wheel)
                throw new Exception("Interesting...");
        }
    }
}
