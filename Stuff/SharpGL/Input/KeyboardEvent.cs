﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Input
{
    public class KeyboardEvent
    {
        public enum EventType { Up, Down }

        public static EventType Up => EventType.Up;
        public static EventType Down => EventType.Down;

        public char Key { get; }

        public EventType Type { get; }

        public KeyboardEvent(char key, EventType type)
        {
            Key = key;
            Type = type;
        }

        public void Deconstruct(out char key, out EventType type)
        {
            key = Key;
            type = Type;
        }
    }
}
