﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Stuff.SharpGL.Graphics;

namespace Stuff.SharpGL.Input
{
    /// <summary>
    /// Handles mouse movement and events in an application using a GraphicsManager.
    /// Designed for a SharpGL application.
    /// </summary>
    public class Mouse
    {
        public enum Button { None, LButton, RButton }

        public const Button NoButton = Button.None;
        public const Button LButton = Button.LButton;
        public const Button RButton = Button.RButton;

        private readonly GraphicsManager gm;

        private readonly Dictionary<Button, bool> buttons;

        private readonly List<MouseEvent> events;

        public IEnumerable<MouseEvent> Events => events;

        /// <summary>
        /// The GraphicsManagers game location of the cursor, which is the ScreenLocation adjusted for current viewlocation and zoom.
        /// </summary>
        public Vector2F GameLocation => gm.ScreenPositionToGameLocation(ScreenPosition);

        public Vector2F ScreenPosition { get; private set; }

        /// <summary>
        /// The UpdateMousePosition method must be called frequently in order to make GameLocation and ScreenPosition useful.
        /// This is easiest done by adding it to an event in the control.
        /// </summary>
        /// <param name="control">The control in which to catch mouse events.</param>
        /// <param name="gm">The GraphicsManager used in the same control.</param>
        public Mouse(Control control, GraphicsManager gm)
        {
            this.gm = gm;
            buttons = new Dictionary<Button, bool>();
            events = new List<MouseEvent>();
            ScreenPosition = Vector2F.NullVector;
            control.MouseDown += MouseDown;
            control.MouseUp += MouseUp;
            control.MouseWheel += MouseWheel;
        }

        public bool this[Button mb] => mb != Button.None && buttons.ContainsKey(mb) ? buttons[mb] : false;

        private void MouseDown(object sender, MouseEventArgs mea) => ButtonEvent(mea, true);

        private void MouseUp(object sender, MouseEventArgs mea) => ButtonEvent(mea, false);

        private void ButtonEvent(MouseEventArgs mea, bool down)
        {
            Button button;
            switch (mea.Button)
            {
                case MouseButtons.Left:
                    button = Button.LButton;
                    break;
                case MouseButtons.Right:
                    button = Button.RButton;
                    break;
                default:
                    button = Button.None;
                    break;
            }
            if (button != Button.None)
            {
                var position = new Vector2F(mea.X, mea.Y);
                var location = gm.ScreenPositionToGameLocation(position);
                buttons[button] = true;
                events.Add(new MouseEvent(button, down ? MouseEvent.Down : MouseEvent.Up, location, position, mea.Delta));
            }
        }

        private void MouseWheel(object sender, MouseEventArgs mea)
        {
            var position = new Vector2F(mea.X, mea.Y);
            var location = gm.ScreenPositionToGameLocation(position);
            events.Add(new MouseEvent(Button.None, MouseEvent.Wheel, location, position, mea.Delta));
        }

        public void UpdateMousePosition(Vector2F mousePosition)
        {
            ScreenPosition = mousePosition;
        }

        public void ClearEvents() => events.Clear();
    }
}
