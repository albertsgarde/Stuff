﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.SharpGL.Graphics.Shapes;

namespace Stuff.SharpGL.Graphics
{
    public interface IShape
    {
        ShapeType Type { get; }

        IEnumerable<Vector2F> Vertices { get; }
    }

    public static class ShapeExtensions
    {
        public static IShape Translate(this IShape shape, Vector2F vec) => new GenericShape(shape.Type, shape.Vertices.Select(v => v + vec));

        public static IShape Multiply(this IShape shape, float f) => new GenericShape(shape.Type, shape.Vertices.Select(v => v * f));

        public static IShape Rotate(this IShape shape, float angle)
        {
            var cos = (float)Math.Cos(angle);
            var sin = (float)Math.Sin(angle);
            return new GenericShape(shape.Type, shape.Vertices.Select(v => v.Rotate(cos, sin)));
        }

        public static IShape Rotate(this IShape shape, float angle, Vector2F pivot)
        {
            return shape.Translate(-pivot).Rotate(angle).Translate(pivot);
        }
    }

    public enum ShapeType
    {
        Quad,
        Triangle,
        Line,
        Point,
        TriangleFan
    }
}
