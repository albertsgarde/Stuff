﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics
{
    public class Vector2I
    {
        public int X { get; }

        public int Y { get; }

        public Vector2I(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static readonly Vector2I NullVector = new Vector2I(0, 0);

        public static implicit operator Vector2F(Vector2I vec) => new Vector2F(vec.X, vec.Y);

        public static implicit operator Vector2I(System.Drawing.Point point) => new Vector2I(point.X, point.Y);

        public static Vector2I operator +(Vector2I vec1, Vector2I vec2) => new Vector2I(vec1.X + vec2.X, vec1.Y + vec2.Y);
        public static Vector2I operator -(Vector2I vec1, Vector2I vec2) => new Vector2I(vec1.X - vec2.X, vec1.Y - vec2.Y);

        public static Vector2I operator %(Vector2I vec1, Vector2I vec2) => new Vector2I(vec1.X % vec2.X, vec1.Y % vec2.Y);

        public override string ToString() => $"({X},{Y})";
    }
}
