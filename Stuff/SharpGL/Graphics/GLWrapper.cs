﻿using SharpGL;
using SharpGL.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics
{
    public class GLWrapper
    {
        private readonly OpenGL gl;

        private Vector2F viewLoc;

        public Vector2F ViewLoc { get; set; }

        private float currentZoom;

        public float CurrentZoom { get; private set; }

        public GLWrapper(OpenGL gl)
        {
            this.gl = gl;
            viewLoc = Vector2F.NullVector;
            ViewLoc = viewLoc;
            currentZoom = 1;
            CurrentZoom = currentZoom;
        }

        public Vector2F GameLocationToScreenPosition(Vector2F gameLocation) => (gameLocation - ViewLoc) * CurrentZoom;

        public Vector2F ScreenPositionToGameLocation(Vector2F screenPosition) => (screenPosition / CurrentZoom) + ViewLoc;

        private void BeginDraw(ShapeType shapeType)
        {
            switch (shapeType)
            {
                case ShapeType.Quad:
                    gl.Begin(BeginMode.Quads);
                    break;
                case ShapeType.Triangle:
                    gl.Begin(BeginMode.Triangles);
                    break;
                case ShapeType.Line:
                    gl.Begin(BeginMode.Lines);
                    break;
                case ShapeType.Point:
                    gl.Begin(BeginMode.Points);
                    break;
                case ShapeType.TriangleFan:
                    gl.Begin(BeginMode.TriangleFan);
                    break;
                default:
                    throw new ArgumentException("Unsupported shape type.");
            }
        }

        private void PlaceVertex(Vector2F vec)
        {
            //var screenPos = GamePositionToScreenPosition(vec);
            //gl.Vertex(screenPos.X, screenPos.Y);
            gl.Vertex((vec.X - viewLoc.X) * currentZoom, (vec.Y - viewLoc.Y) * currentZoom);
        }

        public void DrawShape(IShape shape, Vector2F location, float scale = 1)
        {
            BeginDraw(shape.Type);
            foreach (var vertex in shape.Multiply(scale).Translate(location).Vertices)
                PlaceVertex(vertex);
            gl.End();
        }

        public void SetGLColour(Colour colour)
        {
            gl.Color(colour.Red, colour.Green, colour.Blue);
        }

        public void PreDraw()
        {
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);  // Clear The Screen And The Depth Buffer
        }

        public void Render()
        {
            gl.Flush();
        }

        public void PostRender()
        {
            viewLoc = ViewLoc;
            currentZoom = CurrentZoom;
        }

        public void Pan(Vector2F pan) => ViewLoc += pan;

        public void PanTo(Vector2F panDest) => ViewLoc = panDest;

        public void Zoom(Vector2F mouseLocation, float amount)
        {
            var mouseScreenLocation = GameLocationToScreenPosition(mouseLocation);
            var zoomRatio = (float)Math.Pow(2, amount);
            CurrentZoom *= zoomRatio;
            ViewLoc = mouseLocation - mouseScreenLocation/CurrentZoom;
        }
    }
}
