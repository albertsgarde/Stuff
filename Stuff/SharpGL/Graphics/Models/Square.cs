﻿using Stuff.SharpGL.Graphics.Shapes;
using SharpGL;
using SharpGL.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Models
{
    public class Square : IModel
    {
        public Vector2F OffSet { get; }

        public float Size { get; }

        public Colour Colour { get; }

        private readonly IShape quad;

        public Square(Vector2F offset, float size, Colour colour)
        {
            OffSet = offset;
            Size = size;
            Colour = colour;
            quad = new Rectangle(offset, offset + new Vector2F(size, size));
        }

        public void Draw(GLWrapper gl, Vector2F location, float scale = 1f)
        {
            gl.SetGLColour(Colour);
            gl.DrawShape(quad, location, scale);
        }
    }
}
