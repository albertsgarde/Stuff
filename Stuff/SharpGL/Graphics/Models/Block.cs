﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.SharpGL.Graphics.Shapes;

namespace Stuff.SharpGL.Graphics.Models
{
    public class Block : IModel
    {
        public Vector2F Size { get; }

        public Colour Colour { get; }

        private readonly IShape block;

        public Block(Vector2F size, Colour colour)
        {
            Size = size;
            Colour = colour;
            block = new Rectangle(Vector2F.NullVector, size);
        }

        public void Draw(GLWrapper gl, Vector2F location, float scale = 1)
        {
            gl.SetGLColour(Colour);
            gl.DrawShape(block, location, scale);
        }
    }
}
