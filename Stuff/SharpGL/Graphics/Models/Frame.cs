﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Models
{
    public class Frame : IModel
    {
        public Vector2F InternalSize { get; }

        public float Thickness { get; }

        public Colour Colour { get; }

        private readonly Block[] borderBlocks;

        private readonly Vector2F[] blockOffsets;

        public Frame(Vector2F internalSize, float thickness)
        {
            InternalSize = internalSize;
            Thickness = thickness;

            Colour = Colour.WHITE * 0.2f;

            borderBlocks = new Block[4];
            blockOffsets = new Vector2F[4];

            borderBlocks[0] = new Block(new Vector2F(InternalSize.X + thickness * 2, thickness), Colour);
            blockOffsets[0] = new Vector2F(-thickness, -thickness);

            borderBlocks[1] = new Block(new Vector2F(thickness, InternalSize.Y), Colour);
            blockOffsets[1] = new Vector2F(InternalSize.X, 0);

            borderBlocks[2] = borderBlocks[0];
            blockOffsets[2] = new Vector2F(-thickness, InternalSize.Y);

            borderBlocks[3] = borderBlocks[1];
            blockOffsets[3] = new Vector2F(-thickness, 0);
        }

        public void Draw(GLWrapper gl, Vector2F location, float scale = 1)
        {
            gl.SetGLColour(Colour);
            for (var i = 0; i < 4; ++i)
                borderBlocks[i].Draw(gl, location + blockOffsets[i], scale);
        }
    }
}
