﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Models
{
    public class GenericModel : IModel
    {
        public IReadOnlyList<(IShape shape, Colour colour, Vector2F location)> Components { get; }

        public GenericModel(params (IShape shape, Colour colour, Vector2F location)[] components)
        {
            Components = components.ToList();
        }

        public void Draw(GLWrapper gl, Vector2F location, float scale = 1)
        {
            foreach (var (shape, colour, componentLocation) in Components)
            {
                gl.SetGLColour(colour);
                gl.DrawShape(shape, location + componentLocation, scale);
            }
        }
    }
}
