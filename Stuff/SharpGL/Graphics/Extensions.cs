﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics
{
    public static class Extensions
    {
        public static Vector2F GetVector2F(this SettingsManager settings, string category, string key)
        {
            return new Vector2F(settings.GetFloat(category, key, 0), settings.GetFloat(category, key, 1));
        }
        public static Colour GetColour(this SettingsManager settings, string category, string key)
        {
            return new Colour(settings.GetFloat(category, key, 0), settings.GetFloat(category, key, 1), settings.GetFloat(category, key, 2));
        }
    }
}
