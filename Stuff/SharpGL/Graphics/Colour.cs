﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics
{
    public class Colour
    {
        public static readonly Colour WHITE = new Colour(1, 1, 1);
        public static readonly Colour RED = new Colour(1, 0, 0);
        public static readonly Colour GREEN = new Colour(0, 1, 0);
        public static readonly Colour BLUE = new Colour(0, 0, 1);

        public float Red { get; }

        public float Green { get; }

        public float Blue { get; }

        public Colour(float red, float green, float blue)
        {
            Red = red;
            Green = green;
            Blue = blue;
        }

        public static Colour operator *(Colour colour, float f) => new Colour(colour.Red * f, colour.Green * f, colour.Blue * f);
    }
}
