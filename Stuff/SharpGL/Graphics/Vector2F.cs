﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics
{
    public class Vector2F
    {
        public float X { get; }

        public float Y { get; }

        public Vector2F(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Vector2F(float radians)
        {
            X = (float)Math.Cos(radians);
            Y = (float)Math.Sin(radians);
        }

        public static Vector2F NullVector { get; } = new Vector2F(0, 0);

        public static Vector2F UnitX { get; } = new Vector2F(1, 0);

        public static Vector2F UnitY { get; } = new Vector2F(0, 1);

        public float Length => (float)Math.Sqrt(LengthSquared);

        public float Radians => Y < 0 ? 2 * (float)Math.PI - (float)Math.Acos(X / Length) : (float)Math.Acos(X / Length);

        public float LengthSquared => X * X + Y * Y;

        public static implicit operator Vector2F(System.Drawing.Point point) => new Vector2F(point.X, point.Y);

        public static Vector2F operator +(Vector2F vecA, Vector2F vecB)
        {
            return new Vector2F(vecA.X + vecB.X, vecA.Y + vecB.Y);
        }

        public static Vector2F operator -(Vector2F vecA, Vector2F vecB)
        {
            return new Vector2F(vecA.X - vecB.X, vecA.Y - vecB.Y);
        }

        public static Vector2F operator *(Vector2F vec, float multiplier)
        {
            return new Vector2F(vec.X * multiplier, vec.Y * multiplier);
        }

        public static Vector2F operator /(Vector2F vec, float divisor)
        {
            return new Vector2F(vec.X / divisor, vec.Y / divisor);
        }

        public static Vector2F operator *(float multiplier, Vector2F vec)
        {
            return new Vector2F(vec.X * multiplier, vec.Y * multiplier);
        }

        public static Vector2F operator -(Vector2F vec) => new Vector2F(-vec.X, -vec.Y);

        public float DistanceSquared(Vector2F vec) => Math.Abs((vec - this).LengthSquared);

        public float DotSum(Vector2F vec) => X * vec.X + Y * vec.Y;

        public float CosAngle(Vector2F vec) => DotSum(vec) / (Length * vec.Length);

        public float Angle(Vector2F vec) => (float)Math.Acos(CosAngle(vec));

        public Vector2F ScaleTo(float length)
        {
            return new Vector2F(length * X / Length, length / Length * Y);
        }

        public Vector2F Rotate(float angle)
        {
            var cos = (float)Math.Cos(angle);
            var sin = (float)Math.Sin(angle);
            return Rotate(cos, sin);
        }

        public Vector2F Rotate(float cos, float sin)
        {
#if DEBUG
            if (Math.Abs(1-cos*cos-sin*sin) > 0.00001)
            {
                Console.WriteLine("acos: " + Math.Acos(cos) + "  asin: " + Math.Asin(sin));
                throw new ArgumentException("Cos and sin must be of the same angle.");
            }
#endif
            return new Vector2F(X * cos - Y * sin, X * sin + Y * cos);
        }

        public Vector2I Floor() => new Vector2I((int)Math.Floor(X), (int)Math.Floor(Y));

        public override string ToString()
        {
            return "(" + X + "," + Y + ")";
        }
    }
}
