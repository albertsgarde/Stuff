﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics
{
    public interface IModel
    {
        void Draw(GLWrapper gl, Vector2F location, float scale = 1f);
    }
}
