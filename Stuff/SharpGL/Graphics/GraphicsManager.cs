﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.SharpGL.Input;
using SharpGL;

namespace Stuff.SharpGL.Graphics
{
    public class GraphicsManager
    {
        private readonly GLWrapper gl;

        private readonly GraphicsSettings settings;

        private bool drawGraphics;

        public bool DrawGraphics { get; set; }

        public delegate void Drawer(GLWrapper gl);

        public event Drawer DrawEvent;


        private const float KEY_BOARD_PAN_SPEED = 3;

        private const float ZOOM_SPEED = 1f / 960;

        private bool mousePanning = false;

        private Vector2F mousePanStart = Vector2F.NullVector;

        public GraphicsManager(OpenGL sharpGL, GraphicsSettings settings)
        {
            this.settings = settings.Clone();

            gl = new GLWrapper(sharpGL)
            {
                ViewLoc = this.settings.InitialViewLocation
            };
            Zoom(settings.InitialZoom);

            drawGraphics = false;
            DrawGraphics = drawGraphics;


        } 

        public GraphicsManager(OpenGL sharpGl) : this(sharpGl, new GraphicsSettings())
        {

        }

        public class GraphicsSettings
        {
            public Vector2F InitialViewLocation { get; set; }

            public float InitialZoom { get; set; }

            public bool DragPan { get; set; }

            public bool KeyboardPan { get; set; }

            public bool ScrollZoom { get; set; }

            public GraphicsSettings()
            {
                InitialViewLocation = Vector2F.NullVector;
                InitialZoom = 0;
                DragPan = true;
                KeyboardPan = true;
                ScrollZoom = true;
            }

            public GraphicsSettings Clone()
            {
                return new GraphicsSettings()
                {
                    InitialViewLocation = InitialViewLocation,
                    InitialZoom = InitialZoom,
                    DragPan = DragPan,
                    KeyboardPan = KeyboardPan,
                    ScrollZoom = ScrollZoom
                };
            }
        }

        public Vector2F GameLocationToScreenPosition(Vector2F gamePosition) => gl.GameLocationToScreenPosition(gamePosition);

        public Vector2F ScreenPositionToGameLocation(Vector2F screenPosition) => gl.ScreenPositionToGameLocation(screenPosition);

        public void Zoom(float multiplier) => gl.Zoom(gl.ViewLoc, multiplier);

        public void Zoom(Vector2F location, float multiplier) => gl.Zoom(location, multiplier);

        public void HandleInput(Keyboard keyboard, Mouse mouse)
        {
            // Mouse events
            foreach (var me in mouse.Events)
            {
                switch (me.Button)
                {
                    case Mouse.RButton when settings.DragPan:
                        if (me.Type == MouseEvent.Down) // Pan start.
                        {
                            mousePanning = true;
                            mousePanStart = me.GameLocation;
                        }
                        else if (me.Type == MouseEvent.Up && mousePanning) // Pan end.
                        {
                            mousePanning = false;
                            gl.Pan(mousePanStart - me.GameLocation);
                        }
                        break;
                    case Mouse.NoButton:
                        switch (me.Type)
                        {
                            case MouseEvent.Wheel when settings.ScrollZoom:
                                gl.Zoom(me.GameLocation, me.WheelDelta * ZOOM_SPEED);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            // Keyboard pan
            if (settings.KeyboardPan)
            {
                if (keyboard['W'])
                    gl.Pan(-Vector2F.UnitY * KEY_BOARD_PAN_SPEED / gl.CurrentZoom);
                if (keyboard['A'])
                    gl.Pan(-Vector2F.UnitX * KEY_BOARD_PAN_SPEED / gl.CurrentZoom);
                if (keyboard['S'])
                    gl.Pan(Vector2F.UnitY * KEY_BOARD_PAN_SPEED / gl.CurrentZoom);
                if (keyboard['D'])
                    gl.Pan(Vector2F.UnitX * KEY_BOARD_PAN_SPEED / gl.CurrentZoom);
            }

            // Pan
            if (mousePanning)
            {
                gl.Pan(mousePanStart - mouse.GameLocation);
                mousePanStart = mouse.GameLocation;
            }
        }

        public void PreDraw()
        {
            gl.PreDraw();
        }

        public void Draw()
        {
            if (drawGraphics)
                DrawEvent.Invoke(gl);
        }

        public void Render()
        {
            gl.Render();
        }

        public void PostRender()
        {
            drawGraphics = DrawGraphics;
            gl.PostRender();
        }

        public void Close()
        {

        }
    }
}
