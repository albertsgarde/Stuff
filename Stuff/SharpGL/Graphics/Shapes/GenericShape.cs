﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Shapes
{
    public class GenericShape : IShape
    {
        public IEnumerable<Vector2F> Vertices { get; }

        public ShapeType Type { get; }

        public GenericShape(ShapeType type, IEnumerable<Vector2F> vertices)
        {
            Type = type;
            Vertices = vertices;
        }
    }
}
