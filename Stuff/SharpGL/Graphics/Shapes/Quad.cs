﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Shapes
{
    public class Quad : IShape
    {
        public ShapeType Type => ShapeType.Quad;

        private readonly Vector2F[] vertices;

        public Quad(Vector2F vertex1, Vector2F vertex2, Vector2F vertex3, Vector2F vertex4)
        {
            vertices = new Vector2F[4];
            vertices[0] = vertex1;
            vertices[1] = vertex2;
            vertices[2] = vertex3;
            vertices[3] = vertex4;
        }

        public IEnumerable<Vector2F> Vertices => vertices;

        public Quad Translate(Vector2F vec)
        {
            return new Quad(vertices[0] + vec, vertices[1] + vec, vertices[2] + vec, vertices[3] + vec);
        }

        public Quad Multiply(float f)
        {
            return new Quad(vertices[0] * f, vertices[1] * f, vertices[2] * f, vertices[3] * f);
        }
    }
}
