﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Shapes
{
    public class Rectangle : IShape
    {
        private readonly Vector2F[] vertices;

        public ShapeType Type => ShapeType.Quad;

        public IEnumerable<Vector2F> Vertices => vertices;

        public Rectangle(Vector2F upperLeft, Vector2F lowerRight)
        {
            vertices = new Vector2F[4];
            vertices[0] = upperLeft;
            vertices[1] = new Vector2F(lowerRight.X, upperLeft.Y);
            vertices[2] = lowerRight;
            vertices[3] = new Vector2F(upperLeft.X, lowerRight.Y);
        }

        public Rectangle Multiply(float f) => new Rectangle(vertices[0] * f, vertices[2] * f);

        public Rectangle Translate(Vector2F vec) => new Rectangle(vertices[0] + vec, vertices[2] + vec);
    }
}
