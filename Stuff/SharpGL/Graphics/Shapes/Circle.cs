﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff.SharpGL.Graphics.Shapes
{
    public class Circle : IShape
    {
        public ShapeType Type => ShapeType.TriangleFan;

        public IEnumerable<Vector2F> Vertices { get; }

        public Circle(Vector2F centre, float radius, int triangles)
        {
            var vertices = new Vector2F[triangles + 2];
            vertices[0] = centre;
            var deltaAngle = (float)Math.PI * 2 / triangles;
            for (var i = 0; i <= triangles; ++i)//Might be possible to speed this up by multiplying by the same matrix each time. It would save some sines and cosines.
                vertices[i + 1] = centre + new Vector2F(deltaAngle * i) * radius;
            Vertices = vertices;
        }
    }
}
