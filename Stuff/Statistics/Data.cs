﻿using System.Collections.Generic;
using System.IO;

namespace Stuff
{
    public class Data
    {
        private readonly DataSet[] data;

        public Data(string fileName)
        {
            var file = new StreamReader(fileName);
            var dataList = new List<DataSet>();
            while (!file.EndOfStream)
                dataList.Add(new DataSet(file));
            file.Close();
            data = dataList.ToArray();
        }

        public DataSet this[int i] => data[i];
    }
}