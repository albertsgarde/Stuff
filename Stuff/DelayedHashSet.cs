﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff
{
    public class DelayedHashSet<T> : IEnumerable<T>
    {
        private readonly HashSet<T> list;
        private readonly HashSet<T> addList;
        private readonly HashSet<T> removeList;

        public DelayedHashSet(int initialCapacity)
        {
            list = new HashSet<T>(initialCapacity);
            addList = new HashSet<T>(initialCapacity);
            removeList = new HashSet<T>(initialCapacity);
        }

        public DelayedHashSet()
        {
            list = new HashSet<T>();
            addList = new HashSet<T>();
            removeList = new HashSet<T>();
        }

        public int Count => list.Count;

        public void Add(params T[] t) => addList.UnionWith(t);

        public void Remove(params T[] t) => removeList.UnionWith(t);

        public void Add(IEnumerable<T> ts) => addList.UnionWith(ts);

        public void Remove(IEnumerable<T> ts) => addList.UnionWith(ts);

        public void Clear()
        {
            removeList.UnionWith(list);
            removeList.UnionWith(addList);
        }

        public void Tick()
        {
            list.UnionWith(addList);
            addList.Clear();
            list.ExceptWith(removeList);
            removeList.Clear();
        }

        public IEnumerator<T> GetEnumerator() => list.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
