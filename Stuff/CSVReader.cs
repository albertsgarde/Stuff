﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stuff
{
    public static class CSVReader
    {
        public static IEnumerable<IReadOnlyList<string>> ReadFile(string path, bool skipFirst = true, char separator = ',')
        {
            var file = new StreamReader(path);
            if (skipFirst)
                file.ReadLine();
            while (!file.EndOfStream)
                yield return file.ReadLine().Split(separator);
        }

        public static IEnumerable<Output> ReadFile<Output>(string path, Func<IReadOnlyList<string>, Output> constructor,
            bool skipFirst = true, char separator = ',') => ReadFile(path, skipFirst, separator).Select(constructor);
    }
}
